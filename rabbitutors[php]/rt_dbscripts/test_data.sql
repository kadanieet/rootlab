use db_rabbitutors;

INSERT INTO rt_tutors(tutor_id, id_no, surname, middle_name, first_name, gender, religion, occupation, organization, hobbies, tsc_no, phone, email, residence) VALUES ('1', '31728199', 'Gichuhi', 'Wainaina', 'Daniel', 'm', 'c', 'student', 'Kenyatta University', 'Dancing, Singing', 0, '0713972278', 'muffwaindan@gmail.com', 'Kiangigi');

INSERT INTO rt_clients(client_id, id_no, surname, middle_name, first_name, gender, religion, email, phone, residence) VALUES ('1', '25818145', 'Gichuhi', 'Wangari', 'Esther', 'f', 'c', 'essymnjoro@gmail.com', '0724081093', 'Kiangigi');

INSERT INTO rt_students(student_id, surname, middle_name, first_name, gender, religion, school, hobbies, client_id) VALUES ('1', 'Njoroge', 'Gichuhi', 'Prince', 'm', 'c', 'KNGP', 'Dancing, Reading', '1');
