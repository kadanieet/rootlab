<div class="menu-top-menu-container">
    <ul id="top-menu" class="menu">
      <li><a href="<?php echo PHP_SELF; ?>">Home</a></li>
      <li><a rel="Client Profile" href="<?php echo PHP_SELF . "?route=clnt_prof" ?>">Profile</a></li>
      <li><a rel="Registered Students" href="<?php echo PHP_SELF . "?route=creg_stdnts" ?>">Students</a></li>
      <li><a rel="Assigned Tutors" href="<?php echo PHP_SELF . "?route=ass_tutor" ?>">Tutors</a></li>
      <li><a rel="Fee Information" href="<?php echo PHP_SELF . "?route=full_fee_in4" ?>">Fees</a></li>
    </ul>
    <a rel="Log out from the portal" href="http://rabbitutors.dev/home/contact/">Log out</a>
  </div>
