<h1>Client Portal</h1>
<?php
		//Utility Functions
			$rt_utils = $_SERVER['DOCUMENT_ROOT'] . 'rt_utils/util_fxns.php';

			if(file_exists($rt_utils))
				require_once $rt_utils;
			else
				exit('File ' .$rt_utils. 'is missing');


		Require_Files('../rt_utils/rt_const_vars.php');
		Require_Files(CNTLR);

		$cntlr = new rt_controller();
		$router = $cntlr->get_rt_app('router');

		$menu = $cntlr->get_rt_app('clt_pt_mnu');
		Require_Files($menu);

		//check the origin of the request also
	      if (isset($_GET['route'])) {
					$frm_util = $cntlr->get_rt_app('frm_util');
					Require_Files($frm_util);

					//get the application from the url
						$app = $cntlr->get_rt_app(clean_input($_GET['route']));
						Require_Files($app);
				}

				else {

						$fee_in4 = $cntlr->get_rt_app('fee_in4');

						//$files = array(/*$profile, $stdnt, $tutor, $rate_tut, */$fee_in4);
						Require_Files($fee_in4);
				}
	?>
