package project;

import java.io.*;
import java.util.Scanner;

class Hanoi {

    public static void main(String[] args) throws IOException {
        System.out.println("This Program allows users to solve the Tower of Hanoi Problem(3 disks)\n by entering the number of disks on the starting peg!");


        try {
            Scanner in = new Scanner(System.in);
            System.out.println("Enter the number of disks");
            int	c = in.nextInt();
                System.out.println("Starting Towers...");
                towers(c, "A", "B", "C");
                System.out.println("Finished Towers");
        } finally {
        	
        }
        
    }

    public static void towers(int numDisks, String startPeg, String sparePeg, String destPeg) {
        if (numDisks > 0) {
            if (numDisks == 1) {
                numDisks = numDisks;
                //System.out.println("Move disk " + numDisks + " from " + startPeg + " to " + destPeg);
            } else {
                towers(numDisks - 1, startPeg, sparePeg, destPeg);
                //System.out.println("Move disk " + numDisks + " from " + startPeg + " to " + destPeg);
                towers(numDisks - 1, startPeg, sparePeg, destPeg);
            }
        } else System.out.println("Get a grip bruh!");

    }
}