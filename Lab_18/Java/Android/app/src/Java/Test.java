import java.io.*;

class Factorial{

    public static void main(String[] args) throws IOException {

        System.out.println("This Program allows users to enter a positive integer and it produces the factorial!");

        InputStreamReader cin = null;

        try {
            cin = new InputStreamReader(System.in);
            System.out.println("Enter a number, any letter to quit.");
            int c;
            do {
                c = (int) cin.read();
                System.out.println("The factorial of " + c + " is " + factorial(c));
            } while (!c instanceof int);
        } finally {
            if (cin != null) {
                cin.close();
            }
        }
    }

    public static int factorial(int n) {
        if (n == 0)
            return;
        else
            return(n * factorial(n - ));
    }
}