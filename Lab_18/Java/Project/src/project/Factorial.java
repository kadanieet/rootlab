package project;

import java.io.*;
import java.util.Scanner;

class Factorial{

    public static void main(String[] args) throws IOException {

        System.out.println("This Program allows users to enter a positive integer and it produces the factorial!");

        Scanner in = new Scanner(System.in);
        Integer	c;
        try {
            System.out.println("Enter a number, any q to quit.");
            do {	
            	c = in.nextInt();
                System.out.println("The factorial of " + c + " is " + factorial(c));
            } while (c != 'q');
        } finally {
            
        }
    }

    public static int factorial(int n) {
        if (n == 0)
            return 1;
        else
            return (n * factorial(n - 1));
    }
}