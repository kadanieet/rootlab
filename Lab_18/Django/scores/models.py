# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

class Score(models.Model):
    scores_id = models.CharField(max_length=5, unique=True, primary_key=True, null=False)
    avg_grade = models.CharField(name='Mean Grade', max_length=2, null=False)
    avg_score = models.SmallIntegerField(name='Mean Score',null=False, default=-1)
    mathematics = models.SmallIntegerField(name='Mathematics', default=-1)
    english = models.SmallIntegerField(name='English', default=-1)
    kiswahili = models.SmallIntegerField(name='Kiswahili', default=-1)
    cre = models.SmallIntegerField(name='CRE', default=-1)
    ire = models.SmallIntegerField(name='IRE', default=-1)
    hre = models.SmallIntegerField(name='HRE', default=-1)
    science = models.SmallIntegerField(name='Science', default=-1)
    social_studies = models.SmallIntegerField(name='Social Studies', default=-1)
    french = models.SmallIntegerField(name='French', default=-1)
    german = models.SmallIntegerField(name='German', default=-1)
    spanish = models.SmallIntegerField(name='Spanish', default=-1)
    chemistry = models.SmallIntegerField(name='Chemistry', default=-1)
    biology = models.SmallIntegerField(name='Biology', default=-1)
    physics = models.SmallIntegerField(name='Physics', default=-1)
    agriculture = models.SmallIntegerField(name='Agriculture', default=-1)
    geography = models.SmallIntegerField(name='Geography', default=-1)
    history = models.SmallIntegerField(name='History', default=-1)
    business_studies = models.SmallIntegerField(name='Business Studies', default=-1)
    music = models.SmallIntegerField(name='Music', default=-1)
    home_science = models.SmallIntegerField(name='Home Science', default=-1)
    art_and_craft = models.SmallIntegerField(name='Art and Craft', default=-1)

    def __str__(self):
        return  self.scores_id
