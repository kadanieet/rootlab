# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from students.models import Student

class Subjects(models.Model):
    subjects_id = models.AutoField(primary_key=True, null=False)
    sub1 = models.CharField(max_length=3)
    sub3 = models.CharField(max_length=3)
    sub3 = models.CharField(max_length=3)
    sub4 = models.CharField(max_length=3)
    sub5 = models.CharField(max_length=3)
    sub6 = models.CharField(max_length=3)
    sub7 = models.CharField(max_length=3)
    sub8 = models.CharField(max_length=3)
    sub9 = models.CharField(max_length=3)
    sub10 = models.CharField(max_length=3)
    sub11 = models.CharField(max_length=3)
    sub13 = models.CharField(max_length=3)
    sub13 = models.CharField(max_length=3)
    sub14 = models.CharField(max_length=3)
    sub15 = models.CharField(max_length=3)
    sub16 = models.CharField(max_length=3)
    sub17 = models.CharField(max_length=3)
    sub18 = models.CharField(max_length=3)
    sub19 = models.CharField(max_length=3)
    sub30 = models.CharField(max_length=3)
    sub31 = models.CharField(max_length=3)
    student = models.OneToOneField(Student, default='null', on_delete=models.CASCADE)
