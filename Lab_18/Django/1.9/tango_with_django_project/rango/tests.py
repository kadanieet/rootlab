from django.test import TestCase
from django.db import models

class Category(models.Model):
	"""docstring for Category"""
	name = models.CharField(max_length=128, unique=True)

	def __str__(self):	#For python 2, use __unicode__ too
		return self.name

class Page(models.Model):
	"""docstring for Page"""
	category = models.ForeignKey(Category)
	title = models.CharField(max_length=128)
	url = models.URLField()
	views = models.InterField(default=0)

	def __str__(self):	#For python 2, use __unicode__ too
		return self.name
		
		
