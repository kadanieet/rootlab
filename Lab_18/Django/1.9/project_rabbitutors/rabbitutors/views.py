from django.shortcuts import render

def home(request):
    return render(request, 'rabbitutors/homepage.html', {})