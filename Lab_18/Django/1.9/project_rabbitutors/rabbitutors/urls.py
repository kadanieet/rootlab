from django.conf.urls import url
from rabbitutors import views

urlpatterns = [
    url(r'^$', views.home, name='home'),
]