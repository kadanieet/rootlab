from django.apps import AppConfig


class RabbitutorsConfig(AppConfig):
    name = 'rabbitutors'
