from django.db import models
from django.contrib.auth.models import User

"""class Scores(models.Model):
    scores_id = models.CharField(max_length=5, unique=True, primary_key=True)
    avg_grade = models.CharField(max_length=2)
    avg_score = models.IntegerField()
    sub1 = models.CharField(max_length=2, name='Mathematics')
    sub2 = models.CharField(max_length=2, name='English')
    sub3 = models.CharField(max_length=2, name='Kiswahili')
    sub4 = models.CharField(max_length=2, name='French')
    sub5 = models.CharField(max_length=2, name='German')
    sub6 = models.CharField(max_length=2, name='Spanish')
    sub7 = models.CharField(max_length=2, name='Chemistry')
    sub8 = models.CharField(max_length=2, name='Biology')
    sub9 = models.CharField(max_length=2, name='Physics')
    sub10 = models.CharField(max_length=2, name='Agriculture')
    sub11 = models.CharField(max_length=2, name='Geography')
    sub12 = models.CharField(max_length=2, name='CRE')
    sub13 = models.CharField(max_length=2, name='IRE')
    sub14 = models.CharField(max_length=2, name='HRE')
    sub15 = models.CharField(max_length=2, name='History')
    sub16 = models.CharField(max_length=2, name='Business Studies')
    sub17 = models.CharField(max_length=2, name='Music')
    sub18 = models.CharField(max_length=2, name='Home Science')
    sub19 = models.CharField(max_length=2, name='Art and Craft')

class Tutor(models.Model):
    tutor_id = models.CharField('Tutor ID',max_length=4, unique=True, primary_key=True)
    id_no = models.IntegerField('ID Number', default=None)
    surname = models.CharField('Surname', max_length=10)
    middle_name = models.CharField('Middle Name', max_length=10, null=True)
    first_name = models.CharField('First Name', max_length=10)
    gender = models.CharField('Gender')
    religion = models.CharField('Religion')
    occupation = models.CharField('Occupation', max_length=20, null=True)
    organization = models.CharField('Organization', max_length=30, null=True)
    high_school = models.CharField('Final High School Attended')
    primary_school = models.CharField('Final Primary School Attended')
    hobbies = models.TextField('Hobbies', max_length=160)
    tsc_no = models.CharField('TSC Number', max_length=8, null=True)
    phone = models.CharField('Phone Number', max_length=10)
    email = models.EmailField('Email')
    longitude = models.SmallIntegerField()
    latitude = models.SmallIntegerField()
    scores_id = models.ForeignKey(Scores)
    kcse_slip = models.ImageField(upload_to='kcse_slips')
    tsc_cert = models.ImageField(upload_to='tsc_certs', blank=True)
    picture = models.ImageField(upload_to='profile_images', blank=True)

    def __str__(self):
        return  self.first_name + self.surname

class Client(models.Model):
    client_id = models.CharField(max_length=4, unique=True, primary_key=True)
    id_no = models.IntegerField(default=None)
    surname = models.CharField(max_length=10)
    middle_name = models.CharField(max_length=10, null=True)
    first_name = models.CharField(max_length=10)
    gender = models.CharField()
    religion = models.CharField()
    phone = models.CharField(max_length=10)
    email = models.EmailField()
    county = models.CharField()
    longitude = models.SmallIntegerField()
    latitude = models.SmallIntegerField()
    picture = models.ImageField(upload_to='profile_images', blank=True)

    def __str__(self):
        return self.first_name + self.surname

class Student(models.Model):
    student_id = models.CharField(max_length=4, unique=True, primary_key=True)
    client_id = models.ForeignKey(Client.client_id)
    surname = models.CharField(max_length=10)
    middle_name = models.CharField(max_length=10, null=True)
    first_name = models.CharField(max_length=10)
    level = models.CharField(max_length=1)
    form = models.SmallIntegerField(max_length=1)
    clas = models.SmallIntegerField(max_length=1)
    school = models.CharField()
    gender = models.CharField(max_length=1)
    religion = models.CharField(max_length=1)
    occupation = models.CharField(max_length=20, null=True)
    hobbies = models.TextField(max_length=160)
    scores_id = models.ForeignKey(Scores)
    picture = models.ImageField(upload_to='profile_images', blank=True)

    def __str__(self):
        return  self.first_name + self.surname

class Assigned(models.Model):
    ass_id = models.AutoField()
    tutor_id = models.ForeignKey(Tutor.tutor_id)
    student_id = models.ForeignKey(Student.student_id)
    date = models.DateField()
    time = models.TimeField()

class Class(models.Model):
    class_id = models.CharField(max_length=4, unique=True)
    assigned = models.ForeignKey(Assigned.ass_id)
    date = models.DateField()
    start = models.TimeField()
    end = models.TimeField()
    subject = models.SmallIntegerField()

class Progress(models.Model):
    assigned = models.ForeignKey(Assigned.ass_id)
    cat1 = models.SmallIntegerField()
    cat2 = models.SmallIntegerField()
    exam = models.SmallIntegerField()

class Credentials(models.Model):
    user = models.OneToOneField(User)#Username, password, email, fname, surname

    def __str__(self):
        return self.user.username"""