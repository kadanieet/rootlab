from django.conf.urls.static import static
from django.conf.urls import url,include
from django.conf import settings
from django.contrib import admin
from rabbitutors import views

urlpatterns = [
    url(r'^$', views.home, name='home'),
    url(r'^rabbitutors/', include('rabbitutors.urls')), # Map any url starting with rabbitutors to be handled by rabbi app
    url(r'^admin/', admin.site.urls),
    url(r'^accounts/', include('registration.backends.simple.urls')),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
