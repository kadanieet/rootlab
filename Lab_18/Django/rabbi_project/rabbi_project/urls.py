from django.conf.urls.static import static
from django.conf.urls import url, include
from django.conf import settings
from django.contrib import admin
from rabbi_project import views

urlpatterns = [
    url(r'^$', views.homepage, name='homepage'),
    url(r'^admin/', admin.site.urls),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^client/', include('clients.urls')),
    url(r'^tutor/', include('tutors.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
