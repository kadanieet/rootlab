from rtauth.account.views import SignupView
from rtauth.account.forms import SignupForm


class ProfileSignupView(SignupView):
    template_name = ''
    success_url = ''
    form_class = SignupForm
    profile_class = None

    def form_valid(self, form):
        response = super(ProfileSignupView, self).form_valid(form)
        profile = self.profile_class(user=self.user)
        profile.save()

        return response