from django.shortcuts import render
from allauth.account.views import SignupView

def homepage(request):
    return render(request, 'homepage.html')
