# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from students.models import Student, Subjects, Score

# Register your models here.
admin.site.register(Student)
admin.site.register(Subjects)
admin.site.register(Score)
