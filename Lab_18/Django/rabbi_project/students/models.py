# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from clients.models import Client

class Student(models.Model):
    student_id = models.CharField('Student ID',max_length=4, unique=True, primary_key=True)
    surname = models.CharField('Surname', max_length=10)
    middle_name = models.CharField('Middle Name', max_length=10, null=True)
    first_name = models.CharField('First Name', max_length=10)
    gender = models.CharField('Gender', max_length=1)
    religion = models.CharField('Religion', max_length=2)
    level = models.CharField('Level',  max_length=1, default='')
    class_form = models.SmallIntegerField('Class/Form')
    school = models.CharField('School', max_length=30)
    hobbies = models.TextField('Hobbies', max_length=160)
    picture = models.ImageField(upload_to='profile_images', null=True)
    client = models.OneToOneField(Client, default='null', on_delete=models.CASCADE)

class Score(models.Model):
    scores_id = models.AutoField(primary_key=True, null=False)
    avg_grade = models.CharField(name='Mean_Grade', max_length=2, null=False)
    avg_score = models.SmallIntegerField(name='Mean_Score',null=False, default='-1')
    mathematics = models.SmallIntegerField(name='Mathematics', default=-1, null=True)
    english = models.SmallIntegerField(name='English', default=-1, null=True)
    kiswahili = models.SmallIntegerField(name='Kiswahili', default=-1, null=True)
    cre = models.SmallIntegerField(name='CRE', default=-1, null=True)
    ire = models.SmallIntegerField(name='IRE', default=-1, null=True)
    hre = models.SmallIntegerField(name='HRE', default=-1, null=True)
    science = models.SmallIntegerField(name='Science', default=-1, null=True)
    social_studies = models.SmallIntegerField(name='Social_Studies', default=-1, null=True)
    french = models.SmallIntegerField(name='French', default=-1, null=True)
    german = models.SmallIntegerField(name='German', default=-1, null=True)
    spanish = models.SmallIntegerField(name='Spanish', default=-1, null=True)
    chemistry = models.SmallIntegerField(name='Chemistry', default=-1, null=True)
    biology = models.SmallIntegerField(name='Biology', default=-1, null=True)
    physics = models.SmallIntegerField(name='Physics', default=-1, null=True)
    agriculture = models.SmallIntegerField(name='Agriculture', default=-1, null=True)
    geography = models.SmallIntegerField(name='Geography', default=-1, null=True)
    history = models.SmallIntegerField(name='History', default=-1, null=True)
    business_studies = models.SmallIntegerField(name='Business_Studies', default=-1, null=True)
    music = models.SmallIntegerField(name='Music', default=-1, null=True)
    home_science = models.SmallIntegerField(name='Home Science', default=-1, null=True)
    art_and_craft = models.SmallIntegerField(name='Art_and_Craft', default=-1, null=True)
    student = models.OneToOneField(Student, default='null', on_delete=models.CASCADE)

    def __str__(self):
        return  self.scores_id

class Subjects(models.Model):
    subjects_id = models.AutoField(primary_key=True, null=False)
    sub1 = models.CharField(max_length=3, null=True)
    sub2 = models.CharField(max_length=3, null=True)
    sub3 = models.CharField(max_length=3, null=True)
    sub4 = models.CharField(max_length=3, null=True)
    sub5 = models.CharField(max_length=3, null=True)
    sub6 = models.CharField(max_length=3, null=True)
    sub7 = models.CharField(max_length=3, null=True)
    sub8 = models.CharField(max_length=3, null=True)
    sub9 = models.CharField(max_length=3, null=True)
    sub10 = models.CharField(max_length=3, null=True)
    sub11 = models.CharField(max_length=3, null=True)
    sub13 = models.CharField(max_length=3, null=True)
    sub13 = models.CharField(max_length=3, null=True)
    sub14 = models.CharField(max_length=3, null=True)
    sub15 = models.CharField(max_length=3, null=True)
    sub16 = models.CharField(max_length=3, null=True)
    sub17 = models.CharField(max_length=3, null=True)
    sub18 = models.CharField(max_length=3, null=True)
    sub19 = models.CharField(max_length=3, null=True)
    sub20 = models.CharField(max_length=3, null=True)
    sub21 = models.CharField(max_length=3, null=True)
    student = models.OneToOneField(Student, default='null', on_delete=models.CASCADE)

    class Meta:
        verbose_name_plural = 'Subjects'