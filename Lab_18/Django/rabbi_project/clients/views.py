# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

def portal(request):
	return render(request, 'clients/portal.html')

def profile(request):
	return render(request, 'tutors/profile.html')

def students(request):
	return render(request, 'tutors/students.html')

def payment(request):
	return render(request, 'tutors/payment.html')
