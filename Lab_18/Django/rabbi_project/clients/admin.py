# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from clients.models import Client, Payments

# Register your models here.
admin.site.register(Client)
admin.site.register(Payments)