# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.conf import settings

class Client(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, default='')
    client_id = models.CharField('Client ID',max_length=4, unique=True, primary_key=True)
    id_no = models.IntegerField('ID Number', default=None)
    surname = models.CharField('Surname', max_length=10)
    middle_name = models.CharField('Middle Name', max_length=10, null=True)
    first_name = models.CharField('First Name', max_length=10)
    gender = models.CharField('Gender', max_length=1)
    religion = models.CharField('Religion', max_length=2)
    occupation = models.CharField('Occupation', max_length=20, null=True)
    organization = models.CharField('Organization', max_length=30, null=True)
    phone = models.CharField('Phone Number', max_length=10)
    longitude = models.FloatField()
    latitude = models.FloatField()
    picture = models.ImageField(upload_to='profile_images', blank=True, null=True)

class Payments(models.Model):
    payment_id = models.CharField(max_length=12, name='Payment ID', null=False)
    client = models.OneToOneField(Client, null=False, default='null', on_delete=models.CASCADE)
    amount = models.SmallIntegerField(name='Amount', null=False)

    class Meta:
        verbose_name_plural = 'Payments'
