from django.conf.urls import url, include
from clients import views

urlpatterns = [
    #url(r'^accounts/', include('allauth.urls')),
    url(r'^payment/$', views.payment, name='payment'),
    url(r'^portal/$', views.portal, name='portal'),
    url(r'^profile/$', views.profile, name='profile'),
    url(r'^students/$', views.students, name='students'),
]