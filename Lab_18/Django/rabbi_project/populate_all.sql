USE db_rabbitutors;

INSERT INTO clients_client
        VALUES
    ('C001', '25818145', 'Gichuhi', 'Wangari', 'Esther', 'f', 'r1', 'farmer', null, '0724081093', -1.6852, 36.1249, null),
    ('C002', '11251564', 'Ibrahim', 'Abiaziz', 'Mohamed', 'm', 'r2', 'Business Man', null, '0722123456', 2.0156, 35.3648, null);

INSERT INTO students_student(student_id, surname, middle_name, first_name, gender, religion, level, class_form, school, hobbies, client_id)
        VALUES
    ('S001', 'Njoroge', 'Gichuhi', 'Prince', 'm', 'r1', 'p', 3, 'Kiangigi', 'Dancing, Reading', 'C001'),
    ('S002', 'Komo', 'Eunice', 'Ann', 'f', 'r2', 'h', 2, 'Rimshers', 'Fencing, Singing', 'C002');

INSERT INTO students_score(Mean_Grade, Mean_Score, Mathematics, English, Kiswahili, Science, Social_Studies, CRE, student_id)
        VALUES
    ('C-', 248, 52, 44, 60, 25, 41, 19, 'S001');

INSERT INTO students_score(Mean_Grade, Mean_Score, Mathematics, English, Kiswahili, IRE, Chemistry, Biology, Agriculture, Business_Studies, student_id)
        VALUES
    ('D+', '44', '52', '60', '23', '32', '71', '44', '56', '61', 'S002');

INSERT INTO students_subjects(sub1, sub2, sub3, student_id)
        VALUES
    ('103', '342', '232', 'S002'), ('341', '110', '101', 'S001');

INSERT INTO tutors_tutor(tutor_id, id_no, surname, middle_name, first_name, gender, religion, occupation, organization, high_school, primary_school, hobbies, phone, longitude, latitude)
        VALUES
    ('T001', '31728199', 'Gichuhi', 'Wainaina', 'Daniel', 'm', 'r1', 'student', 'Kenyatta University', 'Kahuho', 'Munanda', 'Dancing, Singing', '0713972278', -1.20, 36.9),
    ('T002', '37151505', 'Mohammed', 'Aisha', 'Fatma', 'f', 'r2', 'student', 'Kenyatta University', 'Jomo Kenyatta', 'Lady of Mercy', 'Writing, Reading, Poetry', '0792411273', 3.62, 39.3);

INSERT INTO tutors_grades(avg_grade, avg_score, Mathematics, English, Kiswahili, Chemistry, Biology, Physics, Agriculture, History, tutor_id)
        VALUES
    ('A-', 79, 'A', 'B+', 'B', 'A', 'A', 'A', 'A', 'A', 'T001');

INSERT INTO tutors_grades(avg_grade, avg_score, Mathematics, English, Kiswahili, Chemistry,  Physics, IRE, Geography, tutor_id)
        VALUES  
    ('B+', 69, 'B', 'B+', 'A-', 'B-', 'A', 'A', 'C+', 'T002');

INSERT INTO tutors_payments(Payment_ID, Amount, tutor_id)
        VALUES
    ('PYMD01', 1400, 'T001');

INSERT INTO tutors_subjects(sub1, sub2, sub3, tutor_id)
        VALUES
    ('121', '233', '231', 'T001'),('233', '342', '102', 'T002');

INSERT INTO classes_assigned(Date, Time, student_id, tutor_id, Subject) 
        VALUES
    ( DATE(NOW()), TIME(NOW()), 'S001', 'T002', '103'),
    ( DATE(NOW()), TIME(NOW()), 'S002', 'T001', '341');

INSERT INTO classes_lesson( Date, Start_Time, End_Time, Subject, assigned_id)
        VALUES
    (DATE(NOW()), TIME(NOW()) + 20000, TIME(NOW()), '103', 1),
    (DATE(NOW()), TIME(NOW()) + 20000, TIME(NOW()), '341', 2);

INSERT INTO classes_progress(CAT_1_Score, CAT_2_Score, Exam_Score, Subject, assigned_id)
        VALUES
    (21, 18, 68, '101', 1);

INSERT INTO finances_account(Account_ID, Amount)
        VALUES
    ('T001', 0),('C001', 0);

select * from clients_client;
select * from students_student;
select * from students_subjects;
select * from tutors_tutor;
select * from tutors_grades;
select * from tutors_payments;
select * from tutors_subjects;
select * from classes_assigned;
select * from classes_lesson;
select * from classes_progress;
select * from finances_account;