import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE',
                      'rabbi_project.settings')

import django
django.setup()
from tutors.models import Tutor, TutorProfile, Scores

def populate():
    return