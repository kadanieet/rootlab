# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from classes.models import Assigned, Lesson
from tutors.models import Tutor
from students.models import Subjects
from clients.models import Client

class Account(models.Model):
    account_id = models.CharField(max_length=6, name='Account_ID')
    amount = models.SmallIntegerField(name='Amount', null=False)