from django.shortcuts import render
from django.contrib.auth import authenticate, login
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse
from tutors.forms import TutorInitialSignup, Stage2Form, Stage3Form, Stage4Form
from tutors.models import Tutor
from allauth.account.views import SignupView
#from rabbi_project.mixins import ProfileSignupView

class TutorSignupView(SignupView):
	template_name = 'account/signup.html'
	view_name = 'tutor_signup'
	success_url = '/tutor/portal/'
	profile_class = Tutor

tutor_signup = TutorSignupView.as_view()

def index(request):
    return render(request, 'registration/login.html')

"""
def account_login(request):
	return render(request, 'rtauth/account/login.htm')

def account_email(request):
	return render(request, 'rtauth/account/login.htm')

def account_signup(request):
	return render(request, 'rtauth/account/login.htm')

def account_logout(request):
	return render(request, 'rtauth/account/login.htm')
"""

def user_login(request):
	if request.method == 'POST':
		username = request.POST.get('username')
		password = request.POST.get('password')

		user = authenticate(username=username, password=password)

		if user:
			if user.is_active:
				login(request, user)
				return HttpResponseRedirect(reverse('index'))
			else:
				return HttpResponse("Your Rango account is disabled.")
		else:
			print("Invalid login details: {0}, {1}".format(username, password))
			return  HttpResponse("Invalid login details provided; Username: {0}, Password: {1}".format(username, password))

	else:
		return render(request, 'registration/login.html', {})


def logout(request):
	return render(request, 'registration/logout.html')


def initial_register(request):
	registered = False

	if request.method == 'POST':
		user_form = TutorInitialSignup(data=request.POST)

		if user_form.is_valid():
			email = request.POST.get('email')
			username = request.POST.get('username')
			user = user_form.save()
			user.save()
			registered = True

			if registered:
				return render(request, 'registration/registration_complete.html.html', {'email': email, 'username': username})
		else:
			print(user_form.errors)
	else:
		user_form = TutorInitialSignup()

	# Render the template depending on the context
	return  render(request, 'registration/stage_0.html',
				   {'user_form': user_form,
					'registered': registered})


def stage1signup(request):
	if request.method == 'POST':
		stage1form = Stage4Form(data=request.POST)
	else:
		stage1form = Stage4Form()

	return render(request, 'registration/stage_1.html', {'stage1form': stage1form})

def location(request):
	return render(request, 'registration/location.html')

def stage2signup(request):
	if request.method == 'POST':
		stage1form = Stage2Form(data=request.POST)
	else:
		stage1form = Stage2Form()

	return render(request, 'registration/stage_2.html', {'stage1form': stage1form})

def stage3signup(request):
	if request.method == 'POST':
		stage1form = Stage3Form(data=request.POST)
	else:
		stage1form = Stage3Form()

	return render(request, 'registration/stage_1.html', {'stage1form': stage1form})

def stage4signup(request):
	if request.method == 'POST':
		stage1form = Stage4Form(data=request.POST)
	else:
		stage1form = Stage4Form()

	return render(request, 'registration/stage_4.html', {'stage1form': stage1form})

def portal(request):
	return render(request, 'tutors/portal.html')

def profile(request):
	return render(request, 'tutors/profile.html')

def students(request):
	return render(request, 'tutors/students.html')

def payment(request):
	return render(request, 'tutors/payment.html')