from django import forms
from django.contrib.auth.models import User
from tutors.models import Tutor, Grades
from allauth.account.forms import SignupForm

"""#class TutorSignupForm(SignupForm):

class TutorInitialSignup(SignupForm):
    username = forms.CharField(max_length=30, required=True, strip=True)
    email = forms.EmailInput()

    def save(self, request):
        user = super(TutorInitialSignup, self).save(request)
        tutor = Tutor(user)
"""

class TutorSignupForm(SignupForm):
    surname = forms.CharField(max_length=10, required=True, strip=True)

    def save(self, request):
        usr = super(TutorInitialSignup, self).save(request)
        tutor = Tutor(user=usr,
                      surname=self.cleaned_data.get('surname')
                      )
        return tutor.user

class TutorInitialSignup(forms.ModelForm):
    class Meta:
        model = User
        fields = ('username', 'email')

class Stage1Form(forms.ModelForm):
    class Meta():
        model = Tutor
        fields = ('surname', 'middle_name', 'first_name', 'id_no', 'phone', 'picture')

class LocationForm(forms.ModelForm):
    model = Tutor

class Stage2Form(forms.ModelForm):
    class Meta():
        model = Tutor
        fields = ('occupation', 'organization', 'religion', 'hobbies')

class Stage3Form(forms.ModelForm):
    class Meta():
        model = Grades
        exclude = ('scores_id', )

class Stage4Form(forms.ModelForm):
    class Meta():
        model = Tutor
        fields = ('high_school', 'primary_school', 'kcse_slip', 'tsc_no', 'tsc_cert')