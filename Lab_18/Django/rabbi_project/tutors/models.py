from django.db import models
from django.contrib.auth.models import User
from django.conf import settings

class Tutor(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, default='')
    tutor_id = models.CharField('Tutor ID',max_length=4, unique=True, primary_key=True)
    id_no = models.IntegerField('ID Number', default=None)
    surname = models.CharField('Surname', max_length=10)
    middle_name = models.CharField('Middle Name', max_length=10, null=True)
    first_name = models.CharField('First Name', max_length=10)
    gender = models.CharField('Gender', max_length=1)
    religion = models.CharField('Religion', max_length=2)
    occupation = models.CharField('Occupation', max_length=20, null=True)
    organization = models.CharField('Organization', max_length=30, null=True)
    high_school = models.CharField('Final High School Attended',  max_length=30)
    primary_school = models.CharField('Final Primary School Attended', max_length=30)
    hobbies = models.TextField('Hobbies', max_length=160)
    tsc_no = models.CharField('TSC Number', max_length=8, null=True)
    phone = models.CharField('Phone Number', max_length=10)
    longitude = models.FloatField()
    latitude = models.FloatField()
    kcse_slip = models.ImageField(upload_to='kcse_slips', null=True)
    tsc_cert = models.ImageField(upload_to='tsc_certs', blank=True, null=True)
    picture = models.ImageField(upload_to='profile_images', blank=True, null=True)

class Grades(models.Model):
    grades_id = models.AutoField(primary_key=True, null=False)
    avg_grade = models.CharField(max_length=2, null=False)
    avg_score = models.SmallIntegerField(null=False, default=-1)
    mathematics = models.CharField(max_length=2, name='Mathematics', default='N', null=True)
    english = models.CharField(max_length=2, name='English', default='N', null=True)
    kiswahili = models.CharField(max_length=2, name='Kiswahili', default='N', null=True)
    cre = models.CharField(max_length=2, name='CRE', default='N', null=True)
    ire = models.CharField(max_length=2, name='IRE', default='N', null=True)
    hre = models.CharField(max_length=2, name='HRE', default='N', null=True)
    science = models.CharField(max_length=2, name='Science', default='N', null=True)
    social_studies = models.CharField(max_length=2, name='Social Studies', default='N', null=True)
    french = models.CharField(max_length=2, name='French', default='N', null=True)
    german = models.CharField(max_length=2, name='German', default='N', null=True)
    spanish = models.CharField(max_length=2, name='Spanish', default='N', null=True)
    chemistry = models.CharField(max_length=2, name='Chemistry', default='N', null=True)
    biology = models.CharField(max_length=2, name='Biology', default='N', null=True)
    physics = models.CharField(max_length=2, name='Physics', default='N', null=True)
    agriculture = models.CharField(max_length=2, name='Agriculture', default='N', null=True)
    geography = models.CharField(max_length=2, name='Geography', default='N', null=True)
    history = models.CharField(max_length=2, name='History', default='N', null=True)
    business_studies = models.CharField(max_length=2, name='Business Studies', default='N', null=True)
    music = models.CharField(max_length=2, name='Music', default='N', null=True)
    home_science = models.CharField(max_length=2, name='Home Science', default='N', null=True)
    art_and_craft = models.CharField(max_length=2, name='Art and Craft', default='N', null=True)
    tutor = models.OneToOneField(Tutor, default='null', on_delete=models.CASCADE)

    class Meta:
        verbose_name_plural = 'Grades'

class Subjects(models.Model):
    subjects_id = models.AutoField(primary_key=True, null=False)
    sub1 = models.CharField(max_length=3, null=True)
    sub2 = models.CharField(max_length=3, null=True)
    sub3 = models.CharField(max_length=3, null=True)
    sub4 = models.CharField(max_length=3, null=True)
    sub5 = models.CharField(max_length=3, null=True)
    sub6 = models.CharField(max_length=3, null=True)
    sub7 = models.CharField(max_length=3, null=True)
    sub8 = models.CharField(max_length=3, null=True)
    sub9 = models.CharField(max_length=3, null=True)
    sub10 = models.CharField(max_length=3, null=True)
    sub11 = models.CharField(max_length=3, null=True)
    sub13 = models.CharField(max_length=3, null=True)
    sub13 = models.CharField(max_length=3, null=True)
    sub14 = models.CharField(max_length=3, null=True)
    sub15 = models.CharField(max_length=3, null=True)
    sub16 = models.CharField(max_length=3, null=True)
    sub17 = models.CharField(max_length=3, null=True)
    sub18 = models.CharField(max_length=3, null=True)
    sub19 = models.CharField(max_length=3, null=True)
    sub20 = models.CharField(max_length=3, null=True)
    sub21 = models.CharField(max_length=3, null=True)
    tutor = models.OneToOneField(Tutor, default='null', on_delete=models.CASCADE)

    class Meta:
        verbose_name_plural = 'Subjects'

class Payments(models.Model):
    payment_id = models.CharField(max_length=12, name='Payment_ID', null=False)
    tutor = models.OneToOneField(Tutor, null=False, default='null', on_delete=models.CASCADE)
    amount = models.SmallIntegerField(name='Amount', null=False)

    class Meta:
        verbose_name_plural = 'Payments'

