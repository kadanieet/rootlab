from django.contrib import admin
from tutors.models import Tutor, Subjects, Grades, Payments

# Register your models here.
admin.site.register(Tutor)
admin.site.register(Payments)
admin.site.register(Subjects)
admin.site.register(Grades)
