# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from tutors.models import Tutor
from students.models import Student

class Assigned(models.Model):
    ass_id = models.AutoField(primary_key=True, null=False)
    tutor = models.OneToOneField(Tutor, default='null', on_delete=models.CASCADE, null=False)
    student = models.OneToOneField(Student, default='null', on_delete=models.CASCADE, null=False)
    subject = models.CharField(max_length=3, name='Subject', null=False)
    date = models.DateField(name='Date', null=False)
    time = models.TimeField(name='Time', null=False)

    class Meta:
        verbose_name_plural = 'Assignments'

class Lesson(models.Model):
    lesson_id = models.AutoField(primary_key=True, null=False)
    assigned = models.OneToOneField(Assigned, default='null', on_delete=models.CASCADE, null=False)
    subject = models.CharField(max_length=3, name='Subject', null=False)
    date = models.DateField(name='Date', null=False)
    start = models.TimeField(name='Start_Time', null=False)
    end = models.TimeField(name='End_Time', null=False)

class Progress(models.Model):
    assigned = models.OneToOneField(Assigned, default='null', on_delete=models.CASCADE, null=False)
    subject = models.CharField(max_length=3, name='Subject', null=False)
    cat1 = models.SmallIntegerField(name='CAT_1_Score')
    cat2 = models.SmallIntegerField(name='CAT_2_Score')
    exam = models.SmallIntegerField(name='Exam_Score')

    class Meta:
        verbose_name_plural = 'Progresses'