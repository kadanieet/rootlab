# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from classes.models import Progress, Assigned, Lesson

# Register your models here.
admin.site.register(Progress)
admin.site.register(Lesson)
admin.site.register(Assigned)