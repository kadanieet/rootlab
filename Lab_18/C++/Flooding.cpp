struct node 
{
    string data;
    struct node *left;
    struct node *right;
    struct node *parent
};
 
/* newNode() allocates a new node with the given data and NULL left and 
   right pointers. */
struct node* newNode(string data)
{
  // Allocate memory for new node 
  struct node* node = (struct node*)malloc(sizeof(struct node));
 
  // Assign data to this node
  node->data = data;
 
  // Initialize left and right children as NULL
  node->left = NULL;
  node->right = NULL;
  return(node);
}
 
 
int main()
{
  /*create root*/
  struct node *A = newNode('A');    
 
  root->left        = newNode(2);
  root->right       = newNode(3); 
 
  root->left->left  = newNode(4);
   
  getchar();
  return 0;
}

