#include <iostream>
#include <Windows.h>
#include <vector>

using namespace std;

class Node{
	public:
		char val; //the node value
		bool root;
		Node* parent;
		std::vector<Node> children;
		
		//setter methods
		void setRoot();
		void setParent(Node *parent);
		
		//getter methods
		char getValue();
		
		Node(char val);//constructor
		
		//functional methods
		void sendMessages();
		void process(Node *source);
		
};

//Member functions definitions
Node::Node(char value)  {
	this->val = value;
}

char Node::getValue() {
	return this->val;
}

void Node::setRoot() {
	this->root = true;
}

void Node::setParent(Node *par) {
	this->parent = par;
}

void Node::sendMessages() {
	if(this->children.empty()) 
	{
		cout << "No children to send messages, Terminating.." << endl;
		cout << endl;Sleep(2000);
	}
	
	else
	{
		cout << this->getValue() << " says, My children are: ";Sleep(2000);
        	
        	for (int x = 0; x != this->children.size(); ++x)
			{
			     cout << this->children.at(x).val << ", ";Sleep(2000);
			}
		
		cout << endl;
		
		
		for (int x = 0; x != this->children.size(); ++x)
		{
			cout << "Sending message to child: ";
		   	cout << this->children.at(x).val << endl;Sleep(2000);
		}
		
		cout << "Done sending messages, terminating.." << endl;Sleep(2000);
		cout << endl;
		
		
		for (int x = 0; x != this->children.size(); ++x)
		{
		   this->children.at(x).process(this);
		}
		
	}
}

void Node::process(Node* source) {
	cout << this->val;
	cout << " says, Received message from parent: ";
	cout << source->getValue();
	cout<< endl;Sleep(2000);
	this->sendMessages();
	
}

/*Everthing Happens here, Main method*/
int main() 
{
	// Initialize the nodes
	Node A('A');	
		A.setRoot();
	Node B('B');
		B.setParent(&A);
	Node C('C');
		C.setParent(&A);
	Node D('D');
		D.setParent(&B);
	Node E('E');
		E.setParent(&B);
	Node F('F');
		F.setParent(&E);
	Node G('G');
		G.setParent(&C);
	Node H('H');
		H.setParent(&G);
	
	G.children.push_back(H);
	
	C.children.push_back(G);
	
	E.children.push_back(F);
	
	B.children.push_back(D);
	B.children.push_back(E);
	
	A.children.push_back(B);
	A.children.push_back(C);
		
	// Start Broadcasting the message from root
	cout << "The root node " << A.getValue() << " has started broadcasting"; Sleep(2000);
	cout << endl;
	cout << endl;
	
	A.sendMessages();
	
	cout << "Broadcasting Complete!, exiting....." << endl ;
	
}
