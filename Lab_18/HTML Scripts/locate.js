var map = null;
var geocoder = null;
    
// using google's LatLng for everything.

function formdisable(t) {
	document.f.address.disabled = t;
	document.f.submit.disabled = t;

	var mdiv = document.getElementById('map_canvas');
	if (mdiv) {
		mdiv.style.opacity = t ? 0.25 : 1.0;
	}
}

function tryhtml5() {
	if (navigator.geolocation) {
		formdisable(true);
		navigator.geolocation.getCurrentPosition(setstatus5, nostatus5);
	}
}

function nostatus5(p) {
	// re-enable form:
	formdisable(false);
}
 
function setstatus5(p)
{
	formdisable(false);

    var pos = new google.maps.LatLng(p.coords.latitude, p.coords.longitude);
    setStatus(pos);
}

// setup maps and ui
function initialize() {
	var mapOptions = {
		center: new google.maps.LatLng(34.02, -118.485),
		zoom: 14,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
    geocoder = new google.maps.Geocoder();
    
    tryhtml5();
    document.f.address.focus();
    document.f.address.select();
}

function setStatus(point) {

	if (point) {
		map.setCenter(point);
		var marker = new google.maps.Marker({
			map: map,
			position: point
		});

		// limit decimals to make the text entry make sense:
		var ll = point.lat().toFixed(4) + ", " + point.lng().toFixed(4);
	
		document.r.style.display = 'block';
		document.getElementById('missing').style.display = 'none';
		
		document.r.latlong.value = ll;
		document.r.latlong.focus();
		document.r.latlong.select();
	} else {
		document.getElementById('missing').style.display = 'block';
		document.r.style.display = 'none';
		document.f.address.focus();
		document.f.address.select();
	}
	//document.getElementById("latlong").innerHTML = point.y + ", " + point.x;
}

function showAddress(address) {
	if (geocoder) {
		geocoder.geocode({'address' : address}, function (results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				setStatus(results[0].geometry.location);
			}
		});
	}
}
