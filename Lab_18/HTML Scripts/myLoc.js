window.onload = getMyLocation;

function getMyLocation() {
	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPostion(displayLocation, displayError);
	} else {
		alert("Oops, no geolocation support");
	}
}

function displayLocation(position) {
	var lat = position.coords.latitude;
	var lon = position.coords.longitude;

	var div = document.getElementById("location");
	div.innerHTML = "You are at Latitude: " + lat + ", longitude: "+ lon;
}

function displayError(error) {
    var errorTypes = {
        0 : "Unkown Error",
        1 : "Permission denied by user",
        2 : "Position is not available",
        3 : "Request timed out"
    };
    
    var errorMessage = errorTypes[error.code];
    
    if (error.code == 0 || error.code == 2) {
        errorMessage = errorMessage + " " + error.message;
    }
    var div = document.getElementById("location");
    div.innerHTML = errorMessage;
}