<?php require_once("res/x5engine.php"); ?>
<!DOCTYPE html><!-- HTML5 -->
<html prefix="og: http://ogp.me/ns#" lang="en-GB" dir="ltr">
	<head>
		<title>Search - Rabbi Tutors</title>
		<meta charset="utf-8" />
		<!--[if IE]><meta http-equiv="ImageToolbar" content="False" /><![endif]-->
		<meta name="author" content="@KadanieeT" />
		<meta name="generator" content="Incomedia WebSite X5 Professional 14.0.4.3 - www.websitex5.com" />
		<meta property="og:locale" content="en" />
		<meta property="og:type" content="website" />
		<meta property="og:url" content="http://rabbitutors.co.ke/imsearch.php" />
		<meta property="og:title" content="Search" />
		<meta property="og:site_name" content="Rabbi Tutors" />
		<meta property="og:image" content="http://rabbitutors.co.ke/favImage.png" />
		<meta property="og:image:type" content="image/png">
		<meta property="og:image:width" content="1248">
		<meta property="og:image:height" content="702">
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		
		<link rel="stylesheet" type="text/css" href="style/reset.css?14-0-4-3" media="screen,print" />
		<link rel="stylesheet" type="text/css" href="style/print.css?14-0-4-3" media="print" />
		<link rel="stylesheet" type="text/css" href="style/style.css?14-0-4-3" media="screen,print" />
		<link rel="stylesheet" type="text/css" href="style/template.css?14-0-4-3" media="screen" />
		<link rel="stylesheet" type="text/css" href="imHeader_pluginAppObj_02/custom.css" media="screen, print" />
		<link rel="stylesheet" type="text/css" href="imFooter_pluginAppObj_01/custom.css" media="screen, print" />
		<link rel="stylesheet" type="text/css" href="pcss/imsearch.css?14-0-4-3-636510463765643951" media="screen,print" />
		<script type="text/javascript" src="res/jquery.js?14-0-4-3"></script>
		<script type="text/javascript" src="res/x5engine.js?14-0-4-3" data-files-version="14-0-4-3"></script>
		<script type="text/javascript" src="imHeader_pluginAppObj_02/main.js"></script>
		<script type="text/javascript" src="imFooter_pluginAppObj_01/main.js"></script>
		<script type="text/javascript">
			window.onload = function(){ checkBrowserCompatibility('Your browser does not support the features necessary to display this website.','Your browser may not support the features necessary to display this website.','[1]Update your browser[/1] or [2]continue without updating[/2].','http://outdatedbrowser.com/'); };
			x5engine.utils.currentPagePath = 'imsearch.php';
			x5engine.boot.push(function () { x5engine.imPageToTop.initializeButton({}); });
		</script>
		<link rel="icon" href="favicon.png?14-0-4-3-636510463765533945" type="image/png" />
		<script type="text/javascript">x5engine.boot.push(function () {x5engine.analytics.setPageView({ "postUrl": "analytics/wsx5analytics.php" });});</script>

	</head>
	<body>
		<div id="imPageExtContainer">
			<div id="imPageIntContainer">
				<div id="imHeaderBg"></div>
				<div id="imFooterBg"></div>
				<div id="imPage">
					<div id="imHeader">
						<h1 class="imHidden">Search - Rabbi Tutors</h1>
						<div id="imHeaderObjects"><div id="imHeader_imObjectTitle_01_wrapper" class="template-object-wrapper"><div id="imHeader_imCell_1" class="" > <div id="imHeader_imCellStyleGraphics_1"></div><div id="imHeader_imCellStyle_1" ><div id="imHeader_imObjectTitle_01"><span id ="imHeader_imObjectTitle_01_text" >NET</span > </div></div></div></div><div id="imHeader_pluginAppObj_02_wrapper" class="template-object-wrapper"><div id="imHeader_imCell_2" class="" > <div id="imHeader_imCellStyleGraphics_2"></div><div id="imHeader_imCellStyle_2" ><div id="imHeader_pluginAppObj_02">
            <div id="soc_imHeader_pluginAppObj_02">
                <div class="wrapper horizontal flat none">
                	<div class='social-icon flat'><a href='https://www.facebook.com/websitex5' target='_blank'><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100"><path d="M57,93V54H70.14l2-15H57V29.09c0-4.39.94-7.39,7.24-7.39H72V8.14a98.29,98.29,0,0,0-11.6-.6C48.82,7.54,41,14.61,41,27.59V39H27V54H41V93H57Z"/></svg><span class='fallbacktext'>Fb</span></a></div><div class='social-icon flat'><a href='#' target='_blank'><svg  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100"><g><path d="M30,86H15V39H30V86ZM23.19,31.82a8.47,8.47,0,1,1,8.47-8.47A8.47,8.47,0,0,1,23.19,31.82ZM85,86H71V62.4c0-5.45.07-12.47-7.41-12.47S55,55.87,55,62V86H41V39H54v4H53.87c1.95-1,6.61-6.76,13.72-6.76C82.37,36.24,85,46.81,85,59.47V86Z"/></g></svg><span class='fallbacktext'>In</span></a></div><div class='social-icon flat'><a href='https://twitter.com/WebSiteX5' target='_blank'><svg  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100"><path d="M37,78.44c26.42,0,40.86-21.88,40.86-40.86,0-.62,0-1.24,0-1.86A29.22,29.22,0,0,0,85,28.29a28.67,28.67,0,0,1-8.25,2.26,14.41,14.41,0,0,0,6.32-7.94,28.78,28.78,0,0,1-9.12,3.49,14.37,14.37,0,0,0-24.47,13.1,40.77,40.77,0,0,1-29.6-15,14.37,14.37,0,0,0,4.45,19.17,14.26,14.26,0,0,1-6.5-1.8c0,0.06,0,.12,0,0.18A14.37,14.37,0,0,0,29.33,55.82a14.34,14.34,0,0,1-6.49.25A14.38,14.38,0,0,0,36.26,66a28.82,28.82,0,0,1-17.84,6.15A29.24,29.24,0,0,1,15,72a40.66,40.66,0,0,0,22,6.45"/></svg><span class='fallbacktext'>Tw</span></a></div>
                </div>

            </div>
                <script type="text/javascript">
                    socialicons_imHeader_pluginAppObj_02();
                </script>
        </div></div></div></div><div id="imHeader_imTextObject_04_wrapper" class="template-object-wrapper"><div id="imHeader_imCell_4" class="" > <div id="imHeader_imCellStyleGraphics_4"></div><div id="imHeader_imCellStyle_4" ><div id="imHeader_imTextObject_04">
	<div class="text-tab-content"  id="imHeader_imTextObject_04_tab0" style="">
		<div class="text-inner">
			<div><b><span class="fs12 cf1">QUALITY</span></b></div><div><br></div><div><span class="cf2">Lorem ipsum dolor sit amet, consectetur.<br></span></div><div><span class="cf2">Donec commodo sapien dapibus consequat. </span></div><div><span class="cf2">Phasellus a sagittis massa.</span></div>
		</div>
	</div>

</div>
</div></div></div><div id="imHeader_imObjectSearch_07_wrapper" class="template-object-wrapper"><div id="imHeader_imCell_7" class="" > <div id="imHeader_imCellStyleGraphics_7"></div><div id="imHeader_imCellStyle_7" ><div id="imHeader_imObjectSearch_07"><form id="imHeader_imObjectSearch_07_form" action="imsearch.php" method="get"><fieldset><input type="text" id="imHeader_imObjectSearch_07_field" name="search" value="" placeholder="..." /><button id="imHeader_imObjectSearch_07_button"></button></fieldset></form><script>$('#imHeader_imObjectSearch_07_button').click(function() { $(this).prop('disabled', true); setTimeout(function(){ $('#imHeader_imObjectSearch_07_button').prop('disabled', false);}, 900); $('#imHeader_imObjectSearch_07_form').submit(); return false; });</script></div></div></div></div><div id="imHeader_imTextObject_16_wrapper" class="template-object-wrapper"><div id="imHeader_imCell_16" class="" > <div id="imHeader_imCellStyleGraphics_16"></div><div id="imHeader_imCellStyle_16" ><div id="imHeader_imTextObject_16">
	<div class="text-tab-content"  id="imHeader_imTextObject_16_tab0" style="">
		<div class="text-inner">
			<div><b class="fs10"><span class="fs12 cf1">SOCIAL</span></b><br></div><div><br></div><div><span class="fs10 cf2">Lorem ipsum dolor sit amet, consectetur .</span><br></div><div><span class="fs10 cf2">Donec commodo sapien dapibus consequat.</span></div><div><span class="fs10 cf2">Phasellus a sagittis massa.</span></div>
		</div>
	</div>

</div>
</div></div></div><div id="imHeader_imTextObject_17_wrapper" class="template-object-wrapper"><div id="imHeader_imCell_17" class="" > <div id="imHeader_imCellStyleGraphics_17"></div><div id="imHeader_imCellStyle_17" ><div id="imHeader_imTextObject_17">
	<div class="text-tab-content"  id="imHeader_imTextObject_17_tab0" style="">
		<div class="text-inner">
			<div><b class="fs10"><span class="fs12 cf1">ABOUT</span></b><br></div><div><br></div><div><span class="fs10 cf2">Lorem ipsum dolor sit amet, consectetur .</span><br></div><div><span class="fs10 cf2">Donec commodo sapien dapibus consequat.</span></div><div><span class="fs10 cf2">Phasellus a sagittis massa.</span></div>
		</div>
	</div>

</div>
</div></div></div><div id="imHeader_imMenuObject_18_wrapper" class="template-object-wrapper"><div id="imHeader_imCell_18" class="" > <div id="imHeader_imCellStyleGraphics_18"></div><div id="imHeader_imCellStyle_18" ><div id="imHeader_imMenuObject_18"><div class="hamburger-button hamburger-component"><div><div><div class="hamburger-bar"></div><div class="hamburger-bar"></div><div class="hamburger-bar"></div></div></div></div><div class="hamburger-menu-background-container hamburger-component">
	<div class="hamburger-menu-background menu-mobile menu-mobile-animated hidden">
		<div class="hamburger-menu-close-button"><span>&times;</span></div>
	</div>
</div>
<ul class="menu-mobile-animated hidden" data-flip-direction="1">
	<li class="imMnMnFirst imPage" data-link-paths="/index.html,/">
<div class="label-wrapper">
<div class="label-inner-wrapper">
		<a class="label" href="index.html">
Home Page		</a>
</div>
</div>
	</li><li class="imMnMnMiddle imPage" data-link-paths="/page-1.html">
<div class="label-wrapper">
<div class="label-inner-wrapper">
		<a class="label" href="page-1.html">
Page 1		</a>
</div>
</div>
	</li><li class="imMnMnMiddle imPage" data-link-paths="/page-2.html">
<div class="label-wrapper">
<div class="label-inner-wrapper">
		<a class="label" href="page-2.html">
Page 2		</a>
</div>
</div>
	</li><li class="imMnMnLast imPage" data-link-paths="/page-3.html">
<div class="label-wrapper">
<div class="label-inner-wrapper">
		<a class="label" href="page-3.html">
Page 3		</a>
</div>
</div>
	</li></ul></div><script type="text/javascript">
x5engine.boot.push(function(){x5engine.initMenu('imHeader_imMenuObject_18',1000)});
$(function () {$('#imHeader_imMenuObject_18 ul li').each(function () {    var $this = $(this), timeout = 0;    $this.on('mouseenter', function () {        if($(this).parents('#imHeader_imMenuObject_18-menu-opened').length > 0) return;         clearTimeout(timeout);        setTimeout(function () { $this.children('ul, .multiple-column').stop(false, false).fadeIn(); }, 250);    }).on('mouseleave', function () {        if($(this).parents('#imHeader_imMenuObject_18-menu-opened').length > 0) return;         timeout = setTimeout(function () { $this.children('ul, .multiple-column').stop(false, false).fadeOut(); }, 250);    });});});

</script>
</div></div></div></div>
					</div>
					<div id="imStickyBarContainer">
						<div id="imStickyBarGraphics"></div>
						<div id="imStickyBar">
							<div id="imStickyBarObjects"><div id="imStickyBar_imMenuObject_01_wrapper" class="template-object-wrapper"><div id="imStickyBar_imCell_1" class="" > <div id="imStickyBar_imCellStyleGraphics_1"></div><div id="imStickyBar_imCellStyle_1" ><div id="imStickyBar_imMenuObject_01"><div class="hamburger-button hamburger-component"><div><div><div class="hamburger-bar"></div><div class="hamburger-bar"></div><div class="hamburger-bar"></div></div></div></div><div class="hamburger-menu-background-container hamburger-component">
	<div class="hamburger-menu-background menu-mobile menu-mobile-animated hidden">
		<div class="hamburger-menu-close-button"><span>&times;</span></div>
	</div>
</div>
<ul class="menu-mobile-animated hidden" data-flip-direction="1">
	<li class="imMnMnFirst imPage" data-link-paths="/index.html,/">
<div class="label-wrapper">
<div class="label-inner-wrapper">
		<a class="label" href="index.html">
Home Page		</a>
</div>
</div>
	</li><li class="imMnMnMiddle imPage" data-link-paths="/page-1.html">
<div class="label-wrapper">
<div class="label-inner-wrapper">
		<a class="label" href="page-1.html">
Page 1		</a>
</div>
</div>
	</li><li class="imMnMnMiddle imPage" data-link-paths="/page-2.html">
<div class="label-wrapper">
<div class="label-inner-wrapper">
		<a class="label" href="page-2.html">
Page 2		</a>
</div>
</div>
	</li><li class="imMnMnLast imPage" data-link-paths="/page-3.html">
<div class="label-wrapper">
<div class="label-inner-wrapper">
		<a class="label" href="page-3.html">
Page 3		</a>
</div>
</div>
	</li></ul></div><script type="text/javascript">
x5engine.boot.push(function(){x5engine.initMenu('imStickyBar_imMenuObject_01',1000)});
$(function () {$('#imStickyBar_imMenuObject_01 ul li').each(function () {    var $this = $(this), timeout = 0;    $this.on('mouseenter', function () {        if($(this).parents('#imStickyBar_imMenuObject_01-menu-opened').length > 0) return;         clearTimeout(timeout);        setTimeout(function () { $this.children('ul, .multiple-column').stop(false, false).fadeIn(); }, 250);    }).on('mouseleave', function () {        if($(this).parents('#imStickyBar_imMenuObject_01-menu-opened').length > 0) return;         timeout = setTimeout(function () { $this.children('ul, .multiple-column').stop(false, false).fadeOut(); }, 250);    });});});

</script>
</div></div></div><div id="imStickyBar_imObjectTitle_02_wrapper" class="template-object-wrapper"><div id="imStickyBar_imCell_2" class="" > <div id="imStickyBar_imCellStyleGraphics_2"></div><div id="imStickyBar_imCellStyle_2" ><div id="imStickyBar_imObjectTitle_02"><span id ="imStickyBar_imObjectTitle_02_text" ><a href="index.html" onmouseover="x5engine.imTip.Show(this, { text: '&amp;lt;img src=&quot;files/C__Data_Users_DefApps_AppData_INTERNETEXPLORER_Temp_Saved-Images_background5_pc.png&quot; alt=&quot;&quot; style=&quot;width: 50%;&quot;/&amp;gt;&amp;lt;div&amp;gt;Professional Tutoring Organization&amp;lt;/div&amp;gt;', width: 180});" onclick="return x5engine.utils.location('index.html', null, false)">Rabbi Tutors</a></span > </div></div></div></div></div>
						</div>
					</div>
					<a class="imHidden" href="#imGoToCont" title="Skip the main menu">Go to content</a>
					<div id="imSideBarContainer">
						<div id="imSideBarGraphics"></div>
						<div id="imSideBar">
							<div id="imSideBarObjects"></div>
						</div>
					</div>
					<div id="imContentContainer">
						<div id="imContentGraphics"></div>
						<div id="imContent">
							<a id="imGoToCont"></a>
				<div id="imSearchPage">
				<h2 id="imPgTitle">Search results</h2>
				<?php
				$search = new imSearch();
				$keys = isset($_GET['search']) ? @htmlspecialchars($_GET['search']) : "";
				$page = isset($_GET['page']) ? @htmlspecialchars($_GET['page']) : 0;
				$type = isset($_GET['type']) ? @htmlspecialchars($_GET['type']) : "pages"; ?>
				<div class="searchPageContainer">
				<?php echo $search->search($keys, $page, $type); ?>
				</div>
				</div>
				
							<div class="imClear"></div>
						</div>
					</div>
					<div id="imFooter">
						<div id="imFooterObjects"><div id="imFooter_pluginAppObj_01_wrapper" class="template-object-wrapper"><div id="imFooter_imCell_1" class="" > <div id="imFooter_imCellStyleGraphics_1"></div><div id="imFooter_imCellStyle_1" ><div id="imFooter_pluginAppObj_01">
            <div id="soc_imFooter_pluginAppObj_01">
                <div class="wrapper horizontal flat none">
                	<div class='social-icon flat'><a href='https://www.facebook.com/websitex5' target='_blank'><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100"><path d="M57,93V54H70.14l2-15H57V29.09c0-4.39.94-7.39,7.24-7.39H72V8.14a98.29,98.29,0,0,0-11.6-.6C48.82,7.54,41,14.61,41,27.59V39H27V54H41V93H57Z"/></svg><span class='fallbacktext'>Fb</span></a></div><div class='social-icon flat'><a href='#' target='_blank'><svg  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100"><g><path d="M30,86H15V39H30V86ZM23.19,31.82a8.47,8.47,0,1,1,8.47-8.47A8.47,8.47,0,0,1,23.19,31.82ZM85,86H71V62.4c0-5.45.07-12.47-7.41-12.47S55,55.87,55,62V86H41V39H54v4H53.87c1.95-1,6.61-6.76,13.72-6.76C82.37,36.24,85,46.81,85,59.47V86Z"/></g></svg><span class='fallbacktext'>In</span></a></div><div class='social-icon flat'><a href='https://twitter.com/WebSiteX5' target='_blank'><svg  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100"><path d="M37,78.44c26.42,0,40.86-21.88,40.86-40.86,0-.62,0-1.24,0-1.86A29.22,29.22,0,0,0,85,28.29a28.67,28.67,0,0,1-8.25,2.26,14.41,14.41,0,0,0,6.32-7.94,28.78,28.78,0,0,1-9.12,3.49,14.37,14.37,0,0,0-24.47,13.1,40.77,40.77,0,0,1-29.6-15,14.37,14.37,0,0,0,4.45,19.17,14.26,14.26,0,0,1-6.5-1.8c0,0.06,0,.12,0,0.18A14.37,14.37,0,0,0,29.33,55.82a14.34,14.34,0,0,1-6.49.25A14.38,14.38,0,0,0,36.26,66a28.82,28.82,0,0,1-17.84,6.15A29.24,29.24,0,0,1,15,72a40.66,40.66,0,0,0,22,6.45"/></svg><span class='fallbacktext'>Tw</span></a></div>
                </div>

            </div>
                <script type="text/javascript">
                    socialicons_imFooter_pluginAppObj_01();
                </script>
        </div></div></div></div><div id="imFooter_imTextObject_04_wrapper" class="template-object-wrapper"><div id="imFooter_imCell_4" class="" > <div id="imFooter_imCellStyleGraphics_4"></div><div id="imFooter_imCellStyle_4" ><div id="imFooter_imTextObject_04">
	<div class="text-tab-content"  id="imFooter_imTextObject_04_tab0" style="">
		<div class="text-inner">
			<b>WEBSITE</b><div><div><a href="index.html" class="imCssLink" onclick="return x5engine.utils.location('index.html', null, false)"><b>Homepage</b></a><div><a href="page-1.html" class="imCssLink" onclick="return x5engine.utils.location('page-1.html', null, false)"><b>Page1</b></a></div><div><a href="page-2.html" class="imCssLink" onclick="return x5engine.utils.location('page-2.html', null, false)"><b>Page2</b></a></div><div><a href="page-3.html" class="imCssLink" onclick="return x5engine.utils.location('page-3.html', null, false)"><b>Page3</b></a></div></div></div>
		</div>
	</div>

</div>
</div></div></div><div id="imFooter_imTextObject_05_wrapper" class="template-object-wrapper"><div id="imFooter_imCell_5" class="" > <div id="imFooter_imCellStyleGraphics_5"></div><div id="imFooter_imCellStyle_5" ><div id="imFooter_imTextObject_05">
	<div class="text-tab-content"  id="imFooter_imTextObject_05_tab0" style="">
		<div class="text-inner">
			<b><span class="cf1">COMPANY</span></b><div><b>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </b><br></div>
		</div>
	</div>

</div>
</div></div></div><div id="imFooter_imTextObject_07_wrapper" class="template-object-wrapper"><div id="imFooter_imCell_7" class="" > <div id="imFooter_imCellStyleGraphics_7"></div><div id="imFooter_imCellStyle_7" ><div id="imFooter_imTextObject_07">
	<div class="text-tab-content"  id="imFooter_imTextObject_07_tab0" style="">
		<div class="text-inner">
			<div style="text-align: right;"><span class="fs10 cf1">Created with WebSite X5</span></div>
		</div>
	</div>

</div>
</div></div></div><div id="imFooter_imTextObject_08_wrapper" class="template-object-wrapper"><div id="imFooter_imCell_8" class="" > <div id="imFooter_imCellStyleGraphics_8"></div><div id="imFooter_imCellStyle_8" ><div id="imFooter_imTextObject_08">
	<div class="text-tab-content"  id="imFooter_imTextObject_08_tab0" style="">
		<div class="text-inner">
			<div style="text-align: right;"><b><span class="cf1">CONTACTS</span></b></div><div style="text-align: right;"><div><b>+00 012 345 678<br></b></div><div><b>example@example.com</b></div></div>
		</div>
	</div>

</div>
</div></div></div><div id="imFooter_imTextObject_09_wrapper" class="template-object-wrapper"><div id="imFooter_imCell_9" class="" > <div id="imFooter_imCellStyleGraphics_9"></div><div id="imFooter_imCellStyle_9" ><div id="imFooter_imTextObject_09">
	<div class="text-tab-content"  id="imFooter_imTextObject_09_tab0" style="">
		<div class="text-inner">
			<b><span class="cf1">PRIVACY</span></b><div><b>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </b><br></div>
		</div>
	</div>

</div>
</div></div></div><div id="imFooter_imTextObject_10_wrapper" class="template-object-wrapper"><div id="imFooter_imCell_10" class="" > <div id="imFooter_imCellStyleGraphics_10"></div><div id="imFooter_imCellStyle_10" ><div id="imFooter_imTextObject_10">
	<div class="text-tab-content"  id="imFooter_imTextObject_10_tab0" style="">
		<div class="text-inner">
			<div><br></div><div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. <br></div>
		</div>
	</div>

</div>
</div></div></div></div>
					</div>
				</div>
				<span class="imHidden"><a href="#imGoToCont" title="Read this page again">Back to content</a></span>
			</div>
		</div>
		
		<noscript class="imNoScript"><div class="alert alert-red">To use this website you must enable JavaScript.</div></noscript>
	</body>
</html>
