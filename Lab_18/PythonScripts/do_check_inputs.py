def do_check_inputs(text_file, delimiter='-'):
    inputs = []
    try:
        with open(text_file) as file:
            for each_line in file:
                clean_line = each_line.strip()
                (value, initial) = clean_line.split(delimiter, 1)
                inputs.append('<p><label for="'+ value.strip() +'">'+ value.strip() +'</label> <input type="checkbox" name="'+ value.strip() +'" id="id_' + value.strip() +'">')

    except IOError as err:
        print ('File error: ' + str(err))

    return inputs
        
