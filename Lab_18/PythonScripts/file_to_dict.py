def file_to_dict(text_file, delimiter='-'):
    data_dict = {}
    try:
        with open(text_file) as file:
            for each_line in file:
                clean_line = each_line.strip()
                (value, initial) = clean_line.split(delimiter, 1)
                key = initial.strip()
                value = value.strip()
                data_dict[key] = value

    except IOError as err:
        print ('File error: ' + str(err))

    return data_dict
