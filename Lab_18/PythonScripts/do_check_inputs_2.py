def do_check_inputs(text_file, delimiter='-'):
    inputs = {}
    try:
        with open(text_file) as file:
            for each_line in file:
                clean_line = each_line.strip()
                (value, initial) = clean_line.split(delimiter, 1)
                key = value.strip().replace(' ','_')
                value = value.strip()
                inputs[key] = value

    except IOError as err:
        print ('File error: ' + str(err))

    return inputs
