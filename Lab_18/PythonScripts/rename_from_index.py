def rename_files_from_index(directory, character_to_start, first_ext_char, last_ext_char):
    import os
    os.chdir(directory)
    for name in os.listdir("."):
        try:
            if name.startswith(character_to_start):
                os.rename(name, (name[name.index(character_to_start):name.index(first_ext_char)] + name[name.index(last_ext_char)+1:] + name[name.index(first_ext_char):name.index(last_ext_char)+2]).strip())
            else:
                os.rename(name, (name[name.index(character_to_start):name.index(first_ext_char)] + ' ' + (name[:name.index(character_to_start)]).replace('_', '').strip() + name[name.index(first_ext_char):]).strip())
        except ValueError:
            continue
