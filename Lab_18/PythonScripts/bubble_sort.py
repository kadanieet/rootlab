def bubble_sort(the_list):
    for passnum in range(len(the_list)-1,0,-1):
        for i in range(passnum):
            if the_list[i]>the_list[i+1]:
                #A temporary storage needed for swaping the values
                temp = the_list[i]
                the_list[i] = the_list[i+1]
                the_list[i+1] = temp

the_list = [54,26,93,17,77,31,44,55,20]
another_list = [9, 1, 9, 7, 3, 10, 13, 15, 8, 12]
print('Unsorted: ' + str(the_list))
bubble_sort(the_list)
print('Sorted: ' + str(the_list))
print('')
print('Unsorted: ' + str(another_list))
bubble_sort(another_list)
print('Sorted: ' + str(another_list))

