import time

class Node:
    def __init__(self,key):
        self.val = key
        self.is_root = False
        self.level = -1
        self.parent = None
        self.children = []
        self.Active = True

    def send_messages(self):
        if self.children == []:
            self.active = False
            print( str(self) + ' says: "No children to send messages, Terminating..."')
            print('');time.sleep(2)

        else:
            print( str(self) + "'s children are:", end=" ");time.sleep(2)
            for child in self.children:
                print(str(child), end=',', flush=True);time.sleep(2)
            print('')
            for child in self.children:
                print(str(self) + ' says: "sending Message to Child: ' + str(child)+ '"');time.sleep(2)
            print(str(self) + ' says: "Done sending messages to children, Terminating..."');time.sleep(2)
            print('')
            
            for child in self.children:
                child.process(self)
                
                
    def process(self, source):
        print(str(self) + ' says: "Received message from Parent: ' + str(source)+ '"')
        self.send_messages()
    
    def __str__(self):
          return self.val  

#Initialize the nodes
A = Node('A')
A.level = 0
A.is_root = True

B = Node('B')
B.level = 1
B.parent = A

C = Node('C')
C.level = 1
C.parent = A

D = Node('D')
D.level = 2
D.parent = B

E = Node('E')
E.level = 2
E.parent = B

F = Node('F')
F.level = 3
F.parent = E

G = Node('G')
G.level = 2
G.parent = C

H = Node('H')
H.level = 3
H.parent = G

#Children definition
A.children = [B,C]
B.children = [D,E]
C.children = [G]
E.children = [F]
G.children = [H]

def start():
    print('The root node: ' + str(A) + ', has started Broadcasting')
    print('');time.sleep(2)
    A.send_messages()

start()
