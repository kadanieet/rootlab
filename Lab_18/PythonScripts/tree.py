import random

# The overall leader
#leader = -1
unexplored = []

class Node:
    def __init__(self,key):
        self.val = key
        self.level = -1
        self.parent = None
        self.children = []
        self.leader = -1

    def leader_msg(self, from_node, msg='leader'):
        if self.leader < from_node.val:
            self.leader = from_node.val; print(str(self) + " says, 'My new leader is node " + str(from_node))
            self.parent = from_node; print(str(self) + " says, 'My new parent is node " + str(from_node))
            self.children = []
            #unexplored
            explore(self)
        elif self.leader == from_node.val:
            from_node.already(self.leader)

    def parent_msg(self, from_node, msg='parent'):
        if from_node.val == self.leader:
            self.children.append(from_node); print(str(self) + " says, 'My new child is node " + str(from_node))
            explore(self)
            
    def already(self, from_node, msg='already'):
        if from_node.val == self.leader:
            explore(self)

    #def echo_status(self, node, msg):
        #print(str() + " says, '" + msg + "'")

    def __str__(self):
        return 'Node: ' + str(self.val)

def explore(node):
    if unexplored != []: # If our unexplored list isn't empty
        p_k = unexplored[random.randrange(len(unexplored))]; print( str(node) + " has picked and removed " + str(p_k) + " in unexplored") # select a random node from our unexplored
        unexplored.remove(p_k); print("Send leader msg to node: " + str(p_k)) # remove the random node from unexplored
        p_k.leader_msg(node) # send the leader message to random node
    else: # If the unexplored is empty
        if node.parent != node: # and self is not set as parent
            node.parent.parent_msg(node) #sent parent msg and leader value
        else:# Terminate as root of spanning Tree
            print('Node ' + str(node.val) + 'is root')
    
# Define random nodes
node0 = Node(0)
node1 = Node(1)
node2 = Node(2)
node3 = Node(3)
node4 = Node(4)

# List of all defined nodes
nodes = [node0, node1, node2, node3, node4]

print("all available nodes are:", end=" ")
for node in nodes:
    print(str(node), end=', ', flush=True)
print('')

#generate a radom index from the nodes to select one who wakes spontaneously
node = nodes[random.randrange(len(nodes))]
print(str(node) + ' has woken up!') 

#for node in nodes:
if node.parent == None: #if the no parent/begin point
    node.leader = node.val; print('New leader is Node: ' + str(node)) # set self as leader
    node.parent = node; print( str(node) + " parent is " + str(node.parent)) # set self as parent
    nodes.remove(node) # remove self from node list
    unexplored = nodes # add all nodes except self in unexplored
    nodes = [node0, node1, node2, node3, node4] # Initialize the node list again
    explore(node) # Explore other nodes
            
            

# create root
"""root = Node(1)
''' following is the tree after above statement
        1
      /   \
     None  None'''
 
root.left = Node(2);
root.right = Node(3);
   
''' 2 and 3 become left and right children of 1
           1
         /   \
        2      3
     /    \    /  \
   None None None None'''
 
 
root.left.left  = Node(4);
'''4 becomes left child of 2
           1
       /       \
      2          3
    /   \       /  \
   4    None  None  None
  /  \
None None'''
"""
