Python 2.7.13 |Anaconda 4.4.0 (32-bit)| (default, May 11 2017, 14:07:41) [MSC v.1500 32 bit (Intel)] on win32
Type "copyright", "credits" or "license()" for more information.
>>> GENDERS = {'M': 'Male', 'F':'Female', 'O':'Other'}
>>> len(GENDERS)
3
>>> S = {'M': 'Male', 'F':'Female', 'O':'Other'}
>>> is S == GENDER
SyntaxError: invalid syntax
>>> is(S == GENDER)
SyntaxError: invalid syntax
>>> S is GENDERS
False
>>> S
{'M': 'Male', 'O': 'Other', 'F': 'Female'}
>>> GENDERS
{'M': 'Male', 'O': 'Other', 'F': 'Female'}
>>> if S == GENDERS:
	print(True)

	
True
>>> 
>>> tutor = ['T1', 'T2', 'T1', 'T13']
>>> students = [1, 1, 1]
>>> tutors = ['T2', 'T2', 'T5']
>>> students = [1, 1, 1, 2, 2, 3]
>>> tutors = ['T2', 'T2', 'T5', 'T2', 'T5', 'T5']
>>> len(students)
6
>>> len(tutors)
6
>>> for key in S.keys():
	if key == 'S':
		S[key] = [S[key], 'Wazee']

		
>>> S
{'M': 'Male', 'O': 'Other', 'F': 'Female'}
>>> for key in S.keys():
	if key == 'M':
		S[key] = [S[key], 'Wazee']

		
>>> S
{'M': ['Male', 'Wazee'], 'O': 'Other', 'F': 'Female'}
>>> for key in S.keys():
	if key == 'S':
		S[key] = [S[key], 'Wazee']
	else:
		S['S'] = 'Wazee'

		
>>> S
{'S': 'Wazee', 'M': ['Male', 'Wazee'], 'O': 'Other', 'F': 'Female'}
>>> for key in S.keys():
	if key == 'S':
		S[key] = S[key].append('Wazee')
	else:
		S['S'] = ['Wazee']

		

Traceback (most recent call last):
  File "<pyshell#34>", line 3, in <module>
    S[key] = S[key].append('Wazee')
AttributeError: 'str' object has no attribute 'append'
>>> S = {}
>>> for key in S.keys():
	if key == 'S':
		S[key] = S[key].append('Wazee')
	else:
		S['S'] = ['Wazee']

		
>>> S
{}
>>> if S.keys():
	print('has')

	
>>> if not S.keys():
	print('has')

	
has
>>> if S.keys():
	for key in S.keys():
		if key == 'S':
			S[key] = S[key].append('Wazee')
		else:
			S['S'] = ['Wazee']
else:
	S['S'] = ['Wazee']

	
>>> s

Traceback (most recent call last):
  File "<pyshell#48>", line 1, in <module>
    s
NameError: name 's' is not defined
>>> S
{'S': ['Wazee']}
>>> if S.keys():
	for key in S.keys():
		if key == 'S':
			S[key] = S[key].append('Wazee')
		else:
			S['S'] = ['Wazee']
else:
	S['S'] = ['Wazee']

	
>>> S
{'S': None}
>>> S = {}
>>> if S.keys():
	for key, value in S.keys():
		if key == 'S':
			S[key] = value.append('Wazee')
		else:
			S['S'] = ['Wazee']
else:
	S['S'] = ['Wazee']

	
>>> if S.keys():
	for key, value in S.keys():
		if key == 'S':
			S[key] = value.append('Wazee')
		else:
			S['S'] = ['Wazee']
else:
	S['S'] = ['Wazee']

	

Traceback (most recent call last):
  File "<pyshell#57>", line 2, in <module>
    for key, value in S.keys():
ValueError: need more than 1 value to unpack
>>> S
{'S': ['Wazee']}
>>> if S.keys():
	for key, value in S:
		if key == 'S':
			S[key] = value.append('Wazee')
		else:
			S['S'] = ['Wazee']
else:
	S['S'] = ['Wazee']

	

Traceback (most recent call last):
  File "<pyshell#60>", line 2, in <module>
    for key, value in S:
ValueError: need more than 1 value to unpack
>>> if S.keys():
	for key, value in S.items():
		if key == 'S':
			S[key] = value.append('Wazee')
		else:
			S['S'] = ['Wazee']
else:
	S['S'] = ['Wazee']

	
>>> S
{'S': None}
>>> S = {}
>>> if S.keys():
	for key in S.keys():
		if key == 'S':
			S[key] = S[key].append('Wazee')
		else:
			S['S'] = ['Wazee']
else:
	S['S'] = ['Wazee']

	
>>> S
{'S': ['Wazee']}
>>> S['S']
['Wazee']
>>> type(S['S'])
<type 'list'>
>>> S['S'] = ['F']
>>> S['S']
['F']
>>> S['S'].append('Wazee')
>>> S['S']
['F', 'Wazee']
>>> for key in S.keys():
	key

	
'S'
>>> for key in S.keys():
	if key == 'S':
		'yes'

		
'yes'
>>> for key in S.keys():
	if key == 'S':
		S[key]

		
['F', 'Wazee']
>>> for key in S.keys():
	if key == 'S':
		S[key].append('Yeah')

		
>>> S
{'S': ['F', 'Wazee', 'Yeah']}
>>> S = {}
>>> if S.keys():
	for key in S.keys():
		if key == 'S':
			S[key].append('Wazee')
		else:
			S['S'] = ['Wazee']
else:
	S['S'] = ['Wazee']

	
>>> S
{'S': ['Wazee']}
>>> if S.keys():
	for key in S.keys():
		if key == 'S':
			S[key].append('Wazee')
		else:
			S['S'] = ['Wazee']
else:
	S['S'] = ['Wazee']

	
>>> S
{'S': ['Wazee', 'Wazee']}
>>> d = ()
>>> d
()
>>> type(d)
<type 'tuple'>
>>> d = set()
>>> d
set([])
>>> type(d)
<type 'set'>
>>> f = ([])
>>> type(f)
<type 'list'>
>>> S = {}
>>> if S.keys():
	for key in S.keys():
		if key == 'S':
			S[key].add('Wazee')
		else:
			S['S'] = set('Wazee')
else:
	S['S'] = set('Wazee')

	
>>> S
{'S': set(['a', 'z', 'e', 'W'])}
>>> d.add("Fail")
>>> d
set(['Fail'])
>>> d.add('sal')
>>> d
set(['Fail', 'sal'])
>>> s = {}
>>> S = {}
>>> if S.keys():
	for key in S.keys():
		if key == 'S':
			S[key] = set()
			S[key].add('Wazee')
		else:
			S[key] = set()
			S[key].add('Wazee')
else:
	S[key] = set()S[key].add('Wazee')
	
SyntaxError: invalid syntax
>>> if S.keys():
	for key in S.keys():
		if key == 'S':
			S[key] = set()
			S[key].add('Wazee')
		else:
			S[key] = set()
			S[key].add('Wazee')
else:
	S[key] = set()
	S[key].add('Wazee')

	
>>> S
{'S': set(['Wazee'])}
>>> if S.keys():
	for key in S.keys():
		if key == 'S':
			S[key] = set()
			S[key].add('Wazee')
		else:
			S[key] = set()
			S[key].add('Wazee')
else:
	S[key] = set()
	S[key].add('Wazee')

	
>>> S
{'S': set(['Wazee'])}
>>> if S.keys():
	for key in S.keys():
		if key == 'S':
			S[key] = set()
			S[key].add('Men')
		else:
			S[key] = set()
			S[key].add('Wazee')
else:
	S[key] = set()
	S[key].add('Wazee')

	
>>> S
{'S': set(['Men'])}
>>> tutors = ['T2', 'T2', 'T5', 'T2', 'T5', 'T5']
>>> set(tutors)
set(['T5', 'T2'])
>>> tutors
['T2', 'T2', 'T5', 'T2', 'T5', 'T5']
>>> for 
