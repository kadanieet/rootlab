"""I start with the list to report the actual index of the item being searched
    otherwise, the index of the item will be zero cause binary search uses the midpoint"""

testlist = [3, 5, 6, 8, 11, 12, 14, 15, 17, 18]

def binary_search(the_list, item):
    if len(the_list) == 0:
        return 'Item: ' + str(item) + ' Not Found'
    else:
        #Divide the list by two(divide and conquer) to reduce the time complexity
        mid_point = len(the_list)//2
        if the_list[mid_point]==item:
          return 'Item: ' + str(item) + ' Found at Index ' + str(testlist.index(item))
        else:
          if item<the_list[mid_point]:
            return binary_search(the_list[:mid_point], item)
          else:
            return binary_search(the_list[mid_point + 1:], item)

print(binary_search(testlist, 8))
print(binary_search(testlist, 13))
print(binary_search(testlist, 14))
