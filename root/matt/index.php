<!DOCTYPE html>
<html>
   <head>
      <title>The Materialize Example</title>
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="stylesheet"
href="https://fonts.googleapis.com/icon?family=Material+Icons">
   <link rel="stylesheet" href="materialize/css/materialize.min.css">
      <script type="text/javascript" src="https://code.jquery.com/jquery-
2.1.1.min.js"></script>
      <script src="/materialize/js/materialize.min.js"></script>
  </head>
  <body>
      <div class="card-panel teal lighten-2"><h3>Hello World!</h3></div>
      <div class="row">
   <div class="col s2 m4 l3">
      <p>This text will use 2 columns on a small screen, 4 on a medium screen,
and 3 on a large screen.</p>
   </div>
</div>
   </body>
</html>
