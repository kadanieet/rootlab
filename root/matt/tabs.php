<!DOCTYPE html>
<html>
   <head>
      <title>The Materialize Tabs Example</title>
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="stylesheet"
href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet"
href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" href="/materialize/css/materialize.min.css">
   <script type="text/javascript" src="/materialize/js/jquery-

1.5.2.min"></script>
   <script src="/materialize/js/materialize.min.js"></script>  
   </head>
   <body class="container">
   <h3>Tabs Demo</h3>
      <div class="row">
        <div class="col s12">
          <ul class="tabs">
            <li class="tab col s3"><a href="#inbox">Inbox</a></li>
            <li class="tab col s3"><a class="active"
            href="#unread">Unread</a></li>
            <li class="tab col s3 disabled"><a href="#outbox">Outbox
(Disabled)</a></li>
            <li class="tab col s3"><a href="#sent">Sent</a></li>
          </ul>
        </div>
        <div id="inbox" class="col s12">Inbox</div>
        <div id="unread" class="col s12">Unread</div>
        <div id="outbox" class="col s12">Outbox (Disabled)</div>
        <div id="sent" class="col s12">Sent</div>
      </div>
   </body>
</html>
