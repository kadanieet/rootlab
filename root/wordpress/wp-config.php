<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'acmebit');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost:3306');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '~U0Lkapuk}6Z{, mFaDlD~|sZ?P-=k2<VluJzwK#1{%71s4-T|7Oj%B(hiW[MynS');
define('SECURE_AUTH_KEY',  'Y%8!i`>:.{,w,lEw%w&!yj]Gt7Jp;ph;yjrEd-!&q&S h7AUb:do$NANz<BZxQ3R');
define('LOGGED_IN_KEY',    'usv!r!UyWZooapn5;x<Pr]5qi$a#-F0TKXIdc6<@3X9`ZS={Js[.sc{55Psdra/y');
define('NONCE_KEY',        'e(XP}~^/2y6noE#3DdpL-P/ww^%:`f`VBZ=~+pjiQ):-S%k+{Ms6-g4!$=evO7jd');
define('AUTH_SALT',        ')RDO)=!Xvq@Pt{bpwe/l.Sb!~;uS]%}WtoPZF(+KMMx/(<>.`W)lRV:LNZSWLn~V');
define('SECURE_AUTH_SALT', 'L_4s*ITmf:k1;&fS@o;m^GWp0j`m|x>3@Y.6 V2z9](yN2#~gxF>lX/g&sCi|y_h');
define('LOGGED_IN_SALT',   '#p8#96b1rY73C`&zKV.`:- mXpKo(,PW-oz}<_WB]IEI!2{Ecm<tJog>WhnK>hu6');
define('NONCE_SALT',       '[3sG50#}qo~Zv/]GYBwIkdN.6V #.zbvr#+h<@S$3pn)M*<*a#M&)O=BX;X|!aET');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
