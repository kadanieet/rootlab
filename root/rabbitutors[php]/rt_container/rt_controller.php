<?php

	//Utility Functions
		$rt_utils = $_SERVER['DOCUMENT_ROOT'] . 'rt_utils/util_fxns.php';

		if(file_exists($rt_utils))
			require_once $rt_utils;
		else
			exit('File ' .$rt_utils. 'is missing');

	Require_Files( UTILS . PREF . 'const_vars.php');

	class rt_controller
	{

		private $app;

		function __construct($value='')
		{
			$this->app = $value;
			/*if (function_exists('clean_input'))
					$this->app = $value;
			else
				exit;*/
		}

		/*This function takes an xml doc and an attribute search value to determine the location of
		a file resource*/
			private function get_rt_resource($search_value, $loc)
			{
				//load_xml_doc from the util_fxns
				$xmlDoc = load_xml_doc($loc);

				$searchNode = $xmlDoc->getElementsByTagName( "type" );

				foreach( $searchNode as $searchNode )
				{
				    $valueID = $searchNode->getAttribute('ID');

				    if($valueID == $search_value)
				    {

				    $xmlLocation = $searchNode->getElementsByTagName( "loc" );
				    return $xmlLocation->item(0)->nodeValue;
				    break;

				    }

				} exit("'". $search_value . "' key not found");
			}

		/*This function takes an xml doc with the data containing tag and any attribute defined.
		It processes the xml data into a html form options with the attribute as the value and tag value
		as the node value*/
			public function processXmlData($xmlDoc, $tag='', $attribute='initial')
			{
				$nodes = $xmlDoc->getElementsByTagName($tag);
				$options = array();
					foreach ($nodes as $node) {
						$init = $node->getAttribute($attribute);
						$options[] = "<option value='" . $init . "'>" . $node->nodeValue . "</option>";
					}
				return $options;
			}

		public function get_rt_app($search_value)
		{
			return $this->get_rt_resource($search_value, XML . PREF . 'app_locs.xml');
		}

		public function get_rt_data($search_value)
		{
			return $this->get_rt_resource($search_value, XML . PREF . 'data_locs.xml');
		}

		public function get_rt_credentials($search_value)
		{
			return $this->get_rt_resource($search_value, CONT_DIR. "rt_connections.xml");

		}

		public function get_rt_relations($search_value)
		{
			return $this->get_rt_resource($search_value, CONT_DIR. "rt_relations.xml");

		}

		function get_model($properties_array)
		{
			$app_loc = $this->get_rt_app($this->app);
		    Require_Files(MOD_DIR. $app_loc);
		    $class_array = get_declared_classes();
			$last_position = count($class_array) - 1;
			$class_name = $class_array[$last_position];
			$rt_object = new $class_name($properties_array);

			return $rt_object;
		}
	}
 ?>
