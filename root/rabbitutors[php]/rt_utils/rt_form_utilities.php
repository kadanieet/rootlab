<?php 
	function clean_input($value='')
		{
			//get rid of leading and trailing whitespaces
			$value = trim($value);
			
			$bad_chars = array("{", "}", "(", ")", ";", ":", "<", ">", "$");
			$value = str_ireplace($bad_chars, "", $value);

			//Removes any html from the string and turns it into &lt; format
			$value = htmlentities($value);

			//Strips html and PHP tags
			$value = strip_tags($value);

				if (get_magic_quotes_gpc()) {
					//Get rid of unwanted quotes
					$value = stripslashes($value);
				}
			return $value;
		}

 ?>



