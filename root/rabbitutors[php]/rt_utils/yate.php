<?php
      //Utility Functions
        $root = $_SERVER['DOCUMENT_ROOT'];
        $rt_utils = $root . 'rt_utils/util_fxns.php';

      if(file_exists($rt_utils))
        require_once $rt_utils;
      else
        exit('File ' .$rt_utils. 'is missing');

      Require_Files($root . 'rt_utils/rt_const_vars.php');
      Require_Files(CNTLR);

      function do_options($resource)
    	{
        $cntlr = new rt_controller();
    		$xmldoc = $cntlr->get_rt_data($resource);
    		$xmlDoc = load_xml_doc($xmldoc);
    		$options = $cntlr->processXmlData($xmlDoc, $resource);
    			foreach ($options as $opt)
    				echo $opt;
    	}
 ?>
