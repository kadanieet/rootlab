<header class="entry-header">
		<h2 class="entry-title">About</h2>
	</header>

	<div class="entry-content">
		<p>Rabbi tutoring group is an organization conceived by a brilliant young man together with three other friends who had a desire to impact the lives of other young men, from both primary and high schools, by enabling them to achieve their dreams and turning out to be leaders.</p>
		<p>Rabbi’s vision acts as a roadmap and framework to achieving a meaningful impact to students, parents and society at large, our aim being to constantly enable students to achieve and surpass their academic goals and become exceptional leaders. As Rabbi we create the best environment for employees and tutors to dispense their duties, and to create forums for impacting the less privileged students in the society.</p>
	</div>