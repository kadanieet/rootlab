<header class="entry-header">
		<h2 class="entry-title">Contact</h2>
	</header>

	<div class="entry-content">
		<p>You can contact us on:</p>
			<ul>
			<li>Telephone : +254 721 915 132</li>
			<li>Email: info@rabbitutors.co.ke</li>
			</ul>
	</div>