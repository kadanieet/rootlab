<header class="entry-header">
		<h2 class="entry-title">Register</h2>
	</header>

	<div class="entry-content">
		<h4>Enter your email and preferred username</h4>

		<form action="<? php echo $_SERVER['PHP_SELF']; ?>" method="post">
		<br/>
		<label for="username">Username</label>
		<input id="username" name="username" required="required" type="text" /><br />

		<label for="email">Email</label>
		<input id="email" name="email" required="required" type="email" /><br />

		<div class="row">
		    <select name="reg_type" required>
		    	<option value="" disabled selected>-Register Type-</option>
			  	<option value="C" >Client</option>
			  	<option value="T" >Tutor</option>
				</select>
		</div>

		<input name="next" type="submit" value="Next" />

		</form>

	</div>
