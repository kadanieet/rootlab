<h4>Tutoring Fee Information</h4>
<p>Number of Students: #</p>
<div id="student_fee_in4">
  Name in Class at School
  <table>
    <tr>
      <th>Subject</th><th>Number of Lessons</th><th>Fee per lesson</th><th>Total Fee of lessons</th>
    </tr>
    <tr>
      <td></td><td></td><td></td><td></td>
    </tr>
    <tr>
      <th>Total Fee Sum</th><td></td>
    </tr>
  </table>
</div>

<div id="fees_payed">
  Amount Payed
  <table>
    <tr>
    <th>Transaction Id</th><th>Date</th><th>Method</th><th>Amount</th>
    </tr>
    <tr>
      <td></td><td></td><td></td><td></td>
    </tr>
    <tr>
      <th>Total Amount Payed</th><td></td>
    </tr>
  </table>
</div>

<div id="balance">
  <p>Balance = Total Amount Payed - Total Fee Sum</p>
</div>
