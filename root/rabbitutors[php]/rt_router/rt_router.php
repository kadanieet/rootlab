<?php
  //check the origin of the request also
      if (isset($_GET['route'])) {
        //Utility Functions
          $rt_utils = $_SERVER['DOCUMENT_ROOT'] . 'rt_utils/util_fxns.php';

          if(file_exists($rt_utils))
            require_once $rt_utils;
          else
            exit('File ' .$rt_utils. 'is missing');


        Require_Files('../rt_utils/rt_const_vars.php');
        Require_Files(CNTLR);

        $cntlr = new rt_controller();

        $frm_util = $cntlr->get_rt_app('frm_util');
        Require_Files($frm_util);

        //get the requested route from the url
          $route = clean_input($_GET['route']);
          $app = $cntlr->get_rt_app($route);
          Redirect(SITE_ADDR . $app);
          
      } else exit("Bad request for route");
 ?>
