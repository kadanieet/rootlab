<?php 
	session_start();
	if(isset($_SESSION['user_id'])) {
		require_once $_SERVER['DOCUMENT_ROOT'] . 'vote/nav_menu.php';
	} 
	else {
		//redirect to the home page
	$home_url = 'http://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/';
	header('Location: ' . $home_url);
	}
		//Database connection	
	if (file_exists($_SERVER['DOCUMENT_ROOT'] .'connectvars.php') && file_exists($_SERVER['DOCUMENT_ROOT'] . 'connection.php')) {
	require_once($_SERVER['DOCUMENT_ROOT'] . 'connectvars.php');
	require_once($_SERVER['DOCUMENT_ROOT'] . 'connection.php');	
	}

	$req_connection = new Db_Connection(DB_HOST, DB_NAME, DB_USER, DB_PASS);

	$connection = $req_connection->connect();

	if (isset($_POST['proceed'])) {
		$prezzo_id = $_POST['president'];
		$aspirant_query = "SELECT surname, other_names from aspirants where aspirant_id=" .$prezzo_id. "";
		$asp_stmt = $connection->prepare($aspirant_query);
		$asp_stmt->execute();

		echo "<h3>Confirm Your Selection</h3>";
		echo "<p><b>Position : President</b></p>";
		echo "<form method='post' action='".$_SERVER['PHP_SELF']."'>";
		foreach ($asp_stmt as $row) {
			echo "<p>you selected ";
			echo "<label for='president'>";
			echo $row['surname']. " " .$row['other_names'];
			echo "</label>";
			echo "<input type='hidden' name='president' value='" .$prezzo_id. "'>";
			echo "</p>";
		}
		echo "<p class='error'>Once you click OK, Your vote will be finalized<p>";
		echo "<input type='submit' name='commit' value='OK'>";
		echo "</form>";
	}

	elseif (isset($_POST['commit'])) {
		$prezzo_id = $_POST['president'];

		$commit_query = "update aspirants set votes = votes + 1 where aspirant_id=" .$prezzo_id. ""; 
		$cmt_stmt = $connection->prepare($commit_query);
		$cmt_stmt->execute();
		
		//changed the voted status of the user to 1 and update the session value
		$voted_query = "UPDATE credentials SET voted = '1' WHERE user_id = '" . $_SESSION['user_id'] . "'";
		$vtd_stmt = $connection->prepare($voted_query);
		$vtd_stmt->execute();
		$_SESSION['voted'] = 1;

		echo "<p>Your Selections have been submitted, Kindly await the results. Redirecting to home.<p>";
		//redirect to the home page after 5 seconds
		$home_url = 'http://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/portal.php';
		header("refresh:5;url=" . $home_url);
	}
	else {
		$AspirantsQuery = "SELECT aspirant_id, surname, other_names from aspirants";

		$asp_stmt = $connection->prepare($AspirantsQuery);

		$asp_stmt->execute();

		//set the resulting array to associative
		$asp_data = $asp_stmt->fetchALL(PDO::FETCH_ASSOC);

		if (count($asp_data) != 0) {
			echo "<p><b>Position : President</b></p>";

				//set the checkbox for voting by checking the request mode
				
				if(isset($_GET['mode']))
				{
					
					echo "<form action='".$_SERVER['PHP_SELF']."' method='post'>";
						foreach ($asp_data as $row) {				
							echo "<p>Name " .$row['surname']. " " .$row['other_names']. "<input type='radio' value='".$row['aspirant_id']."' name='president' id='".$row['aspirant_id']."'></p>";
							//echo "<input type='hidden' name='surname' value='" .$row['surname']. "'>";
							//echo "<input type='hidden' name='other_names' value='" .$row['other_names']. "'>";
							//pass the table name too
						}
						echo "<input type='submit' name='proceed' value='Proceed'>";
						echo "</form>";
				}
			
				else
				{
					foreach ($asp_data as $row) {
						echo "<p>Name " .$row['surname']. " " .$row['other_names']. "</p>";
					}
				}
			
		}
	}
 ?>