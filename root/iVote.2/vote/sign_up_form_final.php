<form method="post" action="<?php /*session_start();*/ echo $_SERVER['PHP_SELF']; ?>">
		<input type="hidden" name="reg_no" value="<?php echo "$reg_no_1"; ?>">
		<input type="hidden" name="email" value ="<?php echo "$email"; ?>">
		
		<!--extra form elements depending on the file usage-->
			<?php if (isset($_SESSION['root']) && $_SESSION['root']) { ?> 
				<label for="reg_no">Registration Number</label>
					<input type="text" name="reg_no" value="<?php echo $reg_no; ?>" placeholder="Registration Number"><br>
			<?php } ?>

		<label for="surname">Surname</label>
			<input type="text" name="surname" value ="<?php echo "$surname"; ?>" placeholder="Surname"><br>
	    <label for="other_name">Other Name</label>
	    	<input type="text" name="other_name" value ="<?php echo "$other_name"; ?>" placeholder="Other Name"><br>

	    <!--extra form elements depending on the file usage-->
			<?php if (isset($_SESSION['root'])  && $_SESSION['root']) { ?> 
				<label for="position">Position</label>	
					<select name="position">
					    <option value="null" selected>---------Select--------</option>
					    		<?php 
									$xmlDoc = new DOMDocument();
									if(file_exists($_SERVER['DOCUMENT_ROOT'] . 'voted_congress_codes.xml')){
										$xmlDoc->load($_SERVER['DOCUMENT_ROOT'] . 'voted_congress_codes.xml');
										$node = $xmlDoc->getElementsByTagName("position");
										foreach ($node as $s_node) {
											$code = $s_node->getAttribute('code');
											echo "<option ";
											if ($position == $code) echo "selected='selected'";
											echo " value='" .$code. "'>".$s_node->nodeValue;
											echo "</option>";
										}
									} else {
										echo 'Error loading file';
										die();
									}
								
						 	?>
				    </select><br>
			<?php } ?>

	    <label for="study_year">Study Year</label>
	    	<select name="study_year">
			    <option value="null" <?php if ($study_year == "null") echo "selected='selected'"; ?> >--Select--</option>
				    <?php 
							$xmlDoc = new DOMDocument();
							
							if(file_exists($_SERVER['DOCUMENT_ROOT'] . 'schools_initials_years.xml')){
								$xmlDoc->load($_SERVER['DOCUMENT_ROOT'] .'schools_initials_years.xml');
								$node = $xmlDoc->getElementsByTagName("school");
								
								foreach ($node as $s_node) {
									$initial = $s_node->getAttribute('initial'); 
									$reg_arr = str_split(strtoupper($_SESSION['reg_no']));
									if (!isset($_SESSION['reg_no']))
											$reg_arr[0] = 'P';

									if($initial == $reg_arr[0]) { 
										$years = $s_node->getAttribute('years');
										$school = $reg_arr[0];

										foreach (range(1, $years) as $year) {
											echo "<option ";
											if ($study_year == $year) echo "selected='selected'";
											echo " value='" .$year. "'>".$year;
											echo "</option>";
										}
										break;		
									}
								}
						} else { exit('error loading file');}
						
				 	?>
			 </select><br>

		<input type="hidden" name="school" value='<?php echo "$school"; ?>'>
		
		<label for="resident">School Resident</label>
			<select name="resident">
		    	<option value="null" <?php if ($resident == "null") echo "selected='selected'"; ?> >--Select--</option>
			  	<option value="2" <?php if ($resident == "2") echo "selected='selected'"; ?> >No</option>
			  	<option value="1" <?php if ($resident == "1") echo "selected='selected'"; ?> >Yes</option>
			</select><br>

	<?php  
		if (isset($_GET['mode']) && $_GET['mode'] == 'edit') { }

		else {
	?>

	    <label for="gender">Gender</label>
		    <select name="gender">
			  <option value="null" <?php if ($gender == "null") echo "selected='selected'"; ?> >--Select--</option>
			  <option value="F" <?php if ($gender == "F") echo "selected='selected'"; ?> >Female</option>
			  <option value="M" <?php if ($gender == "M") echo "selected='selected'"; ?> >Male</option>
			</select><br>
	<?php 
			}
	 ?>
	    <label for="hostel">Hostel Name</label>
		    <select name="hostel">
			    <option value="null" selected>---------Select--------</option>
			    		<?php 
							$xmlDoc = new DOMDocument();
							if(file_exists($_SERVER['DOCUMENT_ROOT'] . 'hostels_codes.xml')){
								$xmlDoc->load($_SERVER['DOCUMENT_ROOT'] . 'hostels_codes.xml');
								$node = $xmlDoc->getElementsByTagName("hostel");
								foreach ($node as $s_node) {
									$code = $s_node->getAttribute('code');
									echo "<option ";
									if ($hostel == $code) echo "selected='selected'";
									echo " value='" .$code. "'>".$s_node->nodeValue;
									echo "</option>";
								}
							} else {
								echo 'Error loading file';
								die();
							}
						
				 	?>
		    </select><br>
	  <?php 
		  if (isset($_GET['mode']) && $_GET['mode'] == 'edit') {
		
		?>
			<label for="password_2">Enter Password to Confirm Changes : </label><br>
				<input type="password" name="password" ><br>
		    <input type="submit" name="update" value="Save Changes">
		<?php

		  } elseif (isset($_SESSION['root']) && $_SESSION['root']) {

		?>	
			<label for="user_image">Aspirant Image</label>
				<input type="file" name="user_image"><br>
			<label for="password_2">Enter Password to Add Aspirant : </label><br>
				<input type="password" name="password" ><br>
			<input type="submit" name="add" value="Register">
		<?php
		  }

		  else {
	   
	   ?>

		    <p>Create A Password for Your Account</p>
		    <label for="password_1">Enter Password</label>
		    	<input type="password" name="password_1" ><br>
			<label for="password_2">Re-Enter Password</label>
				<input type="password" name="password_2" ><br>
		    <input type="submit" name="create" value="Create Account">
		
		<?php 
			}
	    ?>
</form>