<?php 
	session_start();
	if(isset($_SESSION['user_id'])) {
		require_once $_SERVER['DOCUMENT_ROOT'] . 'vote/nav_menu.php';
		require_once $_SERVER['DOCUMENT_ROOT'] . 'vote/profile.php';
	} 
	else {
		//redirect to the home page
	$home_url = 'http://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/';
	header('Location: ' . $home_url);
	}
 ?>