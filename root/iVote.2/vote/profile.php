<?php 
	//session_start();

	if(isset($_SESSION['user_id'])) {
			//Database connection	
		if (file_exists($_SERVER['DOCUMENT_ROOT'] .'connectvars.php') && file_exists($_SERVER['DOCUMENT_ROOT'] . 'connection.php')) {
		require_once($_SERVER['DOCUMENT_ROOT'] . 'connectvars.php');
		require_once($_SERVER['DOCUMENT_ROOT'] . 'connection.php');	
		}

		$req_connection = new Db_Connection(DB_HOST, DB_NAME, DB_USER, DB_PASS);

		$connection = $req_connection->connect();

		//require_once $_SERVER['DOCUMENT_ROOT'] . 'vote/nav_menu.php';

		$VoterQuery = "SELECT surname, other_names, study_year, resident, hostel, gender FROM voters WHERE voter_id = '" .$_SESSION['user_id']. "'";

		$CredQuery = "SELECT reg_no, email, password FROM credentials where user_id = '" .$_SESSION['user_id']. "'";

		$voter_stmt = $connection->prepare($VoterQuery);
		$cred_stmt = $connection->prepare($CredQuery);

		$voter_stmt->execute();
		$cred_stmt->execute();

		foreach ($voter_stmt as $voter_row){
			$surname = $voter_row['surname'];
			$other_name = $voter_row['other_names'];
			$study_year = $voter_row['study_year'];
			$hostel = $voter_row['hostel'];
			$resident = $voter_row['resident'];
			$gender = $voter_row['gender'];
		}

		foreach ($cred_stmt as $cred_row) {
			$reg_no_1 = $cred_row['reg_no'];
			$email = $cred_row['email'];
			$password = $cred_row['password'];
		}

		/*if (isset($_POST['update'])) {
			//ouput the edit profile form status
			$output_edit_profile = 'no';

			$surname = clean_input($_POST['surname']);
			$other_name = clean_input($_POST['other_name']);
			$study_year = clean_input($_POST['study_year']);
			$resident = clean_input($_POST['resident']);
			$hostel = clean_input($_POST['hostel']);
			$password_1 = clean_input($_POST['password_1']);
		}

		/*if (isset($_GET['mode']) && $_GET['mode'] == 'edit') {
				require_once $_SERVER['DOCUMENT_ROOT'] . 'vote/sign_up_form_final.php';		
		}

		else {*/
?>
		
		<p>Name : <?php echo "$surname $other_name"; ?></p>
		<p>Registration Number : <?php echo "$reg_no_1"; ?></p>
		<p>Email : <?php echo "$email"; ?></p>
		<p>Gender : 
		<?php
			$xmlDoc = new DOMDocument();
					if(file_exists($_SERVER['DOCUMENT_ROOT'] . 'genders.xml')){
						$xmlDoc->load($_SERVER['DOCUMENT_ROOT'] . 'genders.xml');
						$node = $xmlDoc->getElementsByTagName("gender");
						foreach ($node as $s_node) {
							$code = $s_node->getAttribute('initial');
							if ($gender == $code)
								echo $s_node->nodeValue;
						}
					} else {
						echo 'Error loading file';
						die();
					}
		 ?> 	
		 </p>
		<p>Hostel : 
		<?php 
			$xmlDoc = new DOMDocument();
					if(file_exists($_SERVER['DOCUMENT_ROOT'] . 'hostels_codes.xml')){
						$xmlDoc->load($_SERVER['DOCUMENT_ROOT'] . 'hostels_codes.xml');
						$node = $xmlDoc->getElementsByTagName("hostel");
						foreach ($node as $s_node) {
							$code = $s_node->getAttribute('code');
							if ($hostel == $code)
								echo $s_node->nodeValue;
						}
					} else {
						echo 'Error loading file';
						die();
					}
		 ?>
		 	
		 </p>
		<p>Password : Not Changed <?php echo "<a href='pass_change.php'>Change</a>"; ?></p>

<?php
		//}
	} 
	else {
		//redirect to the home page
	$home_url = 'http://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/';
	header('Location: ' . $home_url);
	}
 ?>