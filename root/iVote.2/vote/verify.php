<?php 
	if(isset($_GET['email']) && !empty($_GET['email']) && isset($_GET['hash']) && !empty($_GET['hash'])){
			
			require_once $_SERVER['DOCUMENT_ROOT'] . 'vote/conn_common.php';
			require_once $_SERVER['DOCUMENT_ROOT'] . 'form_utilities.php';

			// Verify data
		    $email = clean_input($_GET['email']); // Set email variable
		    $hash = clean_input($_GET['hash']); // Set hash variable

			$qeury = "SELECT user_id, reg_no, email, voted FROM credentials where email = '" . $email . "' AND hash = '" . $hash . "'";

			$ver_stmt = $connection->prepare($qeury);
			$ver_stmt->execute();

			//set the resulting array to associative
			$row = $ver_stmt->fetchALL(PDO::FETCH_ASSOC);

			if (count($row) < 1) {
				//No match -> invalid url or account has already been activated.
        		echo '<div class="statusmsg">The url is either invalid or you already have activated your account.</div>';
			}

			else {
				session_start();

				foreach ($row as $row_1) {
						
							$_SESSION['user_id'] = $row_1['user_id'];
							$_SESSION['reg_no'] = $row_1['reg_no'];
							$_SESSION['email'] = $row_1['email'];
							$_SESSION['voted'] = $row_1['voted'];
							
						}
				//redirect to final sign up form
					$sign_up = 'http://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/index.php?sign=final';
					header('Location: ' . $sign_up);	
				}

	}

	else {
		//redirect to the home page
		$home_url = 'http://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/';
		header('Location: ' . $home_url);
	}

 ?>