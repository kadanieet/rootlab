<?php 
	session_start();

	// get the form_utilities file for cleaning user inputs
	require_once $_SERVER['DOCUMENT_ROOT'] . '/form_utilities.php';

	$output_pass_change_form = 'yes';
	
	if (isset($_POST['update']) && $output_pass_change_form = 'no') {
		$output_pass_change_form = 'no';
		
		$password_old = clean_input($_POST['password_old']);
		$password_new_1 = clean_input($_POST['password_new_1']);
		$password_new_2 = clean_input($_POST['password_new_2']);

		if (empty($password_old) || empty($password_new_1) || empty($password_new_2)) {
			$output_pass_change_form = 'yes';
			echo '<p class="error">Enter data in all fields!</p>';	
		}
		elseif ($password_new_1 != $password_new_2) {
			$output_pass_change_form = 'yes';
			echo '<p class="error">The new passwords entered do not match, try again!</p>';
		}
		else {
			require_once $_SERVER['DOCUMENT_ROOT'] . 'vote/conn_common.php';

			require_once $_SERVER['DOCUMENT_ROOT'] . 'vote/verify_pass.php';
			}		
	}

	if($output_pass_change_form = 'yes' && !isset($_POST['update'])) {
 ?>

<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
	<p>Password is case sensitive </p>
	    <label for="password_1">Enter Current Password</label>
	    	<input type="password" name="password_old" ><br>
	    <label for="password_1">Enter New Password</label>
	    	<input type="password" name="password_new_1" ><br>
		<label for="password_2">Re-Enter New Password</label>
			<input type="password" name="password_new_2" ><br>
	    <input type="submit" name="update" value="Update Password">
</form>

<?php 
	}
 ?>