
<?php
	/*session_start();
	if(isset($_SESSION['user_id'])) {
		//redirect to the home page
		$home_url = 'http://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/portal.php';
		header('Location: ' . $home_url);
	} 
	else {
		//redirect to the home page
		$home_url = 'http://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/';
		header('Location: ' . $home_url);
	}*/

	if (!empty($_GET['sign']) && isset($_GET['sign']) && $_GET['sign'] == 'final' && !isset($_POST['create'])) {
		//output the final sign up form only
			$output_sign_up_final_form = 'yes';
			$output_login_form = 'no';
			$output_sign_up_initial_form = 'no';

			echo "<p id='msg'>Enter your personal details to finalize your account</p>";

	} else {

			//form status
			$output_login_form = "yes";
			$output_sign_up_initial_form = "yes";
			$output_sign_up_final_form = 'no';
		}

	// get the form_utilities file for cleaning user inputs
	require_once $_SERVER['DOCUMENT_ROOT'] . '/form_utilities.php';

	//Database connection	
			if (file_exists($_SERVER['DOCUMENT_ROOT'] .'connectvars.php') && file_exists($_SERVER['DOCUMENT_ROOT'] . 'connection.php')) {
			require_once($_SERVER['DOCUMENT_ROOT'] . 'connectvars.php');
			require_once($_SERVER['DOCUMENT_ROOT'] . 'connection.php');	
			}

			$req_connection = new Db_Connection(DB_HOST, DB_NAME, DB_USER, DB_PASS);

			$connection = $req_connection->connect();

	//globals for user data 
	$reg_no_1 = "";
	$reg_no_2 = "";
	$email = "";
	$surname = "";
	$other_name = "";
	$study_year = "null";
	$resident = "null";
	$gender = "null";
	$hostel = "";
	$login_user = "";
	$reg_no = "";

	//On submit sign up form
	if(isset($_POST['signup'])){
		//form status
	$output_login_form = "no";
	$output_sign_up_initial_form = "no";
	$output_sign_up_final_form = 'no';

		//mysqli_real_escape_string($connection, $string);

		$reg_no_1 = strtoupper(clean_input($_POST['reg_no_1']));
		$reg_no_2 = strtoupper(clean_input($_POST['reg_no_2']));
		$email = clean_input($_POST['email']);

		if (empty($reg_no_1)) {
			echo "<p id='error'>You have forgotten to enter your registration number</p>";
			$output_login_form = 'yes';
			$output_sign_up_initial_form = 'yes';
		}

		elseif (empty($reg_no_2)) {
			echo "<p id='error'>You have forgotten to re-enter your registration number</p>";
			$output_login_form = 'yes';
			$output_sign_up_initial_form = 'yes';
		}

		elseif (strtoupper($reg_no_1) != strtoupper($reg_no_2)) {
			echo "<p id='error'>The registration numbers don't match</p>";
			$output_login_form = 'yes';
			$output_sign_up_initial_form = 'yes';
		}

		elseif (empty($email)) {
			echo "<p id='error'>You have forgotten to enter your email</p>";
			$output_login_form = 'yes';
			$output_sign_up_initial_form = 'yes';
		}

			
		elseif (!preg_match('/^[a-zA-Z0-9][a-zA-Z0-9\._\-&!?=#]*@/', $email)) {
		      // $email is invalid because LocalName is bad
		      echo '<p class="error">Your email address is invalid.</p>';
		      	$output_login_form = 'yes';
				$output_sign_up_initial_form = 'yes';
		}

			
			      
      // Strip out everything but the domain from the email and check if $domain is valid
	    elseif (preg_replace('/^[a-zA-Z0-9][a-zA-Z0-9\._\-&!?=#]*@/', '', $email) != 'students.ku.ac.ke') {
	         echo '<p class="error">Your email address is not a valid Kenyatta University Coporate email.</p>';
	         	$output_login_form = 'yes';
				$output_sign_up_initial_form = 'yes';
		}
					  	
		
		else {
	
			$query_reg = "SELECT user_id from credentials WHERE reg_no='" .$reg_no_1. "'";
			$query_email = "SELECT user_id from credentials WHERE email='"  .$email. "'";
			$reg_stmt = $connection->prepare($query_reg);
			$email_stmt = $connection->prepare($query_email);

			$reg_stmt->execute();
			$email_stmt->execute();

			//set the resulting array to associative
			$reg_row = $reg_stmt->fetchALL(PDO::FETCH_ASSOC);
			$email_row = $email_stmt->fetchALL(PDO::FETCH_ASSOC);
			

			if (count($reg_row) != 0 && count($email_row) == 0) {

				echo "<p id='error'>That registration number is already registered</p>";
				$output_login_form = 'yes';
				$output_sign_up_initial_form = 'yes';

			}

			elseif (count($email_row) != 0 && count($reg_row) == 0) {

				echo "<p id='error'>That coporate email is already registered</p>";
			 	$output_login_form = 'yes';
				$output_sign_up_initial_form = 'yes';

			 }

			 elseif (count($reg_row) != 0 && count($email_row) != 0) {
			  	echo "<p id='error'>Both the registration number and coporate email are already registered</p>";
			 	$output_login_form = 'yes';
				$output_sign_up_initial_form = 'yes';

			  } 

			else {
				//output the relevant forms
				$output_sign_up_final_form = 'no';
				$output_login_form = 'no';
				$output_sign_up_initial_form = 'no';


				// Generate random 32 character hash and assign it to a local variable.
				$hash = md5( rand(0,1000) );

				// Generate random number between 1000 and 5000 and assign it to a local variable.
				$pass = rand(1000,5000);

				$query = "INSERT INTO credentials(reg_no, email, password, hash) VALUES ('" .$reg_no_1."', '" .$email. "', SHA('" .$pass. "'), '" .$hash. "')";

				$verify_stmt = $connection->prepare($query);

				$verify_stmt->execute();

				//send a link to the coporate email to verify student
					$to      = $email; // Send email to our user
					$subject = 'Signup | Verification'; // Give the email a subject 
					$message = '
					 
					Thanks for signing up!
					Your account has been created, you can login with the following credentials after you have activated your account by pressing the url below.
					 
					------------------------
					Username: '.$reg_no_1.'
					Password: '.$pass.'
					------------------------
					 
					Please click this link to activate your account:
					http://' .$_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']). '/verify.php?email='.$email.'&hash='.$hash.'
					 
					'; // Our message above including the link
					                     
					//$headers = 'From:noreply@ivote.dev' . "\r\n"; // Set from headers
					mail($to, $subject, $message/*, $headers*/); // Send our email

					echo "<p id='msg'>Log in into your email to verify your account!</p>";

			}
		}

	} 

	//on submit log in form
	elseif (isset($_POST['login'])) {
		//form status
	$output_login_form = "no";
	$output_sign_up_initial_form = "no";
	$output_sign_up_final_form = 'no';

		$login_user = clean_input($_POST['reg_no']);
		$login_pass = clean_input($_POST['password']);

		if (empty($login_user)) {
			// login username is blank
		      echo '<p class="error">You forgot to enter your username.</p>';
		      $output_login_form = 'yes';
		      $output_sign_up_initial_form = 'yes';
		}

		elseif (empty($login_pass)) {
			// login password is blank
		      echo '<p class="error">You forgot to enter your password.</p>';
		      $output_login_form = 'yes';
		      $output_sign_up_initial_form = 'yes';
		}

		/*if(preg_match('/^[a-Z][1-3][0-9]\/\d{5}\/20[0null]\d{1}$/', $login_user)) {

		}*/

		else {
			if( $connection != null) {
				require_once $_SERVER['DOCUMENT_ROOT'] . 'vote/verify_pass.php';
				/*$login_query = "SELECT user_id,reg_no FROM credentials WHERE reg_no = '" .$login_user. "' AND password = SHA('"  .$login_pass. "')";
				$stmt = $connection->prepare($login_query);

				$stmt->execute();

				//set the resulting array to associative
				$row_1 = $stmt->fetchALL(PDO::FETCH_ASSOC);

				if(count($row_1) == 1) 
				{
					$output_login_form = 'no';
					foreach ($row_1 as $row) {
						session_start();
							$_SESSION['user_id'] = $row['user_id'];
							$_SESSION['reg_no'] = $row['reg_no'];
							
							$home_url = 'http://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/portal.php';
					  		header('Location: ' . $home_url);
					}
				}

				else {
				      echo '<p class="error">You have entered the wrong username or password, try again!</p>';
				      $output_login_form = 'yes';
				      $output_sign_up_initial_form = 'yes';
				}*/

			}
		}
	}

	elseif (isset($_POST['create'])) {
		//form status
		$output_login_form = "no";
		$output_sign_up_initial_form = "no";
		$output_sign_up_final_form = 'no';

		/*
		$reg_no = clean_input($_POST['reg_no']);
		//Sync the registration numbers for final and initial sign ups
			if (!empty($reg_no)) {
				$reg_no_1 = $reg_no;
			} else {
				$reg_no_1 = "";
			}

		$email = clean_input($_POST['email']);
		*/
		//$reg_no = $_SESSION['reg_no']);
		//$email = $_SESSION['email']);

		$surname = clean_input($_POST['surname']);
		$other_name = clean_input($_POST['other_name']);
		$study_year = clean_input($_POST['study_year']);
		$school = clean_input($_POST['school']);
		$resident = clean_input($_POST['resident']);
		$gender = clean_input($_POST['gender']);
		$hostel = clean_input($_POST['hostel']);
		$password_1 = clean_input($_POST['password_1']);
		$password_2 = clean_input($_POST['password_2']);

				if($surname == "") {
					echo "<p id='error'>You have forgotten to enter your surname</p>";
						$output_sign_up_final_form = 'yes';
				} 
				elseif($other_name == "") {
					echo "<p id='error'>You have forgotten to enter your other name</p>";
						$output_sign_up_final_form = 'yes';
				} 
				elseif($study_year == "null") {
					echo "<p id='error'>You have forgotten to select your study year</p>";
						$output_sign_up_final_form = 'yes';
				} 
				elseif($resident == "null") {
					echo "<p id='error'>You have forgotten to select your residence</p>";
						$output_sign_up_final_form = 'yes';
				} 
				elseif($gender == "null") {
					echo "<p id='error'>You have forgotten to select your gender</p>";
						$output_sign_up_final_form = 'yes';
				}  
				elseif($hostel == "null" && $resident != 2) {
					echo "<p id='error'>You have forgotten to select your hostel</p>";
						$output_sign_up_final_form = 'yes';
				} 
				elseif($password_1 == "" || $password_2 == "") {
					echo "<p id='error'>You have forgotten to enter your passwords</p>";
						$output_sign_up_final_form = 'yes';
				}  
				elseif($password_1 != $password_2) {
					echo "<p id='error'>The passwords don't match</p>";
						$output_sign_up_final_form = 'yes';
				}

				else{
					if (session_status() == PHP_SESSION_NONE) {
					    session_start();
					    //echo "string";
					}

					//update the password in the credentials table
					$cred_q = "UPDATE credentials SET active = 1, password = SHA('" .$password_1. "') WHERE user_id = '" . $_SESSION['user_id'] . "'";
					$cred_stmt = $connection->prepare($cred_q);
					$cred_stmt->execute();

					if ($hostel == "null" && $resident == 2) {
						//update details in the voters table
						$voter_q = "INSERT INTO voters VALUES ('" . $_SESSION['user_id'] . "', '" . $surname . "', '" . $other_name . "', " . $study_year . ", '" . $school . "', '" . $gender . "', '" . $resident . "', " . strtoupper($hostel) . ")";
					}
					else {
						$voter_q = "INSERT INTO voters VALUES ('" . $_SESSION['user_id'] . "', '" . $surname . "', '" . $other_name . "', " . $study_year . ", '" . $school . "', '" . $gender . "', '" . $resident . "', '" . $hostel . "')";
					}
					

					$voter_stmt = $connection->prepare($voter_q);
					$voter_stmt->execute();


					echo "<p id='msg'>Account created successfully, redirecting to the portal</p>";
					// Redirect to the portal after 5 seconds
					  $home_url = 'http://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/portal.php';
					  header("refresh:5;url=" . $home_url);

				}
	} 

	if ($output_login_form == 'yes' && $output_sign_up_initial_form == 'yes' && $output_sign_up_final_form == 'no') {
		
		require_once $_SERVER['DOCUMENT_ROOT'] . 'vote/login_form.php';
		require_once $_SERVER['DOCUMENT_ROOT'] . 'vote/sign_up_form_initial.php';
	}

	if($output_sign_up_final_form == 'yes' && $output_login_form == 'no' && $output_sign_up_initial_form == 'no') {
		session_start();

		require_once $_SERVER['DOCUMENT_ROOT'] . 'vote/sign_up_form_final.php';
	}

	else {
		$output_login_form = 'yes';

		$output_sign_up_initial_form = 'yes';

		$output_sign_up_final_form = 'no';
	}

	//$req_connection->disconnect();
 ?>