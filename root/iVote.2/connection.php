<?php 
	class Db_Connection
	{
		// initialize the connection variables as null
		private $db_host = null;
		private $db_name = null;
		private $db_user = null;
		private $db_pass = null;
		private $conn = null;

		function __construct($value1, $value2, $value3, $value4)
		{
			$this->db_host = $value1;
			$this->db_name = $value2;
			$this->db_user = $value3;
			$this->db_pass = $value4;	
		}

		function connect()
		{
			try {
				$this->conn = new PDO("mysql:host=$this->db_host;dbname=$this->db_name", $this->db_user, $this->db_pass);
				
				//Set the PDO error mode to exception
				$this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

				// return the connection instance
				return $this->conn;
				}

				catch(PDOException $e)
				{
					echo "Connection failed:" .$e->getMessage();
					//return $conn;
				}
			return $this->conn;
		}

		function disconnect()
		{
			if($this->conn != null)
				$this->conn = null;
			return true;
		}
	}
 ?>