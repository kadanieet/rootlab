/*With the root user password table*/
CREATE DATABASE  ivote;

USE ivote;

CREATE TABLE voters(
	voter_id MEDIUMINT NOT NULL PRIMARY KEY,
	surname VARCHAR(15) NOT NULL,
	other_names VARCHAR(15) NOT NULL,
	study_year TINYINT NOT NULL,
	school VARCHAR(1) NOT NULL,
	gender VARCHAR(1) NOT NULL,
	resident VARCHAR(3) NOT NULL,
	hostel VARCHAR(3)
);

/*less writes but more reads*/
CREATE TABLE aspirants(
	aspirant_id SMALLINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	image_id VARCHAR(10) NOT NULL,
	reg_no VARCHAR(20) NOT NULL,
	surname VARCHAR(15) NOT NULL,
	other_names VARCHAR(15) NOT NULL,
	position VARCHAR(4) NOT NULL,
	gender VARCHAR(1) NOT NULL,
	study_year TINYINT NOT NULL,
	school VARCHAR(1) NOT NULL,
	resident VARCHAR(3) NOT NULL,
	hostel VARCHAR(3),
	votes MEDIUMINT NOT NULL DEFAULT 0
);

/*less writes but more reads*/
CREATE TABLE credentials(
	user_id SMALLINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	reg_no VARCHAR(15) NOT NULL,
	email VARCHAR(30) NOT NULL,
	password VARCHAR(40) NOT NULL,
	hash VARCHAR(32) NOT NULL,
	voted MEDIUMINT NOT NULL DEFAULT 0
	active MEDIUMINT NOT NULL DEFAULT 0
);

CREATE TABLE admin(
	password VARCHAR(40) NOT NULL
);

CREATE TABLE root(
	password VARCHAR(40) NOT NULL
);

