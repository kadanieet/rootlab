<?php 
	//delete the session variables to log out
	session_start();
	if (isset($_SESSION['root'])) {
		$_SESSION = array();

		session_destroy();
	}

	//redirect to the home page
	$home_url = 'http://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/';
	header('Location: ' . $home_url);

 ?>