<?php
	session_start();
	if(isset($_SESSION['root'])) {
		require_once $_SERVER['DOCUMENT_ROOT'] . '/root_nav_menu.php';
	} 
	else {
		//redirect to the home page
	$home_url = 'http://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/';
	header('Location: ' . $home_url);
	}
	//globals for user data 
	$reg_no_1 = "";
	$reg_no_2 = "";
	$email = "";
	$surname = "";
	$position = "";
	$other_name = "";
	$study_year = "null";
	$resident = "null";
	$gender = "null";
	$hostel = "";
	$login_user = "";
	$reg_no = "";

	if (isset($_POST['add'])) {
		$output_form = 'no';
		require_once $_SERVER['DOCUMENT_ROOT'] . '/form_utilities.php';

		$reg_no = clean_input($_POST['reg_no']);
		$email = clean_input($_POST['email']);
		$surname = clean_input($_POST['surname']);
		$other_name = clean_input($_POST['other_name']);
		$position = clean_input($_POST['position']);
		$study_year = clean_input($_POST['study_year']);
		$resident = clean_input($_POST['resident']);
		$gender = clean_input($_POST['gender']);
		$hostel = clean_input($_POST['hostel']);
		$password = clean_input($_POST['password']);

		$image_id = $_POST['user_image'];

		$asp_query = "SELECT reg_no FROM aspirants WHERE reg_no = '" .$reg_no. "'";

		$ins_asp_query = "INSERT INTO aspirants 
	(image_id, reg_no, surname, other_names, study_year, school,  resident, hostel, room_no)
	VALUES ('" .$image_id. "', '" .$reg_no_1. "', '" .$surname. "', '" .$other_name. "', " .$study_year. ", '" .$resident. "','" .$hostel. "')";

		$root_query = "SELECT password from root where password = SHA('" .$password. "')";


				if (empty($reg_no)) {
					echo "<p id='error'>You have forgotten to enter the registration number</p>";
					$output_form = 'yes';
				}
				elseif($surname == "") {
						echo "<p id='error'>You have forgotten to enter the surname</p>";
						$output_form = 'yes';
				} 
				elseif($other_name == "") {
					echo "<p id='error'>You have forgotten to enter the other name</p>";
						$output_form = 'yes';
				}
				elseif($position == "null") {
					echo "<p id='error'>You have forgotten to select the position</p>";
						$output_form = 'yes';
				} 
				elseif($study_year == "null") {
					echo "<p id='error'>You have forgotten to select the study year</p>";
						$output_form = 'yes';
				} 
				elseif($resident == "null") {
					echo "<p id='error'>You have forgotten to select the residence</p>";
						$output_form = 'yes';
				} 
				elseif($gender == "null") {
					echo "<p id='error'>You have forgotten to select the gender</p>";
						$output_form = 'yes';
				}  
				elseif($hostel == "null" && $resident == 'yes') {
					echo "<p id='error'>You have forgotten to select the hostel</p>";
						$output_form = 'yes';
				}
				elseif($password == ""){
					echo "<p id='error'>Enter your password to confirm</p>";
						$output_form = 'yes';
				}
				else {
					if (file_exists($_SERVER['DOCUMENT_ROOT'] .'connectvars.php') && file_exists($_SERVER['DOCUMENT_ROOT'] . 'connection.php')) {
								require_once($_SERVER['DOCUMENT_ROOT'] . 'connectvars.php');
								require_once($_SERVER['DOCUMENT_ROOT'] . 'connection.php');	
							} else {
								exit('file error');
							}

						$req_connection = new Db_Connection(DB_HOST, DB_NAME, DB_USER, DB_PASS);

						$connection = $req_connection->connect();
						
					//check if the registration number has been used again
					$asp_stmt = $connection->prepare($asp_query);

					$asp_stmt->execute();

					$asp_res = $asp_stmt->fetchALL(PDO::FETCH_ASSOC);

					//check if the root password is correct
					$root_stmt = $connection->prepare($root_query);

					$root_stmt->execute();

					$root_res = $root_stmt->fetchALL(PDO::FETCH_ASSOC);

					if (count($asp_res) != 0) {
						echo "<p id='error'>The registration number has already been used</p>";
						$output_form = 'yes';
					}
					elseif (count($root_res == 0)) {
						echo "<p id='error'>Incorrect password, # attempts remaining</p>";
							$output_form = 'yes';
					}
				}
		
	} else {
		$output_form = 'yes';
	}


	if ($output_form == 'yes') {
		require_once $_SERVER['DOCUMENT_ROOT'] . '/vote/sign_up_form_final.php';
	}
 ?>