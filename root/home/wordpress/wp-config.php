<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'test');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '{LaS#;.<@A6z0oxmaIy%4/-r A~>>yGQnk.*uu&}]ZBQfCRg9YEpPSb*.q%}Qk:r');
define('SECURE_AUTH_KEY',  'ypq:lf8o5DyMWuDaJ[ga?0/h<H-#FJ&oS1)5A(Jt1-/Li`s3CTp{-,Q.Gy<)}L&y');
define('LOGGED_IN_KEY',    '-@sT+R}0~|yc-u;Q9n%mp`A%Yt^VLMswV?zg@UA<9I625sJmN2?{.+m@  AP=-$t');
define('NONCE_KEY',        '1S]q 1snWEW2}/2-UzjFcG7*$mn3HI#jy]ufg0c^.v!g?T_7Js$Se-$6a/E[sdaY');
define('AUTH_SALT',        'xdhfL#ut?}z1?Mbb~LNQTn[m>jJ[{4gJKf/!ERfpw4Z4U-7~Z@$Qo:bWupX/]ap{');
define('SECURE_AUTH_SALT', 'q<I<m{!(=X*fHf;w3)Mwbt3?Bl>fV#cal}/#CNe2)aDrhg^(Pm~+h6/XJxTrOJ+&');
define('LOGGED_IN_SALT',   'b?O:M)r26 ~>UX?1yyFqC2,;_2Y;aUPIgvtDn*]Z{{y0H4xLllk=!A-)u>bhUh].');
define('NONCE_SALT',       '0+>?#qn|Rr+(:FeMeNv9;w{@U;g.&0]YbQZs(%UqB,{OE*6;E*f!Sw;Oy}-FaW4e');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
