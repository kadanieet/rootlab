<?php 
	/**
	* 
	*/
	$util_fxs = 'control/utility_functions.php';
      if (file_exists($util_fxs))
        require_once $util_fxs;
      else
        exit('utility funtions files unvailable');

	Require_Files('control/controller.php');

	class login
	{
		private $user;
		private $pass;

		private $db_host;
		private $db_user;
		private $db_pass;
		private $db_name;

		private $tb_name;

		function __construct($properties_array)
		{
			if (/*method_exists('voter_container', 'get_model') &&*/ is_array($properties_array)) {
				$this->user = $properties_array[0];
				$this->pass = $properties_array[1];
			} else exit('no method get_model or array not proper');
		}

		function verify_login()
		{
			$controller = new controller('connection_model');
			$this->set_credentials($controller);
			$this->set_relation($controller, 'credentials');
			$properties_array = array($this->db_host, $this->db_user, $this->db_pass, $this->db_name);
			//echo $this->db_user;
			$connection = $controller->get_rt_model($properties_array);
			$connection->connect();
			$useridpasswords_array = $connection->SelectQuery("SELECT password, username, active FROM " .$this->tb_name. "");
			$json = json_encode($useridpasswords_array);
			$valid_useridpasswords = json_decode($json, TRUE);

			$url = '';
			foreach($valid_useridpasswords as $user)
			{
			    if (in_array($this->user, $user))
				{					
						if(sha1($this->pass) == $user['password'])
						{
							if($user['active'] == '1')
								$url = 'portal.php';

							elseif($user['active'] == '0')
								$url = 'final_reg.php';

							//$_SESSION['reg_no'] = $this->user;
							//$_SESSION['password'] = $this->pass;
							//$_SESSION['voted'] = $user['voted'];

							return $url;
						}
								 
						
				}
						
			}	
			return $url;
		}

		private function set_credentials($controller)
		{
			
			$this->db_host = $controller->get_rt_credentials('db_host');
			$this->db_user = $controller->get_rt_credentials('db_user');
			$this->db_pass = $controller->get_rt_credentials('db_pass');
			$this->db_name = $controller->get_rt_credentials('db1_name');
		}


		private function set_relation($controller, $rel_name)
		{
			$this->tb_name = $controller->get_rt_relations($rel_name);
		}
	}
 ?>