<?php 
	class Db_Connection
	{
		// initialize the connection variables as null
		private $db_host = null;
		private $db_name = null;
		private $db_user = null;
		private $db_pass = null;
		private $conn = null;
		private $result = null; 

		function __construct($properties_array)
		{
			$this->db_host = $properties_array[0];
			$this->db_name = $properties_array[3];
			$this->db_user = $properties_array[1];
			$this->db_pass = $properties_array[2];	
		}

		function connect()
		{
			try {
				$this->conn = new PDO("mysql:host=$this->db_host;dbname=$this->db_name", $this->db_user, $this->db_pass);
				
				//Set the PDO error mode to exception
				$this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

				// return the connection instance
				return $this->conn;
				}

				catch(PDOException $e)
				{
					echo "Connection failed:" .$e->getMessage();
					//return $conn;
				}
			return $this->conn;
		}

		function SelectQuery($query)
		{
			$stmt = $this->conn->prepare($query);
			$stmt->execute();
			$this->result = $stmt->fetchALL(PDO::FETCH_ASSOC);
			return $this->result;
		}

		function InsertQuery($query)
		{
			$stmt = $this->conn->prepare($query);
			$stmt->execute();
			/*$this->result = $stmt->fetchALL(PDO::FETCH_ASSOC);
			return $this->result;*/
		}

		function UpdateQuery($query)
		{
			$stmt = $this->conn->prepare($query);
			$stmt->execute();
			/*$this->result = $stmt->fetchALL(PDO::FETCH_ASSOC);
			return $this->result;*/
		}

		function disconnect()
		{
			if($this->conn != null)
				$this->conn = null;
			return true;
		}

		function __destruct()
		{
			if($this->conn != null)
				$this->conn = null;
			return true;
		}
	}
 ?>