<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>RABBI TUTORS</title>
  
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">

  <link rel='stylesheet prefetch' href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900|RobotoDraft:400,100,300,500,700,900'>
<link rel='stylesheet prefetch' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css'>

      <link rel="stylesheet" href="css/style.css">

  
</head>

<body>
  
<!-- Mixins-->
<!-- Pen Title-->
<div class="pen-title">
  <h1>Rabbi Tutors Login</h1>
</div>
<div class="container">
  <div class="card"></div>
  <div class="card">
    <h1 class="title">Login</h1>

<?php 
  //globals for login user data   
    $login_user = "";

    //Require files modules
        $util_fxs = 'control/utility_functions.php';
      if (file_exists($util_fxs))
        require_once $util_fxs;
      else
        exit('utility funtions files unvailable');
    
  //log in form submit handler
    if (isset($_POST['uname']) && isset($_POST['pword'])) {
      //form status
        //Output_Form_Status();
      if (empty($_POST['uname']) || empty($_POST['pword'])) 
            Print_Message("You forgot to enter your Username or Password<br/>");

      elseif (!(preg_match("/^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).*$/", $_POST['pword'])))
          Print_Message("Invalid Password Format");

      else
      {
        //get the form_utilities file for cleaning user inputs, connectvars and connection files
          $form_utils = 'control/form_utilities.php';
          Require_Files($form_utils);

        GLOBAL $uname;

        $uname = clean_input($_POST['uname']);
        $pword = clean_input($_POST['pword']);
        
        $cntrl = 'control/controller.php';
        Require_Files($cntrl);

        $controller = new controller(clean_input($_POST['mode']));
        $properties_array = array($uname, $pword);
        $login_model = $controller->get_rt_model($properties_array);
        $go_to = $login_model->verify_login();
        Redirect('http://rabbitutors.dev/rabbi login/'. $go_to);
        //Output_Form_Status('yes', 'yes');         
      }
    } 
 ?>


    <form method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>">
      <div class="input-container">
        <input type="text" id="username" name="uname" required="required"/>
        <label for="#{label}">Username</label>
        <div class="bar"></div>
      </div>
      <div class="input-container">
        <input type="password" id="pword" name="pword" required="required"/>
        <label for="#{label}">Password</label>
        <div class="bar"></div>
      </div>
        <input type="hidden" id="mode" name="mode" value="user_login" />
      <div class="button-container">
        <button><span>Go</span></button>
      </div>
      <div class="footer"><a href="#">Forgot your password?</a></div>
    </form>
  </div>
</div>
<!-- Portfolio--><a id="portfolio" href="http://andytran.me/" title="View my portfolio!"><i class="fa fa-link"></i></a>
<!-- CodePen--><a id="codepen" href="https://kreatnet.org//Devit" title="Follow me!"><i class="fa fa-codepen"></i></a>
  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

    <script  src="js/index.js"></script>

</body>
</html>
