<?php 
	/**
	* 
	*/
		$util_fxs = 'control/utility_functions.php';
      if (file_exists($util_fxs))
        require_once $util_fxs;
      else
        exit('utility funtions files unvailable');

	class controller
	{
	
		private $app;
		private $rt_file;

		function __construct($value)
		{
			if (function_exists('clean_input'))
					$this->app = $value;
			else
				exit;
		}

		private function get_rt_resource($search_value, $loc)
		{
			$xmlDoc = load_xml_doc($loc);

			$searchNode = $xmlDoc->getElementsByTagName( "type" ); 

			foreach( $searchNode as $searchNode ) 
			{ 
			    $valueID = $searchNode->getAttribute('ID'); 
			    
			    if($valueID == $search_value)
			    {

			    $xmlLocation = $searchNode->getElementsByTagName( "location" ); 
			    return $xmlLocation->item(0)->nodeValue;
			    break;

			    }

			}
		}

		public function get_rt_application($search_value)
		{
			return $this->get_rt_resource($search_value, 'control/rabbi_apps.xml');
		}

		public function get_rt_credentials($search_value)
		{
			return $this->get_rt_resource($search_value, "control/rabbi_conns.xml");
		
		}

		public function get_rt_relations($search_value)
		{
			return $this->get_rt_resource($search_value,  "control/rabbi_rels.xml");
		
		}

		function get_rt_model($properties_array)
		{
			$login_loc = $this->get_rt_application($this->app);
		    Require_Files('model/'. $login_loc);
		    $class_array = get_declared_classes();
			$last_position = count($class_array) - 1;
			$class_name = $class_array[$last_position];	
			$voter_object = new $class_name($properties_array);
			
			return $voter_object;
		}
	}
 ?>