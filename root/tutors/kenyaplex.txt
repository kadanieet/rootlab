https://www.kenyaplex.com/tutors/
Kenyaplex makes it easy for students and parents to find tutors and private teachers for online or home tutoring.
We have a large database of trainers and tutors for all levels such as pre-primary school, primary school, high school, college, univerisity undergraduate and postgraduate levels.
Regardless of your location in Kenya, you will find a suitable tutor for any subject, discipline or skill.