from distutils.core import setup

setup(
        name = 'print_lol',
        version = '1.0.0',
        py_modules = ['print_lol'],
        author = 'The Architect',
        author_email = 'muffwaindan@gmail.com',
        url = 'https://www.facebook.com/KadanieeT',
        description = 'A simple printer of nested lists',
    )
