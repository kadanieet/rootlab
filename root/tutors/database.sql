CREATE DATABASE IF NOT EXISTS rt_credentials;

USE rt_credentials;

CREATE TABLE IF NOT EXISTS rt_params
    (
        user_id MEDIUMINT NOT NULL,
        username VARCHAR(20) NOT NULL,
        email VARCHAR(30) NOT NULL,
        hash VARCHAR(40) NOT NULL,
        password VARCHAR(40) NOT NULL,
        active SMALLINT NOT NULL DEFAULT 0
    );

CREATE DATABASE IF NOT EXISTS db_rabiitutors;

USE db_rabiitutors;

CREATE TABLE IF NOT EXISTS rt_tutors
    (
        tutor_id MEDIUMINT NOT NULL,
        id_no INT NOT NULL,
        surname VARCHAR(10) NOT NULL,
        middle_name VARCHAR(10),
        first_name VARCHAR(10) NOT NULL,
        gender VARCHAR(1) NOT NULL,
        religion VARCHAR(1) NOT NULL,
        occupation VARCHAR(20),
        organization VARCHAR(30),
        hobbies VARCHAR(160) NOT NULL,
        tsc_no int,
        phone VARCHAR(10) NOT NULL,
        email VARCHAR(30),
        residence VARCHAR(10) NOT NULL,
        PRIMARY KEY (tutor_id)
    );
    
CREATE TABLE IF NOT EXISTS rt_tutor_scores
    (
    	s_no INT AUTO_INCREMENT,
        tutor_id MEDIUMINT NOT NULL,
        kcse_score VARCHAR(2) NOT NULL,
        s1 VARCHAR(2),
        s2 VARCHAR(2),
        s3 VARCHAR(2),
        s4 VARCHAR(2),
        s5 VARCHAR(2),
        s6 VARCHAR(2),
        s7 VARCHAR(2),
        s8 VARCHAR(2),
        s9 VARCHAR(2),
        s10 VARCHAR(2),
        s11 VARCHAR(2),
        s12 VARCHAR(2),
        PRIMARY KEY (s_no),
        CONSTRAINT tutor_score 
            FOREIGN KEY(tutor_id) 
                REFERENCES rt_tutors(tutor_id)
    );
    
CREATE TABLE IF NOT EXISTS rt_clients
    (
    	client_id MEDIUMINT NOT NULL,
        id_no INT NOT NULL,
        surname VARCHAR(10) NOT NULL,
        middle_name VARCHAR(10),
        first_name VARCHAR(10) NOT NULL,
        gender VARCHAR(1) NOT NULL,
        religion VARCHAR(1) NOT NULL,
        email VARCHAR(30),
        phone VARCHAR(10) NOT NULL,
        residence VARCHAR(10) NOT NULL,
        PRIMARY KEY (client_id)
    );
        
CREATE TABLE IF NOT EXISTS rt_students
    (
        student_id MEDIUMINT NOT NULL,
        surname VARCHAR(10) NOT NULL,
        middle_name VARCHAR(10),
        first_name VARCHAR(10) NOT NULL,
        gender VARCHAR(1) NOT NULL,
        religion VARCHAR(1) NOT NULL,
        school VARCHAR(4) NOT NULL,
        hobbies VARCHAR(160) NOT NULL,
        client_id MEDIUMINT NOT NULL,
        PRIMARY KEY (student_id),
        CONSTRAINT client_student 
            FOREIGN KEY (client_id) 
                REFERENCES rt_clients (client_id)
    );
    
CREATE TABLE IF NOT EXISTS rt_primary_academics
    (
    	s_no INT,
        student_id MEDIUMINT NOT NULL,
        class TINYINT NOT NULL,
        s1_score TINYINT NOT NULL DEFAULT 0,
        s2_score TINYINT NOT NULL DEFAULT 0,
        s3_score TINYINT NOT NULL DEFAULT 0,
        s4_score TINYINT NOT NULL DEFAULT 0,
        s5_score TINYINT NOT NULL DEFAULT 0,
        s6_score TINYINT NOT NULL DEFAULT 0,
        s7_score TINYINT NOT NULL DEFAULT 0,
        s8_score TINYINT NOT NULL DEFAULT 0,
        s9_score TINYINT NOT NULL DEFAULT 0,
        s10_score TINYINT NOT NULL DEFAULT 0,
        s11_score TINYINT NOT NULL DEFAULT 0,
        s12_score TINYINT NOT NULL DEFAULT 0,
        PRIMARY KEY (s_no),
        CONSTRAINT primary_student 
            FOREIGN KEY (student_id) 
                REFERENCES rt_students (student_id)
    );
    
CREATE TABLE IF NOT EXISTS rt_secondary_academics
    (
    	s_no INT,
        student_id MEDIUMINT NOT NULL,
        form TINYINT NOT NULL,
        s1_score TINYINT NOT NULL DEFAULT 0,
        s2_score TINYINT NOT NULL DEFAULT 0,
        s3_score TINYINT NOT NULL DEFAULT 0,
        s4_score TINYINT NOT NULL DEFAULT 0,
        s5_score TINYINT NOT NULL DEFAULT 0,
        s6_score TINYINT NOT NULL DEFAULT 0,
        s7_score TINYINT NOT NULL DEFAULT 0,
        s8_score TINYINT NOT NULL DEFAULT 0,
        s9_score TINYINT NOT NULL DEFAULT 0,
        s10_score TINYINT NOT NULL DEFAULT 0,
        s11_score TINYINT NOT NULL DEFAULT 0,
        s12_score TINYINT NOT NULL DEFAULT 0,
        PRIMARY KEY (s_no),
        CONSTRAINT secondary_student 
            FOREIGN KEY (student_id) 
                REFERENCES rt_students (student_id)
    );

CREATE TABLE IF NOT EXISTS rt_interested_subjects
    (
    	s_no INT,
        student_id MEDIUMINT NOT NULL,
        s1 VARCHAR(2) NOT NULL,
        s2 VARCHAR(2),
        s3 VARCHAR(2),
        s4 VARCHAR(2),
        s5 VARCHAR(2),
        s6 VARCHAR(2),
        s7 VARCHAR(2),
        s8 VARCHAR(2),
        s9 VARCHAR(2),
        s10 VARCHAR(2),
        s11 VARCHAR(2),
        s12 VARCHAR(2),
        PRIMARY KEY (s_no),
        CONSTRAINT student_subjects 
            FOREIGN KEY (student_id) 
                REFERENCES rt_students (student_id)
    );
    
CREATE TABLE IF NOT EXISTS rt_classes
    (
        class_id INT NOT NULL,
        tutor_id MEDIUMINT NOT NULL,
        student_id MEDIUMINT NOT NULL,
        class_date DATE NOT NULL,
        start_time TIME NOT NULL,
        end_time TIME NOT NULL,
        subject VARCHAR(2) NOT NULL,
        topics VARCHAR(8) NOT NULL,
        PRIMARY KEY (class_id),
        CONSTRAINT student_classes 
            FOREIGN KEY (student_id) 
                REFERENCES rt_students (student_id),
        CONSTRAINT tutor_classes 
            FOREIGN KEY (tutor_id) 
                REFERENCES rt_tutors (tutor_id)
    );
    
CREATE TABLE IF NOT EXISTS rt_progress
    (
    	s_no INT,
        tutor_id MEDIUMINT NOT NULL,
        student_id MEDIUMINT NOT NULL,
        cat1 TINYINT NOT NULL,
        cat2 TINYINT NOT NULL,
        exam TINYINT NOT NULL,
        PRIMARY KEY (s_no),
        CONSTRAINT student_progress 
            FOREIGN KEY (student_id) 
                REFERENCES rt_students (student_id),
        CONSTRAINT student_tutor 
            FOREIGN KEY (tutor_id) 
                REFERENCES rt_tutors (tutor_id)
    );