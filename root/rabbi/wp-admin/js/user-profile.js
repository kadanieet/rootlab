/* global ajaxurl, pwsL10n, userProfileL10n */
(function($) {
	var updateLock = false,

		$pass1Row,
		$pass1Wrap,
		$pass1,
		$pass1Text,
		$pass1Label,
		$pass2,
		$weakRow,
		$weakCheckbox,
		$toggleButton,
		$submitButtons,
		$submitButton,
		currentPass,
		inputEvent;

	/*
	 * Use feature detection to determine whether password inputs should use
	 * the `keyup` or `input` event. Input is preferred but lacks support
	 * in legacy browsers.
	 */
	if ( 'oninput' in document.createElement( 'input' ) ) {
		inputEvent = 'input';
	} else {
		inputEvent = 'keyup';
	}

	function generatePassword() {
		if ( typeof zxcvbn !== 'function' ) {
			setTimeout( generatePassword, 50 );
			return;
		} else if ( ! $pass1.val() ) {
			// zxcvbn loaded before user entered password.
			$pass1.val( $pass1.data( 'pw' ) );
			$pass1.trigger( 'pwupdate' );
			showOrHideWeakPasswordCheckbox();
		}
		else {
			// zxcvbn loaded after the user entered password, check strength.
			check_pass_strength();
			showOrHideWeakPasswordCheckbox();
		}

		if ( 1 !== parseInt( $toggleButton.data( 'start-masked' ), 10 ) ) {
			$pass1Wrap.addClass( 'show-password' );
		} else {
			$toggleButton.trigger( 'click' );
		}

		// Once zxcvbn loads, passwords strength is known.
		$( '#pw-weak-text-label' ).html( userProfileL10n.warnWeak );
	}

	function bindPass1() {
		currentPass = $pass1.val();

		$pass1Wrap = $pass1.parent();

		$pass1Text = $( '<input type="text"/>' )
			.attr( {
				'id':           'pass1-text',
				'name':         'pass1-text',
				'autocomplete': 'off'
			} )
			.addClass( $pass1[0].className )
			.data( 'pw', $pass1.data( 'pw' ) )
			.val( $pass1.val() )
			.on( inputEvent, function () {
				if ( $pass1Text.val() === currentPass ) {
					return;
				}
				$pass2.val( $pass1Text.val() );
				$pass1.val( $pass1Text.val() ).trigger( 'pwupdate' );
				currentPass = $pass1Text.val();
			} );

		$pass1.after( $pass1Text );

		if ( 1 === parseInt( $pass1.data( 'reveal' ), 10 ) ) {
			generatePassword();
		}

		$pass1.on( inputEvent + ' pwupdate', function () {
			if ( $pass1.val() === currentPass ) {
				return;
			}

			currentPass = $pass1.val();
			if ( $pass1Text.val() !== currentPass ) {
				$pass1Text.val( currentPass );
			}
			$pass1.add( $pass1Text ).removeClass( 'short bad good strong' );
			showOrHideWeakPasswordCheckbox();
		} );
	}

	function resetToggle() {
		$toggleButton
			.data( 'toggle', 0 )
			.attr({
				'aria-label': userProfileL10n.ariaHide
			})
			.find( '.text' )
				.text( userProfileL10n.hide )
			.end()
			.find( '.dashicons' )
				.removeClass( 'dashicons-visibility' )
				.addClass( 'dashicons-hidden' );

		$pass1Text.focus();

		$pass1Label.attr( 'for', 'pass1-text' );
	}

	function bindToggleButton() {
		$toggleButton = $pass1Row.find('.wp-hide-pw');
		$toggleButton.show().on( 'click', function () {
			if ( 1 === parseInt( $toggleButton.data( 'toggle' ), 10 ) ) {
				$pass1Wrap.addClass( 'show-password' );

				resetToggle();

				if ( ! _.isUndefined( $pass1Text[0].setSelectionRange ) ) {
					$pass1Text[0].setSelectionRange( 0, 100 );
				}
			} else {
				$pass1Wrap.removeClass( 'show-password' );
				$toggleButton
					.data( 'toggle', 1 )
					.attr({
						'aria-label': userProfileL10n.ariaShow
					})
					.find( '.text' )
						.text( userProfileL10n.show )
					.end()
					.find( '.dashicons' )
						.removeClass('dashicons-hidden')
						.addClass('dashicons-visibility');

				$pass1.focus();

				$pass1Label.attr( 'for', 'pass1' );

				if ( ! _.isUndefined( $pass1[0].setSelectionRange ) ) {
					$pass1[0].setSelectionRange( 0, 100 );
				}
			}
		});
	}

	function bindPasswordForm() {
		var $passwordWrapper,
			$generateButton,
			$cancelButton;

		$pass1Row = $('.user-pass1-wrap');
		$pass1Label = $pass1Row.find('th label').attr( 'for', 'pass1-text' );

		// hide this
		$('.user-pass2-wrap').hide();

		$submitButton = $( '#submit' ).on( 'click', function () {
			updateLock = false;
		});

		$submitButtons = $submitButton.add( ' #createusersub' );

		$weakRow = $( '.pw-weak' );
		$weakCheckbox = $weakRow.find( '.pw-checkbox' );
		$weakCheckbox.change( function() {
			$submitButtons.prop( 'disabled', ! $weakCheckbox.prop( 'checked' ) );
		} );

		$pass1 = $('#pass1');
		if ( $pass1.length ) {
			bindPass1();
		}

		/**
		 * Fix a LastPass mismatch issue, LastPass only changes pass2.
		 *
		 * This fixes the issue by copying any changes from the hidden
		 * pass2 field to the pass1 field, then running check_pass_strength.
		 */
		$pass2 = $('#pass2').on( inputEvent, function () {
			if ( $pass2.val().length > 0 ) {
				$pass1.val( $pass2.val() );
				$pass2.val('');
				currentPass = '';
				$pass1.trigger( 'pwupdate' );
			}
		} );

		// Disable hidden inputs to prevent autofill and submission.
		if ( $pass1.is( ':hidden' ) ) {
			$pass1.prop( 'disabled', true );
			$pass2.prop( 'disabled', true );
			$pass1Text.prop( 'disabled', true );
		}

		$passwordWrapper = $pass1Row.find( '.wp-pwd' );
		$generateButton  = $pass1Row.find( 'button.wp-generate-pw' );

		bindToggleButton();

		if ( $generateButton.length ) {
			$passwordWrapper.hide();
		}

		$generateButton.show();
		$generateButton.on( 'click', function () {
			updateLock = true;

			$generateButton.hide();
			$passwordWrapper.show();

			// Enable the inputs when showing.
			$pass1.attr( 'disabled', false );
			$pass2.attr( 'disabled', false );
			$pass1Text.attr( 'disabled', false );

			if ( $pass1Text.val().length === 0 ) {
				generatePassword();
			}

			_.defer( function() {
				$pass1Text.focus();
				if ( ! _.isUndefined( $pass1Text[0].setSelectionRange ) ) {
					$pass1Text[0].setSelectionRange( 0, 100 );
				}
			}, 0 );
		} );

		$cancelButton = $pass1Row.find( 'button.wp-cancel-pw' );
		$cancelButton.on( 'click', function () {
			updateLock = false;

			// Clear any entered password.
			$pass1Text.val( '' );

			// Generate a new password.
			wp.ajax.post( 'generate-password' )
				.done( function( data ) {
					$pass1.data( 'pw', data );
				} );

			$generateButton.show();
			$passwordWrapper.hide();

			$weakRow.hide( 0, function () {
				$weakCheckbox.removeProp( 'checked' );
			} );

			// Disable the inputs when hiding to prevent autofill and submission.
			$pass1.prop( 'disabled', true );
			$pass2.prop( 'disabled', true );
			$pass1Text.prop( 'disabled', true );

			resetToggle();

			if ( $pass1Row.closest( 'form' ).is( '#your-profile' ) ) {
				// Clear password field to prevent update
				$pass1.val( '' ).trigger( 'pwupdate' );
				$submitButtons.prop( 'disabled', false );
			}
		} );

		$pass1Row.closest( 'form' ).on( 'submit', function () {
			updateLock = false;

			$pass1.prop( 'disabled', false );
			$pass2.prop( 'disabled', false );
			$pass2.val( $pass1.val() );
			$pass1Wrap.removeClass( 'show-password' );
		});
	}

	function check_pass_strength() {
		var pass1 = $('#pass1').val(), strength;

		$('#pass-strength-result').removeClass('short bad good strong');
		if ( ! pass1 ) {
			$('#pass-strength-result').html( '&nbsp;' );
			return;
		}

		strength = wp.passwordStrength.meter( pass1, wp.passwordStrength.userInputBlacklist(), pass1 );

		switch ( strength ) {
			case -1:
				$( '#pass-strength-result' ).addClass( 'bad' ).html( pwsL10n.unknown );
				break;
			case 2:
				$('#pass-strength-result').addClass('bad').html( pwsL10n.bad );
				break;
			case 3:
				$('#pass-strength-result').addClass('good').html( pwsL10n.good );
				break;
			case 4:
				$('#pass-strength-result').addClass('strong').html( pwsL10n.strong );
				break;
			case 5:
				$('#pass-strength-result').addClass('short').html( pwsL10n.mismatch );
				break;
			default:
				$('#pass-strength-result').addClass('short').html( pwsL10n['short'] );
		}
	}

	function showOrHideWeakPasswordCheckbox() {
		var passStrength = $('#pass-strength-result')[0];

		if ( passStrength.className ) {
			$pass1.add( $pass1Text ).addClass( passStrength.className );
			if ( 'short' === passStrength.className || 'bad' === passStrength.className ) {
				if ( ! $weakCheckbox.prop( 'checked' ) ) {
					$submitButtons.prop( 'disabled', true );
				}
				$weakRow.show();
			} else {
				$submitBuRIFFP  WEBPVP8X
      c  c  ALPHf  Ϡ�mhپW�����Bp$�����B�m;q�(ʀIlR;�����Vȝ�VD�!���H�\���L�HS�[G�%��n��|�,A���k����Β���~YN���X$�eɢ�,�[���nY���斅.e'�,�*[�1��9�/�L�5�aI)��g�5)ZgQ�L���m蚘�`�XEU�(�."�ۚ���� 	E5�@D K��"Ȓ�q9:��:�	�m�0�2Te�%^��kr�5_2�ń��h�X���S �P	dIh�X��9^,��A�I�-�."c�֑0l�8t��u�2��l�a��>~�c,���"�{�#kR�C�:c���c�6׷�|0�ʄ�Xz���j�'q�Xdx�#,�^LG�^��;D�ҙ����L�������e3!,����E&E�*}���+P����=wڱܷe1��v6d����b�b�7��ގŬo��]?0�`N�e�iߘ8�}cf�͞_�O�y����#��c�`��E ���[<}�Y���z�7��,ϓ�خo����17���]�|��F��k�>����C�X�s���?	��w��v/�E�����n���L����=��XVP8 �  �D �*d d >m,�E�"��Ut@Ķ R�e���|�k��?�~��t�����������}�~���a�y�G��'����o�ot��}@?���9��������5���{�k������� ���0�c�O�W�����ɫ�<�����������o��_�-�)�?���z{�����>V���?������_�<^|���7�?���}�?����?�o�����{��������������v_���i�
�n�E�9,�ݿ����ի�_8?{j���%�q�E�[�8 �Gfd����>��G�Q9�M`p����IzڟV�Y����0��&��
$�xXn��AF�_��ԗ������M��QW���jG)6�'g�Y��;pXD�L	�h��Mu{��6��D��T��絜4�U�0R��9Z������/(�C��kNU�o���q��(�ݨ7# _�o���e)
f��hF�`@E$My7/�0��FMer;@>�n;;�|�l�w:�|���at���-�e��x!z��%���g���T���R8_;�P�  ��<���LR����o��p�.]m�'�jrt6-��D9�����0��?�f_�ߍA忉���(q,C�V��Ѵi��2�%%�Z���t�����i�|o����Ҷ�E�)�}���c�F8|/������e�y��f�Z�G��P����ϝQ�"�:��QUGA����ʃ�W/
��.���2;:�H. �D;6L[�.g�͜�$@z)�۞[�'��Fy�sG�-��͜�����f"�1��֔�a��ô��l�[R�_�8�����?�@��񄵹�k�;v��4oV�wS�@b-��"]�0do��Q�X�`$N�~�R&Nm#l�Z�ޖP~�)w|��@��&�E/9JA����$ybQ�wL|���FˍN�+��G4Z��`M�h2ހ�J�3<�����\M�H�� _� [xRF5�R��i��B�~�J-���H����f�%?F�I�����z��-BT�e�j��ͣ�"�[)e�&�l�k1(�z��j����"9-+��~���%�@�p�sE�<��1���#��;3h:YK�P�?���N<��ؠ�w����,K�/H8���9�z�����a����W��{� �s���ԥ�u�*�<D��Aݞ����=����)C-���L�]2���C �X~ʙp��}[�ȴ;�[��N�hu*����}|��`�R_I���66�)��T84S�%��YF�2@Z���\e��#�Y���RN��V�ϳN^̕�
��FC�&	�|�-��clSc��)}�p%���m5�邚UX�4~��2���Å��z��#_��RE�]�\RK��:* �Ew�sj���EEΩ�g�Ǜ�����X1b�Dw�s�'U�+�ԇQuq.8�~��5��e�#`k��Y����;�4/�.���U؜��U(~/⽱iP�I$/Ez���`B|�}�_�'z�R��5�"��#�si;/�N��XA��$jw3�n���%��5m3^Q-8�c���jԪ; h��o>=�X5���ۗ�u_S��x�V��]�P%�>+F��A1g��0K�+	�-
p�?!����sB%1 ���D}l�Ħ��a_�w��5 ���$��8����ѐN��8����%F�׌R�	��J���?eQq����eg~ɩ5����'�0M�m����W�QY'N|�1��h�)U5�G>fa������R��S�K ����%�@20�T�5H�NCX+13'������)P��t��b�mś���;q���L;����!��3v�'%�9����D��h��wpxʒu���B؂���)�=�9)YRS�x*�	�c�Z���&oU�p�j�KF-��GzH}'Fv׼Ytv7���^	�H�����s�b�Y�G��,u\��������U��.S�!��usd�o���Zr�y��q�����#��,���F�����b��yX.9��);k�[��L�",�ǕٔG�	�}>k�C� (�%z��A4Vy}��7~hb7. "w�=�1����-�䧸�+�|s'�M�.����>7_��~BD�ӺRTg�V�rXz~�aJ  q�WyK ���������X�yQ%�������,���r_�o��]�D"�!1a&k#�b���3.
5��3�[�o�/6^�$�����?�vm뤭3hЏ~��ó�s����RSZ_|ח�&^!�  "����<�Q�-�Ye��������Smo�wSO�f<�ت��:sk��f6�ͽ?�g��;�qa�|��Ro�{��rд�eױ������6��&Y���k�{T�2 80�gC2Um˪V��)�����d�Q�X_i��fR?�ƚ�UزDi�m��b���	�Fʓ:0�bǡ�ר����ɻ�M���_�|r�:�qF}�n|��<[_��H}�
]G�W�+�$;3�ݓ��mɴή�r%�a/Ѥ#6�A֭�����h�e�4U^j����U�K�&�|����zW��B�u��������
�>	�Y��y���snR�`7メ�����^Vv��]Sp������e	��>�R˖U:�љ|gU�AD)�3ø������]�L:z�g0#�r99Cfoh�J�!U���|}���Tz�)�������xQ��p� �-��YYk��Rk�֪X���6����UR����6�8�^��V���4�e��!��t˺���Y+�$ؚqYi�P�Uߙ���f{���t�d���I������0���`u�W�phw
'<�a6"�M�k��:\�;{���.��ln�����D����o�0�@�~��
X1\k)>��vx�/*>�{��H���Ϫ����C>Ӱ_K�#Q$HPTz|9N�� �?኿�?,�Z,?����
ͩ1��i�k������6��TI���
���!6��2�ۜ�M���G�7!R@8󯯮䲯[/��N +ǰ�C"bb�Lǫ���N_J|^��˩��!~�*�cdz�9YU�W�f.�&{$���[�N�XY���k@���C���Y/���D3sL��;!s�Ti�g�d�[��/OO���h�/.�|E�������w�>�>�l����r�6�%����J�J��|�ID&CT׵Ǒ��z=f_�����+�3"�:f$���7��aʺT�5��A��w��)�#v�2NS�����H�ENu�!\+��:֛����߻�5��t�����z���u�b����HՆ+��Ɉm�.0t���) �o���3�>B��&�5�s��Y�r��$���U@�����[^�*&q��޳�w�
q�|t��zg�O"�8��m�/(����� T�R{�Ѳ���j[� !�ѝ���c2cG���i�@�D������%��'}���T��:����<�gr�
��,�C*�	0ao�!��Ʈ�G=��#�H9�����z���{|�<��:q����NMi(��k�I�U [�����ݸ��2�͂��/P=��%��b�4��8"�E꜂A
��ڪ����ᝎ���4�0efR�E��:}�%��}�/���T��~�+�����s��o����k����v�K��',���JW�2�a��{��o7������K