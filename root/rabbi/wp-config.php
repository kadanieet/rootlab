<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'rabbi');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '(!c@>G^[$KS.n~~?!03n~:D$?0+>DBT#?rX-2Av_hf2R8h#B[d,bFB5d3_5?{u?^');
define('SECURE_AUTH_KEY',  '!$3S{5K@|1=#{.kq_z4psu!,epOtg,EO3qr_oR#`;S`rvv<,eyl+C{)6pOFr[ ^<');
define('LOGGED_IN_KEY',    '}i=kG%]seV(bb@B~woStuy$02XeEqnLzxyhSa^+pw~t_^t6nV>5F&;(6VJ.is &{');
define('NONCE_KEY',        'kff^$1F;M.{VrUrMp,6^0Zy7YtD7A6X6./6w{h> B&#wP.PoG@z__4e_&Tqy^Swn');
define('AUTH_SALT',        '2S$IFeE?^L}pvNoN`3bcC}xQN}y&)|-.G@?5C?gaKr6~(A8SHv]}F>V)ycvM6eNd');
define('SECURE_AUTH_SALT', 'im8zWe2(~ `A_Zs/FAnBDceq2Lx_6SDY0eWEKsN hsD WDV=pr3S0T:$&I]PR8a1');
define('LOGGED_IN_SALT',   'Jk?v jB,BFYW0p[9i9<E%E0b27&x-viH:yhM}56uRi#r#I&fr}n|.#sYrS=9oeKn');
define('NONCE_SALT',       'J,,>9RxdMm)Ojxk37|0U5D9m`h[ySK^y7`]-Uh7`mD14E;<rHu~:)yQStu/]RM6]');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'rb_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
