<?php

	//Utility Functions
	if (!defined('ROOT') && !defined('UTILS')) {
		define('ROOT', $_SERVER['DOCUMENT_ROOT'] . '/');
		define('UTILS' , 'rt_build_0/rt_utils/');
		//ROOT = $_SERVER['DOCUMENT_ROOT'] . '/';
		//UTILS = 'rt_build_0/rt_utils/';
		}

		$rt_utils = ROOT . UTILS . 'util_fxns.php';

		if(file_exists($rt_utils))
			require_once $rt_utils;
		else
			exit('File ' . $rt_utils . ' is missing');

	//Require_Files( ROOT . UTILS . 'rt_const_vars.php');

	class ad_controller
	{

		private $app;

		function __construct($value='')
		{
			$this->app = $value;
			/*if (function_exists('clean_input'))
					$this->app = $value;
			else
				exit;*/
		}

		/*This function takes an xml doc and an attribute search value to determine the location of
		a file resource*/
			private function get_rt_resource($search_value, $loc)
			{
				//load_xml_doc from the util_fxns
				$xmlDoc = load_xml_doc($loc);

				$searchNode = $xmlDoc->getElementsByTagName( "type" );

				foreach( $searchNode as $searchNode )
				{
				    $valueID = $searchNode->getAttribute('ID');

				    if($valueID == $search_value)
				    {

				    $xmlLocation = $searchNode->getElementsByTagName( "loc" );
				    return $xmlLocation->item(0)->nodeValue;
				    break;

				    }

				} exit("'". $search_value . "' key not found");
			}

		public function get_ad_app($search_value)
		{
			return $this->get_rt_resource($search_value, '../ad_xml/ad_reso.xml');
		}

		public function get_rt_data($search_value)
		{
			return $this->get_rt_resource($search_value, XML . PREF . 'data_locs.xml');
		}

		public function get_ad_credential($search_value)
		{
			return $this->get_rt_resource($search_value,  "../ad_xml/ad_creds.xml");

		}

		public function get_relation($search_value)
		{
			return $this->get_rt_resource($search_value, ROOT . "rt_build_0/rt_xml/rt_relations.xml");

		}

		function get_model($properties_array)
		{
			$app_loc = $this->get_ad_app($this->app);
		    Require_Files('../ad_model/'. $app_loc);
		    $class_array = get_declared_classes();
			$last_position = count($class_array) - 1;
			$class_name = $class_array[$last_position];
			$rt_object = new $class_name($properties_array);

			return $rt_object;
		}
	}
 ?>
