<?php
	/**
	*
	*/
	//if (session_status() == PHP_SESSION_NONE) session_start();

	if (!defined('ROOT_DIR'))
		define('ROOT_DIR', $_SERVER['DOCUMENT_ROOT'] . '/');


	//Require files modules
		if ( file_exists(ROOT_DIR . 'rt_build_0/rt_utils/util_fxns.php') )
			require_once ROOT_DIR . 'rt_build_0/rt_utils/util_fxns.php';
		else
			exit('utility funtions files unvailable :: line ' . __LINE__ . ' in ' . __FILE__);

	//$files = array(ROOT_DIR . 'globalvars.php', CONT_DIR . 'ad_controller.php');

	Require_Files('../ad_control/ad_controller.php');

	class connect
	{
		private $db_host;
		private $db_user;
		private $db_pass;
		private $db_name;

		private $tb_name;

		private $connection;

		private $controller;

		function __construct($db_properties)
		{
			$this->controller = new ad_controller('connection_model');
			$this->set_credentials($db_properties);
			/*$this->db_host = $db_properties[0];
			$this->db_user = $db_properties[1];
			$this->db_pass = $db_properties[2];
			$this->db_name = $db_properties[3];*/

		}

		private function set_credentials($db_properties)
		{
			$this->db_host = $db_properties[0];
			$this->db_user = $db_properties[1];
			$this->db_pass = $db_properties[2];
			$this->db_name = $db_properties[3];
			/*$this->db_host = $this->controller->get_ad_credential($db_properties[0]);
			$this->db_user = $this->controller->get_ad_credential($db_properties[1]);
			$this->db_pass = $this->controller->get_ad_credential($db_properties[2]);
			$this->db_name = $this->controller->get_ad_credential($db_properties[3]);*/
		}

		/*public function set_relation($rel_name)
		{
			return $this->controller->get_relation($rel_name);
		}*/

		public function request_connection()
		{
			$this->connection = $this->controller->get_model(array($this->db_host, $this->db_user, $this->db_pass, $this->db_name));
			$this->connection->connect();
			//return $this->connection;
		}

		public function execute_query($query)
		{
			return $this->connection->SelectQuery($query);
		}
	}
 ?>
