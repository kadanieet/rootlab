<?php
    /**
     *
     */
    class Client
    {
      $private cl_id;
      $private id_no;
      $private sname;
      $private mname;
      $private fname;
      $private gender;
      $private rlgn;
      $private email;
      $private phone;
      $private resid;

      function __construct($array)
      {
        # code...
      }



      /*GET METHODS*/
      public function get_id($value='')
      {
        return $this->cl_id;
      }

      public function get_id_no($value='')
      {
        return $this->id_no;
      }

      public function get_sname($value='')
      {
        return $this->sname;
      }

      public function get_mname($value='')
      {
        return $this->mname;
      }

      public function get_fname($value='')
      {
        return $this->fname;
      }

      public function get_gender($value='')
      {
        return $this->gender;
      }

      public function get_rlgn($value='')
      {
        return $this->rlgn;
      }

      public function get_email($value='')
      {
        return $this->email;
      }

      public function get_phone($value='')
      {
        return $this->phone;
      }

      public function get_resid($value='')
      {
        return $this->resid;
      }

      /*SET METHODS*/
      public function set_id($value='')
      {
        $this->cl_id = $value;
      }

      public function set_id_no($value='')
      {
        $this->id_no = $value;
      }

      public function set_sname($value='')
      {
        $this->sname = $value;
      }

      public function set_mname($value='')
      {
        $this->mname = $value;
      }

      public function set_fname($value='')
      {
        $this->fname = $value;
      }

      public function set_gender($value='')
      {
        $this->gender = $value;
      }

      public function set_rlgn($value='')
      {
        $this->rlgn = $value;
      }

      public function set_email($value='')
      {
        $this->email = $value;
      }

      public function set_phone($value='')
      {
        $this->phone = $value;
      }

      public function set_resid($value='')
      {
        $this->resid = $value;
      }
    }

 ?>
