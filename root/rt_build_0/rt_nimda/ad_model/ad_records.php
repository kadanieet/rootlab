<?php
    /**
     *
     */
     require_once '../ad_control/ad_controller.php';

    class Records
    {
        private $records;
        private $controller;
        private $connection;

        function __construct($records)
        {
          $this->set_records($records[0]);
        }

        private function set_records($value='')
        {
          $this->records = $value;
        }

        public function fetch_records()
        {
          return $this->fetch($this->records);
        }

        private function fetch($records='')
        {
          $this->init_conn('connection');
          $rel = $this->get_relation($records);
          return $this->connection->execute_query("SELECT * FROM " . $rel . "");
        }

        private function init_conn($app)
        {
          $this->controller = new ad_controller($app);
          $creds = $this->get_credentials();
          $this->connection = $this->controller->get_model($creds);
          $this->connection->request_connection();
        }

        private function get_credentials()
    		{

    			$db_host = $this->controller->get_ad_credential('db_host');
    			$db_user = $this->controller->get_ad_credential('db_user');
    			$db_pass = $this->controller->get_ad_credential('db_pass');
    			$db_name = $this->controller->get_ad_credential('db_name');
          return array($db_host, $db_user, $db_pass, $db_name);
    		}

    		private function get_relation($rel_name)
    		{
    			return $this->controller->get_relation($rel_name);
    		}

    }

 ?>
