<?php
	//requiring single file
	function Require_File($url)
	{
		if ( file_exists($url) ) require_once($url); else exit("Error including file(s): $url");
	}

	//requiring multiple files
	function Require_Files($url_list)
	{
		if (gettype($url_list) == 'string') Require_File($url_list);
		if (gettype($url_list) == 'array') foreach ( $url_list as $url ) Require_File($url);
	}

	function load_xml_doc($loc)
	{
		$xmlDoc = new DOMDocument();
		if (file_exists($loc))
			$xmlDoc->load($loc);
		else
			exit("Error loading xml doc(s): $loc");
		return $xmlDoc;
	}

	/*This function takes an xml doc with the data containing tag and any attribute defined.
	It processes the xml data into a html form options with the attribute as the value and tag value
	as the node value*/
		function processXmlData($xmlDoc, $tag='', $attribute='initial')
		{
			$nodes = $xmlDoc->getElementsByTagName($tag);
			$options = array();
				foreach ($nodes as $node) {
					$init = $node->getAttribute($attribute);
					$options[] = "<option value='" . $init . "'>" . $node->nodeValue . "</option>";
				}
			return $options;
		}

	function Print_Message($message)
	{
		echo "<div id='error'>" . $message . "</div>";
	}

	function Redirect($address, $timeout = 0)
	{
		header("refresh:" .$timeout. ";url=" . $address);
	}

	function to_uppercase($string)
	{
		return strtoupper($string);
	}

	function to_lowercase($string)
	{
		return strtolower($string);
	}
 ?>
