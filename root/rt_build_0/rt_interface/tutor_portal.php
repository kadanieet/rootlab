<?php
    require_once 'include.php';
    
    $router = $cntlr->get_rt_app('router');

 ?>

<h2>Tutor Portal</h2>
<div class="menu-top-menu-container">
    <ul id="top-menu" class="menu">
      <li><a href="<?php echo PHP_SELF; ?>">Home</a></li>
      <li><a rel="Client Profile" href="<?php echo PHP_SELF . "?route=tut_prof" ?>">Profile</a></li>
      <li><a rel="Registered Students" href="<?php echo PHP_SELF . "?route=creg_stdnts" ?>">Students</a></li>
      <li><a rel="Fee Information" href="<?php echo PHP_SELF . "?route=pay_due" ?>">Payment In4</a></li>
    </ul>
    <a rel="Log out from the portal" href="http://rabbitutors.dev/home/contact/">Log out</a>
  </div>

<?php
//check the origin of the request also
    if (isset($_GET['route'])) {
      $frm_util = $cntlr->get_rt_app('frm_util');
      Require_Files($frm_util);

      //get the application from the url
        $app = $cntlr->get_rt_app(clean_input($_GET['route']));
        Require_Files($app);
    }

    else {
  echo "Welcome: Tutor Name<br>";

  echo "Incoming Lessons<br>";
  echo "<table>";
  echo "<tr><th>Date</th><th>Lesson Subject</th><th>Start Time</th><th>Duration</th><th>Student</th><th>Level</th><th>Place</th><th>Client Contact</th><tr>";
  echo "<tr><td></td><td></td></tr>";
  echo "</table>";

  echo "Missed Lessons<br>";
  echo "<table>";
  echo "<tr><th>Date</th><th>Lesson Subject</th><th>Start Time</th><th>Duration</th><th>Student</th><th>Level</th><th>Place</th><th>Client Contact</th><tr>";
  echo "<tr><td></td><td></td></tr>";
  echo "</table>";

  echo "Students Performance<br>";

  echo "Rating<br>";

  echo "Payment Summary<br>";
}
?>
