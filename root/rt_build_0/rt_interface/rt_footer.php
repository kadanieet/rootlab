<footer id="colophon" class="site-footer" role="contentinfo">
			<div class="wrap">
				

	<aside class="widget-area" role="complementary">
					<div class="widget-column footer-widget-1">
				<section id="text-4" class="widget widget_text"><h2 class="widget-title">Find Us</h2>			<div class="textwidget"><p><strong>Address</strong><br />
Coming Soon</p>
<p><strong>Hours</strong><br />
Coming Soon</p>
</div>
		</section>			</div>
					<div class="widget-column footer-widget-2">
				<section id="text-5" class="widget widget_text"><h2 class="widget-title">About This Site</h2>			<div class="textwidget"><p>Want to work as a tutor or hire a tutor. Rabbi Tutors is the right place for you. Registration is only a click away..Join our large network in the country and be part of this brilliant achievement.</p>
</div>
		</section><section id="search-4" class="widget widget_search"><h2 class="widget-title">Search This Site</h2>

<form role="search" method="get" class="search-form" action="http://rabbi.dev/">
	<label for="search-form-5a44b4ec00fd9">
		<span class="screen-reader-text">Search for:</span>
	</label>
	<input type="search" id="search-form-5a44b4ec00fd9" class="search-field" placeholder="Search &hellip;" value="" name="s" />
	<button type="submit" class="search-submit"><svg class="icon icon-search" aria-hidden="true" role="img"> <use href="#icon-search" xlink:href="#icon-search"></use> </svg><span class="screen-reader-text">Search</span></button>
</form>
</section>			</div>
			</aside><!-- .widget-area -->

					<nav class="social-navigation" role="navigation" aria-label="Footer Social Links Menu">
						<div class="menu-social-links-menu-container"><ul id="menu-social-links-menu" class="social-links-menu">
<li id="menu-item-24" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-24"><a href="https://www.facebook.com/wordpress"><span class="screen-reader-text">Facebook</span><svg class="icon icon-facebook" aria-hidden="true" role="img"> <use href="#icon-facebook" xlink:href="#icon-facebook"></use> </svg></a></li>
<li id="menu-item-25" class="menu-item menu-item-type-custom menu-istem-object-custom menu-item-25"><a href="https://twitter.com/wordpress"><span class="screen-reader-text">Twitter</span><svg class="icon icon-twitter" aria-hidden="true" role="img"> <use href="#icon-twitter" xlink:href="#icon-twitter"></use> </svg></a></li>
<li id="menu-item-26" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-26"><a href="https://www.instagram.com/explore/tags/wordcamp/"><span class="screen-reader-text">Instagram</span><svg class="icon icon-instagram" aria-hidden="true" role="img"> <use href="#icon-instagram" xlink:href="#icon-instagram"></use> </svg></a></li>
<li id="menu-item-27" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-27"><a href="mailto:wordpress@example.com"><span class="screen-reader-text">Email</span><svg class="icon icon-envelope-o" aria-hidden="true" role="img"> <use href="#icon-envelope-o" xlink:href="#icon-envelope-o"></use> </svg></a></li>
</ul></div>					</nav><!-- .social-navigation -->
</div>
</footer>