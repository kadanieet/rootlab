<?php
      //Utility Functions
        $root = $_SERVER['DOCUMENT_ROOT'];
        $rt_utils = $root . '/rt_build_0/rt_utils/util_fxns.php';

      if(file_exists($rt_utils))
        require_once $rt_utils;
      else
        exit('File ' . $rt_utils . ' is missing');

      Require_Files($root . '/rt_build_0/rt_utils/rt_const_vars.php');
      /*$files = array(CNTLR, YATE);
      Require_Files($files);

      $cntlr = new rt_controller();*/
 ?>

<h1>Profile Registration</h1>
    <form action="" method="post">
      <label for="surname">Surname</label>
        <input type="text" id="surname" name="surname" required="required"/><br />

        <label for="midname">Middle Name</label>
        <input type="text" id="midname" name="midname"/><br />

         <label for="fname">First Name</label>
        <input type="text" id="fname" name="fname" required="required"/><br />

        <label for="phone">Phone</label>
        <input type="number" id="phone" name="phone" required="required"/><br />

         <label for="tscno">TSC Number</label>
        <input type="text" id="tscno" name="tscno"/><br />

        <label for="idno">ID Number</label>
        <input type="number" id="idno" name="idno" required="required"/><br />

        <label for="occupation">Occupation</label>
        <input type="text" id="occupation" name="occupation" required="required"/><br />

        <label for="org">Organization</label>
        <input type="text" id="org" name="org" required="required"/><br />

        <label for="relgn">Religion</label>
        <select name="relgn">
        <option disabled selected>-Choose-</option>
        <?php
            do_options('religion');
         ?>
       </select><br/>

       <label for="hobbies">Hobbies</label>
       <textarea id="hobbies" cols="30" rows="4" name="hobbies" required="required"></textarea><br />

     <input name="next" type="submit" value="Register" />
</form>
