<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
	    <?php

        require_once 'rt_interface/include.php';

			  $title = $cntlr->get_rt_app('site_title');

				$jquery = $cntlr->get_rt_app('jquery');
        $bstrap = $cntlr->get_rt_app('bootstrap');
				//$mtrlz = $cntlr->get_rt_app('materialize');
				$inclds = array($jquery, $bstrap);
				Require_Files($inclds);

			$branding = $cntlr->get_rt_app('site_brand');
			$tp_menu = $cntlr->get_rt_app('top_menu');
			$login = $cntlr->get_rt_app('home_login');

			$header = array($title, $branding, $tp_menu, $login);
			Require_Files($header);
		?>

  </head>
  <body>

  	<?php
  			$home = $cntlr->get_rt_app('home');
			$about = $cntlr->get_rt_app('about');
			$register = $cntlr->get_rt_app('register');
			$tstmnl = $cntlr->get_rt_app('clt_tstmnl');
			$contact = $cntlr->get_rt_app('contact');
			$footer = $cntlr->get_rt_app('footer');

			$body = array($home, $about, $register, $tstmnl, $contact, $footer);
			Require_Files($body);
  	 ?>

  </body>
</html>
