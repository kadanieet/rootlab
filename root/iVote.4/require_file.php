<?php
	//requiring single file 
	function Require_File($url)
	{
		if ( file_exists($url) )
			require_once($url);
		else
			exit("Error including file");
	}

	//requiring multiple files
	function Require_Files($url_list)
	{
		foreach ( $url_list as $url ) {
			if ( file_exists($url) )
				require_once($url);
			else
				exit("Error including files");
		}
	}	
 ?>