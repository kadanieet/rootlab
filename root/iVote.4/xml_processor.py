import print_lol

def xml_processor(root_element_title, child_element_name, text_files=[]):
    for each_file in text_files:
        dirty_xml = []
        (file_name, ext) = each_file.split('.', 1)
        try:
            with open('xml_declaration.xml') as declaration:
                for dec in declaration:
                    clean_dec = dec.strip()
                    dirty_xml.append(clean_dec)
        except IOError as err:
            print('File error: ' + str(err))

        dirty_xml.append('<' + str(root_element_title) + '>')

        try:
            with open(each_file) as file:
                for each_line in file:
                    clean_line = each_line.strip()
                    dirty_xml.append('<' + str(child_element_name) + '>' + str(clean_line) + '</' + str(child_element_name) + '>')

        except IOError as err:
            print ('File error: ' + str(err))

        dirty_xml.append('</' + str(root_element_title) + '>')

        with open(file_name + '.xml', 'w') as clean_xml:
            print_lol.print_lol(dirty_xml, fh=clean_xml)
                    
