use ivote;

INSERT INTO aspirants
(image_id, reg_no, surname, other_names, position, study_year, school, gender, resident, hostel) 
VALUES 
('img_1', 'J174/0772/2013', 'WAWERU', 'MWANGI', 'P1',4, 'J', 'M', 'yes', 'H3'),
('img_2', 'E142s/98616/2013', 'GITAU', 'NDUTA', 'P1', 4, 'D', 'F', 'no', NULL);

/*passed*/
insert into admin values (SHA2('qwerty1212', 224));

/*passed*/
insert into root values (SHA2('asdfjkl', 224));

/*passed*/
INSERT INTO voters
(voter_id, surname, other_names, study_year, school, gender,resident, hostel) 
VALUES
(1,'GICHUHI', 'WAINAINA', 3, 'J', 'M', 1, 'H9');

/*schema changed, votes added as a column to aspirants table*/
/*insert into votes(aspirant_id) values (1),(2);*/
/*select surname, other_names, votes from 
aspirants, votes
 where votes.aspirant_id = aspirants.aspirant_id;

update votes set votes = votes + 1 where aspirant_id = 1;*/

/*passed*/
INSERT INTO credentials 
(reg_no, email, password, hash, last_attempt, valid_login)
VALUES 
('J17/0773/2013', 'w.gichuhi5@students.ku.ac.ke', SHA2('emachichi', 224), '', NOW(), NOW()),
('K23S/0785/2017', 'gichuhi@students.ku.ac.ke', SHA2('chibalonza', 224), '', NOW(), NOW());