<?php 
	if (!defined('ROOT_DIR'))
		define('ROOT_DIR', $_SERVER['DOCUMENT_ROOT']);
	
	define('PHP_SELF', dirname($_SERVER['PHP_SELF']));
	define('VOTE_ADDR', 'http://' . $_SERVER['HTTP_HOST'].'/vote/');
	define('CURR_ADDR', 'http://' . $_SERVER['HTTP_HOST'] . PHP_SELF . '/');
	define('VIEW_ADDR', VOTE_ADDR . 'view/');
	define('CURR_DIR', ROOT_DIR . PHP_SELF . '/');
	define('SYS_ERR', 'System Not Available At The Moment, Try Later!');
	define('VOTER_PASS_REGEX', '/^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).*$/');
	define('REGNOLB', 'Registration Number');
	define('KUEMLB', 'KU Email');
	define('VOTE_DIR', ROOT_DIR . 'vote/');
	define('MOD_DIR', VOTE_DIR . 'model/');
	define('VIEW_DIR', VOTE_DIR . 'view/');
	define('CONT_DIR', VOTE_DIR . 'controller/');
 ?>