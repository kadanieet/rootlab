<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" >
	<input type="submit" name="sort" value="Sort By">
	<select name="category">
		<option value="none" <?php echo ($category == 'none') ? 'selected="selected"' : '';?>>All</option>
		<option value="school" <?php echo ($category == 'school') ? 'selected="selected"' : '';?>>School</option>
		<option value="year" <?php echo ($category == 'year') ? 'selected="selected"' : '';?>>Year</option>
		<option value="gender" <?php echo ($category == 'gender') ? 'selected="selected"' : '';?>>Gender</option>
		<option value="residence" <?php echo ($category == 'residence') ? 'selected="selected"' : '';?>>Residence</option>
		<option value="hostel" <?php echo ($category == 'hostel') ? 'selected="selected"' : '';?>>Hostel</option>
	</select>
</form>