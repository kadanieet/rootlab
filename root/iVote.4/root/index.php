<?php 
	if (isset($_POST['submit'])) {
		$output_form = 'no';
		if (file_exists($_SERVER['DOCUMENT_ROOT'] .'connectvars.php') && file_exists($_SERVER['DOCUMENT_ROOT'] . 'connection.php')) {
			require_once($_SERVER['DOCUMENT_ROOT'] . 'connectvars.php');
			require_once($_SERVER['DOCUMENT_ROOT'] . 'connection.php');	
		} else {
			exit('file error');
		}

	$req_connection = new Db_Connection(DB_HOST, DB_NAME, DB_USER, DB_PASS);

	$connection = $req_connection->connect();

	$root_pass = $_POST['password'];

	$root_query = "SELECT password from root where password = SHA('" .$root_pass. "')";

	$stmt = $connection->prepare($root_query);

	$stmt->execute();

	$root_res = $stmt->fetchALL(PDO::FETCH_ASSOC);

	if (count($root_res) != 0) {
			// start the root session and redirect to the dashboard
			session_start();
			$_SESSION['root'] = true;

			  //redirect to the home page
			$home_url = 'http://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/dashboard.php';
			header('Location: ' . $home_url);
		}
		else {	
			echo "<p id='error'>Unsuccessful login, # attempts remaining</p>";
			$output_form = 'yes';
		}
	} 

	else {
		$output_form = 'yes';
	}

	if ($output_form == 'yes') {
 ?>
	<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
		<label for="password">Password</label><input type="password" name="password"><br>
		<input type="submit" name="submit" value="Login">
	</form>

<?php

	}
 ?>