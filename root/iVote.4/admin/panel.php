<?php 
	session_start();
	if(isset($_SESSION['admin'])) {
		require_once $_SERVER['DOCUMENT_ROOT'] . '/admin/admin_menu.php';
	} 
	else {
		//redirect to the home page
	$home_url = 'http://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/';
	header('Location: ' . $home_url);
	}

 ?>