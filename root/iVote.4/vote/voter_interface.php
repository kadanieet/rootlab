<?php 

		function login_handler($login_user, $password)
		{
			$user = to_uppercase(clean_input($login_user));
			$pass = clean_input($password);

			$feedback_msg = '';
			$feedback_state = 'FAIL';

			$feedback = array();

			if (empty($user)) {
				$feedback_msg = 'You forgot to enter your username'; 
				return $feedback_msg;
			}
			    		
			elseif (empty($pass))
				$feedback_msg = 'You forgot to enter your password'; 
			      
			elseif (!(preg_match(VOTER_PASS_REGEX, $pass)))
				$feedback_msg = 'Password Does not match the given criterion';

			//elseif();//username(Registration Number format)

			else {
				Require_File('voter_container.php');
				$container = new voter_container();
				$container->check_data($user, $pass, 'login');
			}
		}
 ?>