
<?php
	if (session_status() == PHP_SESSION_NONE) session_start();

	if (!defined('ROOT_DIR'))
		define('ROOT_DIR', $_SERVER['DOCUMENT_ROOT']); 

	//Require files modules
		if ( file_exists(ROOT_DIR . 'utility_functions.php') )
			require_once ROOT_DIR . 'utility_functions.php';
		else
			exit('utility funtions files unvailable');

	Require_Files( ROOT_DIR . 'globalvars.php');

	$files = array( CURR_DIR . 'view/login_form.php', CURR_DIR . 'view/sign_up_form_initial.php');
			Require_Files($files);
	
	?>