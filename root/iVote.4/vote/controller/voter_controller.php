<?php 
	/**
	* 
	*/
	if (!defined('ROOT_DIR'))
		define('ROOT_DIR', $_SERVER['DOCUMENT_ROOT']); 

	//Require files modules
		if ( file_exists(ROOT_DIR . 'utility_functions.php') )
			require_once ROOT_DIR . 'utility_functions.php';
		else
			exit('utility funtions files unvailable');

	Require_Files( ROOT_DIR . 'globalvars.php');

	class voter_controller
	{
	
		private $app;
		private $dog_file;

		function __construct($value)
		{
			if (function_exists('clean_input'))
					$this->app = $value;
			else
				exit;
		}

		private function get_voter_resource($search_value, $loc)
		{
			$xmlDoc = load_xml_doc($loc);

			$searchNode = $xmlDoc->getElementsByTagName( "type" ); 

			foreach( $searchNode as $searchNode ) 
			{ 
			    $valueID = $searchNode->getAttribute('ID'); 
			    
			    if($valueID == $search_value)
			    {

			    $xmlLocation = $searchNode->getElementsByTagName( "location" ); 
			    return $xmlLocation->item(0)->nodeValue;
			    break;

			    }

			}
		}

		public function get_voter_application($search_value)
		{
			return $this->get_voter_resource($search_value, CONT_DIR . 'voter_applications.xml');
		}

		public function get_voter_credentials($search_value)
		{
			return $this->get_voter_resource($search_value, CONT_DIR. "voter_connections.xml");
		
		}

		public function get_voter_relations($search_value)
		{
			return $this->get_voter_resource($search_value, CONT_DIR. "voter_relations.xml");
		
		}

		function get_model($properties_array)
		{
			$app_loc = $this->get_voter_application($this->app);
		    Require_Files(MOD_DIR. $app_loc);
		    $class_array = get_declared_classes();
			$last_position = count($class_array) - 1;
			$class_name = $class_array[$last_position];	
			$voter_object = new $class_name($properties_array);
			
			return $voter_object;
		}
	}
 ?>