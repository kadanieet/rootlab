<?php 
	//initial sign up form submit handler
		if(isset($_POST['signup'])){
			//no form output
				//Output_Form_Status();
			if (empty($_POST['reg_no_1']) || empty($_POST['reg_no_2']) || empty($_POST['email'])) 
				Print_Message('You have forgotten to enter your registration number and/or your email');

			elseif (to_uppercase($_POST['reg_no_1']) != to_uppercase($_POST['reg_no_2'])) 
				Print_Message("The registration numbers are not matching");

	     	// Strip out everything but the domain from the email and check if $domain is valid
			elseif (!preg_match('/^[a-zA-Z0-9][a-zA-Z0-9\._\-&!?=#]*@/', $_POST['email']) || preg_replace('/^[a-zA-Z0-9][a-zA-Z0-9\._\-&!?=#]*@/', '', $_POST['email']) != 'students.ku.ac.ke') 
			    	Print_Message("The email address is not a valid Kenyatta University Coporate email.");
			else
			{
				//get the form_utilities file for cleaning user inputs and converting text case
					$files = array(ROOT_DIR . 'form_utilities.php', ROOT_DIR . 'utility_functions.php');
					Require_Files($files);

				//initial sign up users input
				GLOBAL $reg_no_1;
				GLOBAL $reg_no_2;
				GLOBAL $email;

				$reg_no_1 = to_uppercase(clean_input($_POST['reg_no_1']));
				$reg_no_2 = to_uppercase(clean_input($_POST['reg_no_2']));
				$email = to_lowercase(clean_input($_POST['email']));

				//form handler
				Require_Files(CONT_DIR . 'voter_controller.php');

				$controller = new voter_controller(clean_input($_POST['case']));
				$properties_array = array($reg_no_1, $email);
				$signup_model = $controller->get_model($properties_array);
				$msg = $signup_model->verify_credentials();
				//Output the initial sign up and log in form incase of inappropriate input
					//Output_Form_Status('yes', 'yes');				

				// confirm that the credentials are not in the database
					if (gettype($msg) == 'array' || (stristr($msg, $reg_no_1) && stristr($msg, $email)) ) 
						 	Print_Message("Both the registration number and coporate email are already registered"); 
					elseif (stristr($msg, $reg_no_1)) 
						Print_Message("That registration number is already registered");

					elseif (stristr($msg, $email)) 
						Print_Message("That coporate email is already registered");

					elseif($msg == 'Passed') {
						//Output_Form_Status();
						if($signup_model->send_verfication_request())
						{	
							Print_Message("Log in into your email to verify your account!");
							Redirect( CURR_ADDR , 5);
						}
					}
			}			

		}
 ?>

<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
	<label for="reg_no_1"><?php echo REGNOLB; ?></label>
		<input type="text" name="reg_no_1" value ="<?php GLOBAL $reg_no_1; echo "$reg_no_1"; ?>" placeholder="Your <?php echo REGNOLB; ?>"><br>
	<label for="reg_no_2"><?php echo REGNOLB; ?></label>
		<input type="text" name="reg_no_2" value ="<?php GLOBAL $reg_no_2; echo "$reg_no_2"; ?>" placeholder="Re-enter <?php echo REGNOLB; ?>"><br>
	<label for="email"><?php echo KUEMLB; ?></label>
		<input type="text" name="email" value ="<?php GLOBAL $email; echo "$email"; ?>" placeholder="<?php echo KUEMLB; ?>"><br>
		<input type="hidden" name="case" value="signup">

		<input type="submit" name="signup" value="Sign Up">
</form>