<?php 
	if (session_status() == PHP_SESSION_NONE) session_start();

	if  (isset($_SESSION['user_id'])) {	

		$files = array( ROOT_DIR . 'connectvars.php', ROOT_DIR . 'connection.php', ROOT_DIR . 'utility_functions.php');
		Require_Files($files);

		$req_connection = new Db_Connection(DB_HOST, DB_NAME, DB_USER, DB_PASS);
		$connection = $req_connection->connect();

		$VoterQuery = "SELECT surname, other_names, study_year, resident, hostel, gender FROM voters WHERE voter_id = '" .$_SESSION['user_id']. "'";

		$CredQuery = "SELECT reg_no, email, password, active FROM credentials where user_id = '" .$_SESSION['user_id']. "'";

		$voter_stmt = $req_connection->SelectQuery($VoterQuery);
		$cred_stmt = $req_connection->SelectQuery($CredQuery);

		foreach ($cred_stmt as $cred_row) {
			$reg_no = $cred_row['reg_no'];
			$email = $cred_row['email'];
			$password = $cred_row['password'];
			$active = $cred_row['active'];
		}

		if ($active == 0) {
			$address = CURR_ADDR . 'index.php?sign_up=final';
			Print_Message( "<a href=$address>Complete your account Registration</a>" ); 
		}
			
		
		elseif ($active == 1) {
			
			foreach ($voter_stmt as $voter_row) {
				$surname = $voter_row['surname'];
				$other_name = $voter_row['other_names'];
				$study_year = $voter_row['study_year'];
				$hostel = $voter_row['hostel'];
				$resident = $voter_row['resident'];
				$gender = $voter_row['gender'];
			}

?>
		
		<p>Name : <?php echo "$surname $other_name"; ?></p>
		<p>Registration Number : <?php echo "$reg_no"; ?></p>
		<p>Email : <?php echo "$email"; ?></p>
		<p>Gender : 
		<?php
			$xmlDoc = new DOMDocument();
					if(file_exists($_SERVER['DOCUMENT_ROOT'] . 'genders.xml')){
						$xmlDoc->load($_SERVER['DOCUMENT_ROOT'] . 'genders.xml');
						$node = $xmlDoc->getElementsByTagName("gender");
						foreach ($node as $s_node) {
							$code = $s_node->getAttribute('initial');
							if ($gender == $code)
								echo $s_node->nodeValue;
						}
					} else {
						echo 'Error loading file';
						die();
					}
		 ?> 	
		 </p>
		<p>Hostel : 
		<?php 
			$xmlDoc = new DOMDocument();
					if(file_exists($_SERVER['DOCUMENT_ROOT'] . 'hostels_codes.xml')){
						$xmlDoc->load($_SERVER['DOCUMENT_ROOT'] . 'hostels_codes.xml');
						$node = $xmlDoc->getElementsByTagName("hostel");
						foreach ($node as $s_node) {
							$code = $s_node->getAttribute('code');
							if ($hostel == $code)
								echo $s_node->nodeValue;
						}
					} else {
						echo 'Error loading file';
						die();
					}
		 ?>
		 	
		 </p>
		<p>Password : Not Changed <?php echo "<a href='pass_change.php'>Change</a>"; ?></p>

<?php
		}
	} 
	else 
		//redirect to the home page
			Redirect( CURR_ADDR );
 ?>