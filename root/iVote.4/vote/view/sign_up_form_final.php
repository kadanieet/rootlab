<?php 
	$surname;
	$other_name;
	$study_year;
	$resident;
	$gender;
	$hostel;
	$school;

	if (!defined('ROOT_DIR'))
		define('ROOT_DIR', $_SERVER['DOCUMENT_ROOT']); 

	//Require files modules
		if ( file_exists(ROOT_DIR . 'utility_functions.php') )
			require_once ROOT_DIR . 'utility_functions.php';
		else
			exit('utility funtions files unvailable');

	Require_Files( ROOT_DIR . 'globalvars.php');


	if(isset($_POST['create']))
	{
		//Output_Form_Status();
		GLOBAL $surname;
		GLOBAL $other_name;
		GLOBAL $study_year;
		GLOBAL $resident;
		GLOBAL $gender;
		GLOBAL $hostel;


		Require_Files(ROOT_DIR . 'form_utilities.php');

		$surname = strtoupper(clean_input($_POST['surname']));
		$other_name = strtoupper(clean_input($_POST['other_name']));
		$study_year = clean_input($_POST['study_year']);
		$school = clean_input($_POST['school']);
		$resident = clean_input($_POST['resident']);
		$gender = clean_input($_POST['gender']);
		$hostel = clean_input($_POST['hostel']);
		$password_1 = clean_input($_POST['password_1']);
		$password_2 = clean_input($_POST['password_2']);


		if(empty($surname) || empty($other_name) || $_POST['study_year'] == "null" 
			|| $_POST['resident'] == "null" || $_POST['gender'] == "null" 
			|| ($_POST['hostel'] == "null" && $_POST['resident'] != 2) || empty($_POST['password_1'])
			|| empty($_POST['password_2']))
				Print_Message("You have not filled/selected all required fields");

		elseif($_POST['password_1'] != $_POST['password_2']) 
			Print_Message("The passwords don't match");

		elseif (!(preg_match("/^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).*$/", $_POST['password_1'])))
	        Print_Message("Invalid Password Format");

		else
		{
			
			if (session_status() == PHP_SESSION_NONE) session_start();
			//Output_Form_Status('no', 'no', 'yes');
			Require_Files(CONT_DIR . 'voter_controller.php');

			$controller = new voter_controller(clean_input($_POST['case']));
			$properties_array = array($_SESSION['reg_no'], $surname, $other_name, $study_year, $school, $resident, $gender, $hostel, $password_1);
			$signup_model = $controller->get_model($properties_array);
			$success = $signup_model->finalize_reg();

			if($success)
			{
				Print_Message("Profile updated successfully, redirecting to the portal");
				// Redirect to the portal after 5 seconds
				Redirect( VIEW_ADDR . 'portal.php', 3);
			}

		}
	}
 ?>
 
<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">

		<label for="surname">Surname</label>
			<input type="text" name="surname" value ="<?php GLOBAL $surname; echo "$surname"; ?>" placeholder="Surname"><br>
	    <label for="other_name">Other Name</label>
	    	<input type="text" name="other_name" value ="<?php GLOBAL $other_name; echo "$other_name"; ?>" placeholder="Other Name"><br>

	    <label for="study_year">Study Year</label>
	    	<select name="study_year">
			    <option value="null" <?php GLOBAL $study_year; if ($study_year == "null") echo "selected='selected'"; ?> >--Select--</option>
				    <?php 
							$xmlDoc = new DOMDocument();
							
							if(file_exists($_SERVER['DOCUMENT_ROOT'] . 'schools_initials_years.xml')){
								$xmlDoc->load($_SERVER['DOCUMENT_ROOT'] .'schools_initials_years.xml');
								$node = $xmlDoc->getElementsByTagName("school");
								
								foreach ($node as $s_node) {
									$initial = $s_node->getAttribute('initial'); 
									$reg_arr = str_split(strtoupper($_SESSION['reg_no']));
									if (!isset($_SESSION['reg_no']))
											$reg_arr[0] = 'P';

									if($initial == $reg_arr[0]) { 
										$years = $s_node->getAttribute('years');
										GLOBAL $school;
										$school = $reg_arr[0];

										foreach (range(1, $years) as $year) {
											echo "<option ";
											if ($study_year == $year) echo "selected='selected'";
											echo " value='" .$year. "'>".$year;
											echo "</option>";
										}
										break;		
									}
								}
						} else { exit('error loading file');}
						
				 	?>
			 </select><br>

		<input type="hidden" name="school" value='<?php GLOBAL $school; echo "$school"; ?>'>
		
		<label for="resident">School Resident</label>
			<select name="resident">
		    	<option value="null" <?php GLOBAL $resident; if ($resident == "null") echo "selected='selected'"; ?> >--Select--</option>
			  	<option value="2" <?php if ($resident == "2") echo "selected='selected'"; ?> >No</option>
			  	<option value="1" <?php if ($resident == "1") echo "selected='selected'"; ?> >Yes</option>
			</select><br>


	    <label for="gender">Gender</label>
		    <select name="gender">
			  <option value="null" <?php GLOBAL $gender; if ($gender == "null") echo "selected='selected'"; ?> >--Select--</option>
			  <option value="F" <?php if ($gender == "F") echo "selected='selected'"; ?> >Female</option>
			  <option value="M" <?php if ($gender == "M") echo "selected='selected'"; ?> >Male</option>
			</select><br>

	    <label for="hostel">Hostel Name</label>
		    <select name="hostel">
			    <option value="null" selected>---------Select--------</option>
			    		<?php 
							$xmlDoc = new DOMDocument();
							if(file_exists($_SERVER['DOCUMENT_ROOT'] . 'hostels_codes.xml')){
								$xmlDoc->load($_SERVER['DOCUMENT_ROOT'] . 'hostels_codes.xml');
								$node = $xmlDoc->getElementsByTagName("hostel");
								foreach ($node as $s_node) {
									$code = $s_node->getAttribute('code');
									echo "<option ";
									GLOBAL $hostel;
									if ($hostel == $code) echo "selected='selected'";
									echo " value='" .$code. "'>".$s_node->nodeValue;
									echo "</option>";
								}
							} else {
								echo 'Error loading file';
								die();
							}
						
				 	?>
		    </select><br>

		    <p>Create A Password for Your Account</p>
		    <label for="password_1">Enter Password</label>
		    	<input type="password" name="password_1" ><br>
			<label for="password_2">Re-Enter Password</label>
				<input type="password" name="password_2" ><br>

			<input type="hidden" name="case" value='signup'>
		    <input type="submit" name="create" value="Create Account">
		
		
</form>