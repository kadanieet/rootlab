<?php 

	define('ROOT_DIR', $_SERVER['DOCUMENT_ROOT']);

	//Require files modules
		if ( file_exists(ROOT_DIR . 'utility_functions.php') )
			require_once ROOT_DIR . 'utility_functions.php';
		else
			exit('System Not Available At The Moment, Try Later!');

		Require_Files(ROOT_DIR . 'globalvars.php');

	if(isset($_GET['email']) && !empty($_GET['email']) && 
		preg_match('/^[a-zA-Z0-9][a-zA-Z0-9\._\-&!?=#]*@/', $_GET['email']) && 
		preg_replace('/^[a-zA-Z0-9][a-zA-Z0-9\._\-&!?=#]*@/', '', $_GET['email']) == 'students.ku.ac.ke' && 
		isset($_GET['hash']) && !empty($_GET['hash'])){
			

			// get the form_utilities file for cleaning user inputs, connectvars and connection files
				$files = array( ROOT_DIR . 'form_utilities.php', CONT_DIR . 'voter_controller.php');
				Require_Files($files);

			// Verify data
			    $email = clean_input($_GET['email']); // Set email variable
			    $hash = clean_input($_GET['hash']); // Set hash variable

			$controller = new voter_controller('verify_signup');
			$properties_array = array($email, $hash);
			$verify_signup_model = $controller->get_model($properties_array);
			$status = $verify_signup_model->verify_user();

			if(gettype($status) == 'string')
				Redirect( $status );
			elseif(gettype($status) == 'array') 
				var_dump($status);
	}			

	else 
		//redirect to the home page
			Redirect( VOTE_ADDR );

 ?>