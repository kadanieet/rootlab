<?php 
	//globals for login user data 	
		$login_user = "";
		
	//log in form submit handler
		if (isset($_POST['login'])) {
			//form status
				//Output_Form_Status();
			if (empty($_POST['reg_no']) || empty($_POST['password'])) 
			      Print_Message("You forgot to enter your " .REGNOLB. " or Password<br/>");		
			
			//get the form_utilities file for cleaning user inputs, connectvars and connection files
				Require_Files(ROOT_DIR . 'form_utilities.php');

			GLOBAL $login_user;

			$login_user = strtoupper(clean_input($_POST['reg_no']));
			$login_pass = clean_input($_POST['password']);

			Require_Files(CONT_DIR . 'voter_controller.php');

			$controller = new voter_controller(clean_input($_POST['case']));
			$properties_array = array($login_user, $login_pass);
			$login_model = $controller->get_model($properties_array);
			$go_to = $login_model->verify_login();
			//echo VIEW_ADDR . $go_to;
			Redirect(VIEW_ADDR . $go_to);
			//Output_Form_Status('yes', 'yes');			    

			/*if(preg_match('/^[a-Z][1-3][0-9]\/\d{5}\/20[0null]\d{1}$/', $login_user)) {

			}*/
		} 
 ?>

<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
		<label for="reg_no"><?php echo REGNOLB; ?></label>
			<input type="text" name="reg_no" value="<?php GLOBAL $login_user; echo $login_user; ?>" placeholder="Registration Number"><br>
		<label for="password">Password</label>
			<input type="password" name="password"><br>
			<input type="hidden" name="case" value="voter_login">
			
			<input type="submit" name="login" value="Login">
</form>
