<?php 
	if (session_status() == PHP_SESSION_NONE) session_start();

	define('ROOT_DIR', $_SERVER['DOCUMENT_ROOT']); 

	//Require files modules
		if ( file_exists(ROOT_DIR . 'utility_functions.php') )
			require_once ROOT_DIR . 'utility_functions.php';
		else
			exit('System Not Available At The Moment, Try Later!');

	if (isset($_SESSION['user_id'])) {
		Require_File( ROOT_DIR . 'globalvars.php' );
		
		Require_File( CURR_DIR . 'nav_menu.php' );

	} 
	else 
		//redirect to the home page
			Redirect( CURR_ADDR );

		//Database connection	
			$files = array( ROOT_DIR . 'connectvars.php', ROOT_DIR . 'connection.php');
			Require_Files($files);

	$req_connection = new Db_Connection(DB_HOST, DB_NAME, DB_USER, DB_PASS);

	$connection = $req_connection->connect();

	if (isset($_POST['proceed'])) {
		$prezzo_id = $_POST['president'];
		$aspirant_query = "SELECT surname, other_names from aspirants where aspirant_id=" .$prezzo_id. "";
		$asp_stmt = $req_connection->SelectQuery($aspirant_query);

		echo "<h3>Confirm Your Selection</h3>";
		echo "<p><b>Position : President</b></p>";
		echo "<form method='post' action='".$_SERVER['PHP_SELF']."'>";
		foreach ($asp_stmt as $row) {
			echo "<p>you selected ";
			echo "<label for='president'>";
			echo $row['surname']. " " .$row['other_names'];
			echo "</label>";
			echo "<input type='hidden' name='president' value='" .$prezzo_id. "'>";
			echo "</p>";
		}
		echo "<p class='error'>Once you click OK, Your vote will be finalized<p>";
		echo "<input type='submit' name='commit' value='OK'>";
		echo "</form>";
	}

	elseif (isset($_POST['commit'])) {
		$prezzo_id = $_POST['president'];

		$commit_query = "update aspirants set votes = votes + 1 where aspirant_id=" .$prezzo_id. ""; 
		$cmt_stmt = $req_connection->InsertQuery($commit_query);
		
		//changed the voted status of the user to 1 and update the session value
		$voted_query = "UPDATE credentials SET voted = '1' WHERE user_id = '" . $_SESSION['user_id'] . "'";
		$vtd_stmt = $req_connection->InsertQuery($voted_query);

		$_SESSION['voted'] = 1;

		Print_Message("Your Selections have been submitted, 
			Kindly await the results. Redirecting to home.");
		
		Redirect ( CURR_ADDR . 'portal.php' );
	}
	
	else {
		if ($_SESSION['active'] == 0) 
			Redirect( CURR_ADDR . 'index.php?sign_up=final' );			
		else {
			$AspirantsQuery = "SELECT aspirant_id, surname, other_names from aspirants";
			
			$asp_data = $req_connection->SelectQuery($AspirantsQuery);

			if (count($asp_data) != 0) 
			{
				echo "<p><b>Position : President</b></p>";

					//set the checkbox for voting by checking the request mode
					
					if(isset($_GET['mode']))
					{
						
						echo "<form action='".$_SERVER['PHP_SELF']."' method='post'>";
							foreach ($asp_data as $row) {				
								echo "<p>Name " .$row['surname']. " " .$row['other_names']. "<input type='radio' value='".$row['aspirant_id']."' name='president' id='".$row['aspirant_id']."'></p>";
								
							}
							echo "<input type='submit' name='proceed' value='Proceed'>";
							echo "</form>";
					}
				
					else
						foreach ($asp_data as $row) 
							echo "<p>Name " .$row['surname']. " " .$row['other_names']. "</p>";
			}
		}
	}
 ?>