<?php 

class voter_data
{
	private $result = null; 
	
	function __construct()
	{
		// get the form_utilities file for cleaning user inputs, connectvars and connection files
			$files = array( ROOT_DIR . 'form_utilities.php', ROOT_DIR . 'connectvars.php', ROOT_DIR . 'connection.php');
			Require_Files($files);

		//Database connection	
			$req_connection = new Db_Connection(DB_HOST, DB_NAME, DB_USER, DB_PASS);
			$connection = $req_connection->connect();		
	}


	function __destruct()
	{
		$connection->disconnect();
	}

	private function deleteRecord($recordNumber) 
	{
		//delete a record
	 }

	private function readRecords($recordNumber) 
	 {
		
		if($recordNumber === "ALL") {
			return $this->dogs_array["dog"];
			
		}
		else 
		{
			return $this->dogs_array["dog"][$recordNumber];
		} 
	}

	private function insertRecords($records_array)
	{
		//mysql insert(PDO)

	}

	private function updateRecords($records_array)
	{	
		//update data
	}

	function processRecords($change_Type, $records_array)
	{

		switch($change_Type)
		{
		    
			case "Delete":
			   
				$this->deleteRecord($records_array);
				break;
			case "Insert":
				$this->insertRecords($records_array);
				break;
			case "Update":
				$this->updateRecords($records_array);
				break;
			case "Display":
				return $this->readRecords($records_array);
				break;
			default:
				throw new Exception("Invalid XML file change type: $change_Type");
		}

	}

	function SelectQuery($query)
	{
		$stmt = $connection->prepare($query);
		$stmt->execute();
		$this->result = $stmt->fetchALL(PDO::FETCH_ASSOC);
		return $this->result;
	}

	function InsertQuery($query)
	{
		$stmt = $connection->prepare($query);
		$stmt->execute();
		/*$this->result = $stmt->fetchALL(PDO::FETCH_ASSOC);
		return $this->result;*/
	}
}
 ?>