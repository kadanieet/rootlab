<?php 
	if (!defined('ROOT_DIR'))
		define('ROOT_DIR', $_SERVER['DOCUMENT_ROOT']); 

	//Require files modules
		if ( file_exists(ROOT_DIR . 'utility_functions.php') )
			require_once ROOT_DIR . 'utility_functions.php';
		else
			exit('utility funtions files unvailable');

	Require_Files( ROOT_DIR . 'globalvars.php');

	Require_Files(CONT_DIR . 'voter_controller.php');

	/**
	* 
	*/
	class signup
	{
		//initial sign up properties
		private $reg_no;
		private $email;

		//final sign up properties
		private $surname;
		private $other_name;
		private $study_year;
		private $school;
		private $sch_res;
		private $gender;
		private $hos_name;
		private $pass;

		//db connection properties
		private $db_conn;
		private $db_host;
		private $db_user;
		private $db_pass;
		private $db_name;

		private $tb_name;
		private $controller;

		function __construct($properties_array)
		{
			if(gettype($properties_array) == 'array')
			{	
				if(sizeof($properties_array) == 2)
				{
					$this->reg_no = $properties_array[0];
					$this->email = $properties_array[1];
				}
				elseif(sizeof($properties_array) == 9)
				{
					$this->reg_no = $properties_array[0];
					$this->surname = $properties_array[1];
					$this->other_name = $properties_array[2];
					$this->study_year = $properties_array[3];
					$this->school = $properties_array[4];
					$this->sch_res = $properties_array[5];
					$this->gender = $properties_array[6];
					$this->hos_name = $properties_array[7];
					$this->pass = $properties_array[8];
				}
			}
			$this->controller = new voter_controller('connection_model');
		}

		private function req_conn()
		{
			
			$this->set_credentials($this->controller);
			$properties_array = array($this->db_host, $this->db_user, $this->db_pass, $this->db_name);
			//echo $this->db_user;
			$this->db_conn = $this->controller->get_model($properties_array);
			$this->db_conn->connect();
		}

		public function verify_credentials()
		{
			$this->req_conn();
			$this->set_relation('credentials');
			$useridemails_array = $this->db_conn->SelectQuery("SELECT email, reg_no FROM " .$this->tb_name. "");
			$json = json_encode($useridemails_array);
			$valid_useridemails = json_decode($json, TRUE);

			$msg = 'Passed';
			foreach($valid_useridemails as $user)
			{
				if((in_array($this->email, $user)) && (in_array($this->reg_no, $user)))
					return array($this->reg_no, $this->email);
			    if(in_array($this->reg_no, $user))
					$msg = $msg . $this->reg_no;
				elseif(in_array($this->email, $user))
					$msg = $msg . $this->email;
			}

			return $msg;
		}

		function send_verfication_request()
		{
			// Generate random 32 character hash and assign it to a local variable.
				$hash = md5(rand(0,1000));

			// Generate random number between 1000 and 5000 and assign it to a local variable.
				$pass = rand(1000,5000);

			$this->db_conn->InsertQuery("INSERT INTO " .$this->tb_name. " (reg_no, email, password, hash) VALUES ('" .$this->reg_no."', '" .$this->email. "', SHA('" .$pass. "'), '" .$hash. "')");

			//send a link to the coporate email to verify student
				$to      = $this->email; // Send email to our user
				$subject = 'Signup | Verification'; // Give the email a subject 
				$message = '
				 
				Thanks for signing up!
				Your account has been created, you can login with the following credentials after you have activated your account by pressing the url below.
				 
				------------------------
				Username: '.$this->reg_no.'
				Password: '.$pass.'
				------------------------
				 
				Please click this link to activate your account:'
				. VIEW_ADDR . '/signup_verify.php?email=' . $this->email . '&hash=' . $hash . '';
				// Our message above including the link
				                     
				mail($to, $subject, $message); // Send our email or exit('error sending mail')
				return TRUE;
		}

		function finalize_reg()
		{
			$this->req_conn();
			$this->set_relation('credentials');
			$this->db_conn->UpdateQuery("UPDATE " . $this->tb_name . " 
										SET active = 1, password = SHA('" .$this->pass. "')
										WHERE user_id = '" . $this->reg_no . "'") or exit();

			$this->set_relation('voters');
			$this->db_conn->InsertQuery("INSERT INTO " .$this->tb_name. " 
										VALUES ('" . $this->reg_no . "', '" . $this->surname . "', '" 
										. $this->other_name . "', " . $this->study_year . ", '" 
										. $this->school . "', '" . $this->gender . "', '" 
										. $this->sch_res . "', " . to_uppercase($hos_name) . ")") or exit();
			return TRUE;
		}

		private function set_credentials()
		{
			
			$this->db_host = $this->controller->get_voter_credentials('db_host');
			$this->db_user = $this->controller->get_voter_credentials('db_user');
			$this->db_pass = $this->controller->get_voter_credentials('db_pass');
			$this->db_name = $this->controller->get_voter_credentials('db_name');
		}


		private function set_relation($rel_name)
		{
			$this->tb_name = $this->controller->get_voter_relations($rel_name);
		}

	}
 ?>