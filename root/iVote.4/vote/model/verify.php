<?php 
	/**
	* 
	*/
	if (session_status() == PHP_SESSION_NONE) session_start();

	if (!defined('ROOT_DIR'))
		define('ROOT_DIR', $_SERVER['DOCUMENT_ROOT']); 

	//Require files modules
		if ( file_exists(ROOT_DIR . 'utility_functions.php') )
			require_once ROOT_DIR . 'utility_functions.php';
		else
			exit('utility funtions files unvailable');

	$files = array(ROOT_DIR . 'globalvars.php', CONT_DIR . 'voter_controller.php');

	Require_Files($files);

	class verify
	{
		private $email;
		private $hash;

		private $tb_name;

		//private $connection;

		function __construct($properties)
		{
			$this->email = $properties[0];
			$this->hash = $properties[1];
		}

		public function verify_user()
		{
			$controller = new voter_controller('connection');
			$db_properties = array('db_host', 'db_user', 'db_pass', 'db_name');
			$connection = $controller->get_model($db_properties);
			$connection->request_connection();
			$this->tb_name = $connection->set_relation("credentials");
			$valid_users_array = $connection->execute_query("SELECT user_id, reg_no, hash, email, voted, active FROM " .$this->tb_name);
			//var_dump($valid_users_array);
			$json = json_encode($valid_users_array);
			$valid_users = json_decode($json, TRUE);
			foreach($valid_users as $user)
			{
			    if (in_array($this->email, $user) && in_array($this->hash, $user))
				{					
						
							if($user['active'] == '1') 
								return array('msg' => 'Account Already Verified', 'url' => CURR_ADDR);
							
							elseif($user['active'] == '0')
								return  VIEW_ADDR . 'sign_up_form_final.php';

							break;
							/*$_SESSION['reg_no'] = $this->user;
							$_SESSION['password'] = $this->pass;
							$_SESSION['voted'] = $user['voted'];*/
								 		
				}
						
			}
			return VOTE_ADDR;
		}
	}
 ?>