CREATE DATABASE  ivote;

USE ivote;

CREATE TABLE voters(
	voter_id mediumint not null,
	surname varchar(15) not null,
	other_names varchar(15) not null,
	study_year tinyint not null,
	school varchar(30) not null,
	residence tinyint not null,
	hostel varchar(30),
	room_no smallint
);

CREATE TABLE aspirants(
	aspirant_id tinyint not null primary key auto_increment,
	image_id varchar(10) not null,
	reg_no varchar(20) not null,
	surname varchar(15) not null,
	other_names varchar(15) not null,
	study_year tinyint not null,
	school varchar(30) not null,
	resident tinyint not null,
	hostel varchar(30),
	room_no smallint
);

CREATE TABLE credentials(
	user_id mediumint not null primary key auto_increment,
	reg_no varchar(15) not null,
	email varchar(30) not null,
	password varchar(30) not null
);

