<!DOCTYPE html>
<html>
<head>
	<title>View Aspirants - iVote</title>
</head>
<body>

<?php 
	require_once('connectvars.php');

	$db = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME) or die('Error: '. mysqli_error());

	$AspirantsQuery = "SELECT aspirant_id, reg_no, surname, other_names, study_year, school, resident, hostel, room_no from aspirants";

	$aspirants_data = mysqli_query($db, $AspirantsQuery) or die('Error :'. mysqli_error($db));

	if (mysqli_num_rows($aspirants_data) != 0) {

		echo "<table>";
		echo "<tr><th>Position : President</th></tr>";
		echo "<tr><td>Name</td><td>Registration Number</td><td>Study Year</td><td>School</td><td>Residence</td><td>Hostel</td><td>Room Number</td></tr>";

		while ($row = mysqli_fetch_array($aspirants_data)) {
			if( isset($_GET['mode']) && $_GET['mode'] == 'delete')
			{
				echo "<tr>";
				echo "<td>" .$row['surname']. " " .$row['other_names']. "</td>";
				echo "<td>" .$row['reg_no']. "</td>";
				echo "<td>" .$row['study_year']. "</td>";
				echo "<td>" .$row['school']. "</td>";
				echo "<td>" .$row['resident']. "</td>";
				echo "<td>" .$row['hostel']. "</td>";
				echo "<td>" .$row['room_no']. "</td>";
				echo "<td><a href='delete.php?asp_id=" .$row['aspirant_id']. "'>poof!</a></td>";
				echo "</tr>";
			}
			else {
				echo "<tr>";
				echo "<td>" .$row['surname']. " " .$row['other_names']. "</td>";
				echo "<td>" .$row['reg_no']. "</td>";
				echo "<td>" .$row['study_year']. "</td>";
				echo "<td>" .$row['school']. "</td>";
				echo "<td>" .$row['resident']. "</td>";
				echo "<td>" .$row['hostel']. "</td>";
				echo "<td>" .$row['room_no']. "</td>";
				echo "<td><a href='delete.php?asp_id=" .$row['aspirant_id']. "'>poof!</a></td>";
				echo "</tr>";
			}
		}
		echo "</table>";
	}

 ?>

</body>
</html>