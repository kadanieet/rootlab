/*With the root user password table*/
CREATE DATABASE  ivote;

USE ivote;

CREATE TABLE voters(
	voter_id MEDIUMINT NOT NULL PRIMARY KEY,
	surname VARCHAR(15) NOT NULL,
	other_names VARCHAR(15) NOT NULL,
	study_year TINYINT NOT NULL,
	school VARCHAR(30) NOT NULL,
	residence TINYINT NOT NULL,
	hostel VARCHAR(30),
	room_no SMALLINT
);

/*less writes but more reads*/
CREATE TABLE aspirants(
	aspirant_id SMALLINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	image_id VARCHAR(10) NOT NULL,
	reg_no VARCHAR(20) NOT NULL,
	surname VARCHAR(15) NOT NULL,
	other_names VARCHAR(15) NOT NULL,
	position TINYINT NOT NULL,
	study_year TINYINT NOT NULL,
	school VARCHAR(30) NOT NULL,
	resident TINYINT NOT NULL,
	hostel VARCHAR(30),
	room_no SMALLINT
);

/*less writes but more reads*/
CREATE TABLE credentials(
	user_id SMALLINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	reg_no VARCHAR(15) NOT NULL,
	email VARCHAR(30) NOT NULL,
	password VARCHAR(30) NOT NULL
);

CREATE TABLE nimda(
	password VARCHAR(15) NOT NULL
);

CREATE TABLE root(
	password VARCHAR(15) NOT NULL
);

/*each position alone*/
CREATE TABLE votes(
    aspirant_id SMALLINT NOT NULL PRIMARY KEY,
    votes SMALLINT NOT NULL DEFAULT 0
);

