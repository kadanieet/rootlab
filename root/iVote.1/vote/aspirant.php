<?php 
	
	class Aspirant
	{
		//instance variables
		private $reg_no = "none";
		private $surname = "none";
		private $other_name  = "none";
		private $study_year = 0;
		private $school = "none";
		private $residence = 0;
		private $hostel = "none"; 
		private $room_no = 0;

		//Aspirants constructor
		function __construct($details_array)
		{
			
		}

		//----------set methods--------------
		function set_reg_no($value)
		{
			$this->reg_no = $value;
		}

		function set_surname($value)
		{
			$this->surname = $value;
		}

		function set_other_name($value)
		{
			$this->other_name = $value;
		}

		function set_study_year($value)
		{
			$this->study_year = $value;
		}

		function set_school($value)
		{
			$this->school = $value;
		}

		function set_residence($value)
		{
			$this->residence = $value;
		}

		function set_hostel($value)
		{
			$this->hostel = $value;
		}

		function set_room_no($value)
		{
			$this->room_no = $value;
		}

		//----------get methods--------------
		function get_reg_no()
		{	return $this->reg_no; }

		function get_surname()
		{	return $this->surname;}

		function get_other_name()
		{	return $this->other_name;}

		function get_study_year()
		{	return $this->study_year;}

		function get_school()
		{	return $this->school;}

		function get_residence()
		{	return $this->residence;}

		function get_hostel()
		{	return $this->hostel;}

		function get_room_no()
		{	return $this->room_no;}
	}
 ?>