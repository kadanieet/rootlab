<?php 
	require_once('connectvars.php');

	$db = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME) or die('Error: '. mysqli_error());

	$user_id = $_GET['user_id'];

	//Initialize the details with no values
	$surname = "";
	$other_names = "";
	$study_year = "";
	$hostel = "";
	$school = "";
	$residence = "";
	$room_no = "";

	$reg_no = "";
	$email = "";
	$password = "";

	$VoterQuery = "SELECT surname, other_names, study_year, school, residence, hostel, room_no FROM voters WHERE voter_id = '" . $user_id. "'";

	$CredQuery = "SELECT reg_no, email, password FROM credentials where user_id = '" .$user_id. "'";

	$voter_data = mysqli_query($db, $VoterQuery) or die('Error :'. mysqli_error($db));

	$cred_data = mysqli_query($db, $CredQuery) or die('Error :'. mysqli_error($db));

	if (mysqli_num_rows($voter_data) != 0 && mysqli_num_rows($cred_data) != 0) {
		$voter_row = mysqli_fetch_array($voter_data);
			$surname = $voter_row['surname'];
			$other_names = $voter_row['other_names'];
			$study_year = $voter_row['study_year'];
			$hostel = $voter_row['hostel'];
			$school = $voter_row['school'];
			$residence = $voter_row['residence'];
			$room_no = $voter_row['room_no'];

		$cred_row = mysqli_fetch_array($cred_data);
			$reg_no = $cred_row['reg_no'];
			$email = $cred_row['email'];
			$password = $cred_row['password'];
	}

 ?>

<!DOCTYPE html>
<html>
<head>
	<title>Edit profile - iVote</title>
</head>
<body>
<form action="" method="post">
	<p>Registration number <?php echo "$reg_no"; ?></p>
	<p>Current Name <?php echo "$surname $other_names"; ?></p>
	<input type="text" name="surname">
	<p>Current Email <?php echo "$email"; ?></p>
	<input type="text" name="email">
	<p>Current Hostel <?php echo "$hostel"; ?></p>
	<input type="text" name="hostel">
	<p>Current Room Number <?php echo "$room_no"; ?></p>
	<input type="number" name="room_no">
	<p>Current Password <?php echo "$password"; ?></p>
	<input type="password" name="password">
</form>
</body>
</html>