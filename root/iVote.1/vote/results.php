<?php 
require_once('connectvars.php');

$db = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME) or die('Error: '. mysqli_error());

//Join the votes and aspirants tables to get the results
$results_query = "SELECT surname, other_names, votes FROM aspirants, votes WHERE votes.aspirant_id = aspirants.aspirant_id";

$results_data = mysqli_query($db, $results_query) or die('Error :'. mysqli_error($db));

 ?>

<p>President</p>
<table>
	<tr><th>Name</th><th>Votes Gannered</th></tr>
<?php 
	if(mysqli_num_rows($results_data) != 0 )
	{
		while ($row = mysqli_fetch_array($results_data)) {
			echo "<tr>";
				echo "<td>" .$row['surname']. " " .$row['other_names']. "</td>";
				echo "<td>" .$row['votes']. "</td>";
			echo "</tr>";
		}
	}
 ?>
</table>