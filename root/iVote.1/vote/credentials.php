<?php 
	
	class Credentials
	{
		//instance variables
		private $reg_no = "none";
		private $email = "none";
		private $password  = "none";

		//credentials constructor
		function __construct($details_array)
		{
			
		}

		//----------set methods--------------
		function set_reg_no($value)
		{
			$this->reg_no = $value;
		}

		function set_email($value)
		{
			$this->email = $value;
		}

		function set_password($value)
		{
			$this->password = $value;
		}

		//----------get methods--------------
		function get_reg_no()
		{	return $this->reg_no;}

		function get_email()
		{	return $this->email;}

		function get_password()
		{	return $this->password;}
	}
 ?>