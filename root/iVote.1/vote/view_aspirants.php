<!DOCTYPE html>
<html>
<head>
	<title>View Aspirants - iVote</title>
</head>
<body>

<?php 

	require_once('connectvars.php');

	$db = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME) or die('Error: '. mysqli_error());

	$name = "";
	$position = "";

	$AspirantsQuery = "SELECT aspirant_id, surname, other_names from aspirants";

	$aspirants_data = mysqli_query($db, $AspirantsQuery) or die('Error :'. mysqli_error($db));

	if (mysqli_num_rows($aspirants_data) != 0) {
		echo "<p><b>Position : President</b></p>";

		//set the checkbox for voting by checking the request mode
			if(isset($_GET['mode']) && $_GET['user_id'])
			{
				$user_id = $_GET['user_id'];
				
				echo "<form action='confirm_selection.php?user_id=" .$user_id. "' method='post'>";
					
					while ($row = mysqli_fetch_array($aspirants_data)) {
						echo "<p>Name " .$row['surname']. " " .$row['other_names']. "<input type='radio' value='".$row['aspirant_id']."' name='president' id='".$row['aspirant_id']."'></p>";
					}
					echo "<input type='submit' name='submit' value='Proceed'>";
					echo "</form>";
			}
		
			else
			{
				while ($row = mysqli_fetch_array($aspirants_data)) {
					echo "<p>Name " .$row['surname']. " " .$row['other_names']. "</p>";
				}
			}
				
	}

 ?>

</body>
</html>