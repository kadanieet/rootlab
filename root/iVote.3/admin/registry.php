<?php 
	session_start();
	if(isset($_SESSION['admin'])) {
		require_once $_SERVER['DOCUMENT_ROOT'] . '/admin/admin_menu.php';
	} 
	else {
		//redirect to the home page
	$home_url = 'http://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/';
	header('Location: ' . $home_url);
	}

	if (file_exists($_SERVER['DOCUMENT_ROOT'] .'connectvars.php') && file_exists($_SERVER['DOCUMENT_ROOT'] . 'connection.php')) {
			require_once($_SERVER['DOCUMENT_ROOT'] . 'connectvars.php');
			require_once($_SERVER['DOCUMENT_ROOT'] . 'connection.php');	
		} else {
			exit('file error');
		}

	$req_connection = new Db_Connection(DB_HOST, DB_NAME, DB_USER, DB_PASS);

	$connection = $req_connection->connect();

	require_once $_SERVER['DOCUMENT_ROOT'] . 'root/category_sort.php';

	if (isset($_POST['sort'])) {
		$category = $_POST['category'];
		require $_SERVER['DOCUMENT_ROOT'] . 'root/sort_form.php';

		//Total number of registered voters
		if($category == 'none')
			category_sort($connection);	
		//sort by school
		elseif ($category == 'school')
			category_sort($connection, 'schools_initials.xml', $category);
		//sort by gender
		elseif ($category == 'gender')
			category_sort($connection, 'genders.xml', $category);
		//sort by hostel
		elseif ($category == 'hostel')
			category_sort($connection, 'hostels_initials.xml', $category);
	}
	else {
		//Total number of registered voters
		category_sort($connection);
		$category = 'none';
	}
		require $_SERVER['DOCUMENT_ROOT'] . 'root/sort_form.php';
 ?>