import print_lol

def xml_with_attr(root_element_title, child_element_name, attribute_name, text_file):
    dirty_xml = []
    (file_name, ext) = text_file.split('.', 1)
    try:
        with open('xml_declaration.xml') as declaration:
            for dec in declaration:
                clean_dec = dec.strip()
                dirty_xml.append(clean_dec)
    except IOError as err:
        print('File error: ' + str(err))

    dirty_xml.append('<' + str(root_element_title.strip()) + '>')

    try:
        with open(text_file) as file:
            for each_line in file:
                clean_line = each_line.strip()
                (school, initial) = clean_line.split('-', 1)
                dirty_xml.append('<' + str(child_element_name.strip()) + ' ' + str(attribute_name.strip()) + '="' + str(initial.strip()) + '">' + str(school.strip()) + '</' + str(child_element_name.strip()) + '>')

    except IOError as err:
        print ('File error: ' + str(err))

    dirty_xml.append('</' + str(root_element_title) + '>')

    with open(file_name + '.xml', 'w') as clean_xml:
        print_lol.print_lol(dirty_xml, fh=clean_xml)
