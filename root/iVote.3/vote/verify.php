<?php 
	if(isset($_GET['email']) && !empty($_GET['email']) && isset($_GET['hash']) && !empty($_GET['hash'])){
			
			define('ROOT_DIR', $_SERVER['DOCUMENT_ROOT']);

			//Require files modules
				if ( file_exists(ROOT_DIR . 'utility_functions.php') )
					require_once ROOT_DIR . 'utility_functions.php';
				else
					exit('System Not Available At The Moment, Try Later!');

			// get the form_utilities file for cleaning user inputs, connectvars and connection files
				$files = array( ROOT_DIR . 'form_utilities.php', ROOT_DIR . 'connectvars.php', ROOT_DIR . 'connection.php', ROOT_DIR . 'globalvars.php');
				Require_Files($files);

			//Database connection	
				$req_connection = new Db_Connection(DB_HOST, DB_NAME, DB_USER, DB_PASS);
				$connection = $req_connection->connect();

			// Verify data
			    $email = clean_input($_GET['email']); // Set email variable
			    $hash = clean_input($_GET['hash']); // Set hash variable

			$qeury = "SELECT user_id, reg_no, email, voted FROM credentials where email = '" . $email . "' AND hash = '" . $hash . "'";

			$row = $req_connection->SelectQuery($qeury);

			if (count($row) < 1) {
				//No match -> invalid url or account has already been activated.
        			PrintMessage('The url is either invalid or you already have activated your account.');

        		//redirect to the home page after 5 seconds
					Redirect( CURR_ADDR , 5);
			}

			else {
				if (session_status() == PHP_SESSION_NONE) session_start();

				foreach ($row as $row_1) {
						
							$_SESSION['user_id'] = $row_1['user_id'];
							$_SESSION['reg_no'] = $row_1['reg_no'];
							$_SESSION['email'] = $row_1['email'];
							$_SESSION['voted'] = $row_1['voted'];
							
						}
				//redirect to final sign up form
					Redirect( CURR_ADDR . 'index.php?sign_up=final');	
				}

	}

	else 
		//redirect to the home page
			Redirect( CURR_ADDR );

 ?>