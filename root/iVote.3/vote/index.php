
<?php
	if (session_status() == PHP_SESSION_NONE) session_start();

	define('ROOT_DIR', $_SERVER['DOCUMENT_ROOT']); 

	//Require files modules
		if ( file_exists(ROOT_DIR . 'utility_functions.php') )
			require_once ROOT_DIR . 'utility_functions.php';
		else
			exit('System Not Available At The Moment, Try Later!');

	Require_File( ROOT_DIR . 'globalvars.php');
	
	//redirect to the portal if user had logged in
		if(isset($_SESSION['user_id']) && empty($_GET['sign_up']) 
			&& !isset($_GET['sign_up']) && $_GET['sign_up'] != 'final') 
				Redirect( CURR_ADDR . 'portal.php' );

	function Output_Form_Status($login = 'no', $sign_init = 'no', $sign_final = 'no')
	{
		GLOBAL $output_login_form;
		GLOBAL $output_sign_up_initial_form;
		GLOBAL $output_sign_up_final_form;

		$output_login_form = $login;
		$output_sign_up_initial_form = $sign_init;
		$output_sign_up_final_form = $sign_final;
	}

	//output the final sign up form only from the verify stage
		if (!empty($_GET['sign_up']) && isset($_GET['sign_up']) 
			&& $_GET['sign_up'] == 'final' && !isset($_POST['create'])) {

				Output_Form_Status('no', 'no', 'yes');
				Print_Message("Enter your personal details to finalize your account");
		} 
		else
			Output_Form_Status('yes', 'yes');

	//Set form display
		if ($output_login_form == 'yes' && $output_sign_up_initial_form == 'yes' && $output_sign_up_final_form == 'no') {
			$files = array( CURR_DIR . 'login_form.php', CURR_DIR . 'sign_up_form_initial.php');
			Require_Files($files);
		}

		if($output_sign_up_final_form == 'yes' && $output_login_form == 'no' && $output_sign_up_initial_form == 'no') 
				Require_File( CURR_DIR . 'sign_up_form_final.php');
		
		else 
			Output_Form_Status('yes', 'yes');

	// get the form_utilities file for cleaning user inputs, connectvars and connection files
		$files = array( ROOT_DIR . 'form_utilities.php', ROOT_DIR . 'connectvars.php', ROOT_DIR . 'connection.php');
		Require_Files($files);

	//Database connection	
		$req_connection = new Db_Connection(DB_HOST, DB_NAME, DB_USER, DB_PASS);
		$connection = $req_connection->connect();

	//globals for login user data 	
		$login_user = "";

	//globals for initial sign up user data 
		$reg_no_1 = "";
		$reg_no_2 = "";
		$email = "";

	//globals for final sign up user data 
		$surname = "";
		$other_name = "";
		$study_year = "null";
		$resident = "null";
		$gender = "null";
		$hostel = ""; 




	//log in form submit handler
		if (isset($_POST['login'])) {
			//form status
				Output_Form_Status();

			GLOBAL $login_user;
			
			$login_user = strtoupper(clean_input($_POST['reg_no']));
			$login_pass = clean_input($_POST['password']);

			Output_Form_Status('yes', 'yes');

			if (empty($login_user)) 
			      Print_Message("You forgot to enter your username");			

			elseif (empty($login_pass)) 
				// login password is blank
			      Print_Message("You forgot to enter your password");
			    

			/*if(preg_match('/^[a-Z][1-3][0-9]\/\d{5}\/20[0null]\d{1}$/', $login_user)) {

			}*/

			else 
				if( $connection != null) 
					Require_File( CURR_DIR . 'verify_pass.php');
		}





	//initial sign up form submit handler
		elseif(isset($_POST['signup'])){
			//no form output
				Output_Form_Status();

			//initial sign up users input	
				$reg_no_1 = strtoupper(clean_input($_POST['reg_no_1']));
				$reg_no_2 = strtoupper(clean_input($_POST['reg_no_2']));
				$email = strtolower(clean_input($_POST['email']));

				echo CURR_DIR;

			//form handler
			Require_File( CURR_DIR . 'sign_up_handler.php');

			//Output the initial sign up and log in form incase of inappropriate input
				Output_Form_Status('yes', 'yes');

			if (empty($reg_no_1) || empty($reg_no_2)) 
				Print_Message('You have forgotten to enter your registration number');

			elseif ($reg_no_1 != $reg_no_2) 
				Print_Message("The registration numbers don't match");

			elseif (empty($email)) 				
				Print_Message("You have forgotten to enter your email");

			// $email is invalid because LocalName is bad
				elseif (!preg_match('/^[a-zA-Z0-9][a-zA-Z0-9\._\-&!?=#]*@/', $email))  
				    Print_Message("Your email address is invalid");

	     	// Strip out everything but the domain from the email and check if $domain is valid
			    elseif (preg_replace('/^[a-zA-Z0-9][a-zA-Z0-9\._\-&!?=#]*@/', '', $email) != 'students.ku.ac.ke') 
			    	Print_Message("Your email address is not a valid Kenyatta University Coporate email.");
			 	
			//data has no errors
				else {
			
					$reg_query = "SELECT user_id FROM credentials WHERE reg_no='" .$reg_no_1. "'";
					$email_query = "SELECT user_id FROM credentials WHERE email='"  .$email. "'";

					$reg_row = $req_connection->SelectQuery($reg_query);
					$email_row = $req_connection->SelectQuery($email_query);					

					// confirm that the credentials are not in the database
						if (count($reg_row) != 0 && count($email_row) == 0) 
							Print_Message("That registration number is already registered");

						elseif (count($email_row) != 0 && count($reg_row) == 0) 
							Print_Message("That coporate email is already registered");
				
						elseif (count($reg_row) != 0 && count($email_row) != 0) 
						 	Print_Message("Both the registration number and coporate email are already registered"); 

						else {
							Output_Form_Status();

							// Generate random 32 character hash and assign it to a local variable.
								$hash = md5( rand(0,1000) );

							// Generate random number between 1000 and 5000 and assign it to a local variable.
								$pass = rand(1000,5000);

							$query = "INSERT INTO credentials(reg_no, email, password, hash) VALUES ('" .$reg_no_1."', '" .$email. "', SHA('" .$pass. "'), '" .$hash. "')";

							$req_connection->InsertQuery($query);

							//send a link to the coporate email to verify student
								$to      = $email; // Send email to our user
								$subject = 'Signup | Verification'; // Give the email a subject 
								$message = '
								 
								Thanks for signing up!
								Your account has been created, you can login with the following credentials after you have activated your account by pressing the url below.
								 
								------------------------
								Username: '.$reg_no_1.'
								Password: '.$pass.'
								------------------------
								 
								Please click this link to activate your account:'
								. CURR_ADDR . '/verify.php?email=' . $email . '&hash=' . $hash . '';
								// Our message above including the link
								                     
								mail($to, $subject, $message); // Send our email

								Print_Message("Log in into your email to verify your account!");

								Redirect( CURR_ADDR , 10);

						}
					}

		}


	



	//Final sign up form handler
		elseif (isset($_POST['create'])) {
			Output_Form_Status();

			$surname = strtoupper(clean_input($_POST['surname']));
			$other_name = strtoupper(clean_input($_POST['other_name']));
			$study_year = clean_input($_POST['study_year']);
			$school = clean_input($_POST['school']);
			$resident = clean_input($_POST['resident']);
			$gender = clean_input($_POST['gender']);
			$hostel = clean_input($_POST['hostel']);
			$password_1 = clean_input($_POST['password_1']);
			$password_2 = clean_input($_POST['password_2']);

					Output_Form_Status('no', 'no', 'yes');
					
					if($surname == "") 
						Print_Message("You have forgotten to enter your surname");
						
					elseif($other_name == "") 
						Print_Message("You have forgotten to enter your other name");
							
					elseif($study_year == "null") 
						Print_Message("You have forgotten to select your study year");
							
					elseif($resident == "null") 
						Print_Message("You have forgotten to select your residence");
							
					elseif($gender == "null") 
						Print_Message("You have forgotten to select your gender");
							
					elseif($hostel == "null" && $resident != 2) 
						Print_Message("You have forgotten to select your hostel");
							
					elseif($password_1 == "" || $password_2 == "") 
						Print_Message("You have forgotten to enter your passwords");
							
					elseif($password_1 != $password_2) 
						Print_Message("The passwords don't match");

					else{
						if (session_status() == PHP_SESSION_NONE) session_start();
						
						//update the password in the credentials table
						$cred_q = "UPDATE credentials SET active = 1, password = SHA('" .$password_1. "') WHERE user_id = '" . $_SESSION['user_id'] . "'";

						$req_connection->InsertQuery($cred_q);

						if ($hostel == "null" && $resident == 2) 
							//update details in the voters table
							$voter_q = "INSERT INTO voters VALUES ('" . $_SESSION['user_id'] . "', '" . $surname . "', '" . $other_name . "', " . $study_year . ", '" . $school . "', '" . $gender . "', '" . $resident . "', " . strtoupper($hostel) . ")";
	
						else 
							$voter_q = "INSERT INTO voters VALUES ('" . $_SESSION['user_id'] . "', '" . $surname . "', '" . $other_name . "', " . $study_year . ", '" . $school . "', '" . $gender . "', '" . $resident . "', '" . $hostel . "')";
						
						

						$req_connection->InsertQuery($voter_q);

						Print_Message("Account created successfully, redirecting to the portal");

						// Redirect to the portal after 5 seconds
							Redirect( CURR_ADDR . 'portal.php', 5);

					}
		} 

	//if (!$req_connection->disconnect()) exit("Can't disconnect");
 ?>