<?php
	if (session_status() == PHP_SESSION_NONE) session_start();

	define('ROOT_DIR', $_SERVER['DOCUMENT_ROOT']); 

	//Require files modules
		if ( file_exists(ROOT_DIR . 'utility_functions.php') )
			require_once ROOT_DIR . 'utility_functions.php';
		else
			exit('System Not Available At The Moment, Try Later!');

	if (isset($_SESSION['user_id'])) {
		Require_File( ROOT_DIR . 'globalvars.php' );
		
		Require_File( CURR_DIR . 'nav_menu.php' );

	} 
	else 
		//redirect to the home page
			Redirect( CURR_ADDR );

		//Database connection	
			$files = array( ROOT_DIR . 'connectvars.php', ROOT_DIR . 'connection.php');
			Require_Files($files);

	$req_connection = new Db_Connection(DB_HOST, DB_NAME, DB_USER, DB_PASS);

	$connection = $req_connection->connect();
	
			$xmlDoc = new DOMDocument();
			if(file_exists($_SERVER['DOCUMENT_ROOT'] . 'voted_congress_codes.xml')) {
				
				$xmlDoc->load($_SERVER['DOCUMENT_ROOT'] . 'voted_congress_codes.xml');
				$node = $xmlDoc->getElementsByTagName("position");
				
				foreach ($node as $s_node) {
					$pos_code = $s_node->getAttribute('code');
					//$pos_name = $s_node->nodeValue;
					$results_query = "SELECT surname, other_names, position, votes FROM aspirants where position = '" .$pos_code. "' ORDER BY VOTES DESC";
					
					$results_stmt = $req_connection->SelectQuery($results_query);

					echo "<p>Position : $s_node->nodeValue </p>";
					echo "<table>";
					echo "<tr><th>Name</th><th>Votes Gannered</th></tr>";
					
					foreach ($results_stmt as $row) {
						echo "<tr>";
							echo "<td>" .$row['surname']. " " .$row['other_names']. "</td>";
							echo "<td>" .$row['votes']. "</td>";
						echo "</tr>";
					}
					echo "</table>";
				}
			} else {die('Error loading file'); }

 ?>
