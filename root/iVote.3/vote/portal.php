<?php 
	if (session_status() == PHP_SESSION_NONE) session_start();

	define('ROOT_DIR', $_SERVER['DOCUMENT_ROOT']); 

	//Require files modules
		if ( file_exists(ROOT_DIR . 'utility_functions.php') )
			require_once ROOT_DIR . 'utility_functions.php';
		else
			exit('System Not Available At The Moment, Try Later!');

	if(isset($_SESSION['user_id'])) {

		Require_File( ROOT_DIR . 'globalvars.php' );
		
		$files = array( CURR_DIR . 'nav_menu.php', CURR_DIR . 'profile.php');
		Require_Files($files);

	} 
	else 
		//redirect to the home page
			Redirect( CURR_ADDR );

 ?>