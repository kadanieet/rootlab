<label for="school">School</label><select name="school">
    <option value="none" selected>--Select--</option>
    <?php 
    		$xmlDoc = new DOMDocument();
			if(file_exists('schools_initials.xml')){
				$xmlDoc->load('schools_initials.xml');
				$node = $xmlDoc->getElementsByTagName("school");
				foreach ($node as $s_node) {
					$initial = $s_node->getAttribute('initial');
					echo "<option value='" .$initial. "'>".$s_node->nodeValue;
					echo "</option>";
				}
			}
			
	 	?>
    </select><br>