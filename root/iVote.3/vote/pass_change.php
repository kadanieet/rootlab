<?php 
	if (session_status() == PHP_SESSION_NONE) session_start();

	define('ROOT_DIR', $_SERVER['DOCUMENT_ROOT']); 

	//Require files modules
		if ( file_exists(ROOT_DIR . 'utility_functions.php') )
			require_once ROOT_DIR . 'utility_functions.php';
		else
			exit('System Not Available At The Moment, Try Later!');

	// get the form_utilities file for cleaning user inputs
		$files = array(ROOT_DIR . 'globalvars.php', ROOT_DIR . 'form_utilities.php');
		Require_Files( $files );

	$output_pass_change_form = 'yes';
	
	if (isset($_POST['update']) && $output_pass_change_form = 'no') {
		$output_pass_change_form = 'no';
		
		$password_old = clean_input($_POST['password_old']);
		$password_new_1 = clean_input($_POST['password_new_1']);
		$password_new_2 = clean_input($_POST['password_new_2']);

		if (empty($password_old) || empty($password_new_1) || empty($password_new_2)) {
			$output_pass_change_form = 'yes';
			Print_Message("Enter data in all fields!");
		}
		elseif ($password_new_1 != $password_new_2) {
			$output_pass_change_form = 'yes';
			Print_Message("The new passwords entered do not match, try again!");
		}
		else {
			//Database connection	
			$files = array( ROOT_DIR . 'connectvars.php', ROOT_DIR . 'connection.php');
			Require_Files($files);

			$req_connection = new Db_Connection(DB_HOST, DB_NAME, DB_USER, DB_PASS);

			$connection = $req_connection->connect();

			Require_File( CURR_DIR . 'verify_pass.php');
			}		
	}

	if($output_pass_change_form = 'yes' && !isset($_POST['update'])) {
 ?>

<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
	<p>Password is case sensitive </p>
	    <label for="password_1">Enter Current Password</label>
	    	<input type="password" name="password_old" ><br>
	    <label for="password_1">Enter New Password</label>
	    	<input type="password" name="password_new_1" ><br>
		<label for="password_2">Re-Enter New Password</label>
			<input type="password" name="password_new_2" ><br>
	    <input type="submit" name="update" value="Update Password">
</form>

<?php 
	}
 ?>