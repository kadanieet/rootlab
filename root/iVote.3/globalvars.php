<?php 
	if (!defined('ROOT_DIR'))
		define('ROOT_DIR', $_SERVER['DOCUMENT_ROOT']);
	
	define('CURR_ADDR', 'http://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/');
	define('CURR_DIR', $_SERVER['DOCUMENT_ROOT'] . dirname($_SERVER['PHP_SELF']). '/');
	define('SYS_ERR', 'System Not Available At The Moment, Try Later!');
 ?>