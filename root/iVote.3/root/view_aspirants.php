<?php 
	session_start();
	if(isset($_SESSION['root'])) {
		require_once $_SERVER['DOCUMENT_ROOT'] . '/root_nav_menu.php';
	} 
	else {
		//redirect to the home page
	$home_url = 'http://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/';
	header('Location: ' . $home_url);
	}
	//globals for user data 
	$reg_no_1 = "";
	$reg_no_2 = "";
	$email = "";
	$surname = "";
	$position = "";
	$other_name = "";
	$study_year = "null";
	$resident = "null";
	$gender = "null";
	$hostel = "";
	$login_user = "";
	$reg_no = "";
	
	if (file_exists($_SERVER['DOCUMENT_ROOT'] .'connectvars.php') && file_exists($_SERVER['DOCUMENT_ROOT'] . 'connection.php')) {
			require_once($_SERVER['DOCUMENT_ROOT'] . 'connectvars.php');
			require_once($_SERVER['DOCUMENT_ROOT'] . 'connection.php');	
		} else {
			exit('file error');
		}

	$req_connection = new Db_Connection(DB_HOST, DB_NAME, DB_USER, DB_PASS);

	$connection = $req_connection->connect();

	$xmlDoc = new DOMDocument();
			if(file_exists($_SERVER['DOCUMENT_ROOT'] . 'voted_congress_codes.xml')) {
				
				$xmlDoc->load($_SERVER['DOCUMENT_ROOT'] . 'voted_congress_codes.xml');
				$node = $xmlDoc->getElementsByTagName("position");
				
				foreach ($node as $s_node) {
					$pos_code = $s_node->getAttribute('code');

					$aspirants_query = "SELECT aspirant_id, reg_no, surname, other_names, position, gender, study_year, school, resident, hostel from aspirants WHERE position = '" .$pos_code. "'";

	$asp_stmt = $connection->prepare($aspirants_query);

	$asp_stmt->execute();

	$aspt_res = $asp_stmt->fetchAll(PDO::FETCH_ASSOC);

	if (count($aspt_res) != 0 ) {

		echo "<p>Position : $s_node->nodeValue</p>";
		echo "<table>";
		echo "<tr><td>Surname</td><td>Other Name</td><td>Registration Number</td><td>Gender</td><td>Study Year</td><td>School</td><td>School Resident</td><td>Hostel</td></tr>";

		foreach ($aspt_res as $row) {
			echo "<tr>";
				echo "<td>" .$row['surname']. "</td>";
				echo "<td>" .$row['other_names']. "</td>";
				echo "<td>" .$row['reg_no']. "</td>";
				echo "<td>";
					$xmlDoc = new DOMDocument();
					if(file_exists($_SERVER['DOCUMENT_ROOT'] . 'genders.xml')){
						$xmlDoc->load($_SERVER['DOCUMENT_ROOT'] . 'genders.xml');
						$node = $xmlDoc->getElementsByTagName("gender");
						foreach ($node as $s_node) {
							$code = $s_node->getAttribute('initial');
							if ($row['gender'] == $code)
								echo $s_node->nodeValue;
						}
					} else {
						echo 'Error loading file';
						die();
					}  
				echo "</td>";
				echo "<td>" .$row['study_year']. "</td>";
				echo "<td>";
					$xmlDoc = new DOMDocument();
					if(file_exists($_SERVER['DOCUMENT_ROOT'] . 'schools_initials.xml')){
						$xmlDoc->load($_SERVER['DOCUMENT_ROOT'] . 'schools_initials.xml');
						$node = $xmlDoc->getElementsByTagName("school");
						foreach ($node as $s_node) {
							$code = $s_node->getAttribute('initial');
							if ($row['school'] == $code)
								echo $s_node->nodeValue;
						}
					} else {
						echo 'Error loading file';
						die();
					}  
				echo "</td>";
				echo "<td>" .strtoupper($row['resident']). "</td>";
				echo "<td>";
					$xmlDoc = new DOMDocument();
					if(file_exists($_SERVER['DOCUMENT_ROOT'] . 'hostels_codes.xml')){
						$xmlDoc->load($_SERVER['DOCUMENT_ROOT'] . 'hostels_codes.xml');
						$node = $xmlDoc->getElementsByTagName("hostel");
						foreach ($node as $s_node) {
							$code = $s_node->getAttribute('code');
							if ($row['hostel'] == $code)
								echo $s_node->nodeValue;
						}
					} else {
						echo 'Error loading file';
						die();
					}  
				echo "</td>";
				echo (isset($_GET['mode']) && $_GET['mode'] == 'edit') ? "<td><a href='edit.php?asp_id=" .$row['aspirant_id']. "'>Edit</a></td>" : '';
				echo (isset($_GET['mode']) && $_GET['mode'] == 'delete') ? "<td><a href='delete.php?asp_id=" .$row['aspirant_id']. "'>Delete !</a></td>" : '';
			echo "</tr>";
		}
		if (isset($_GET['align']) && $_GET['align'] == 'tabular') {
			require_once($_SERVER['DOCUMENT_ROOT'] . 'root/register_form.php');
		}
		echo "</table>";
		echo "<a href='".$_SERVER['PHP_SELF']."?mode=edit&align=tabular'>Add aspirant</a>";
	}

	}
			} else {die('Error loading file'); }

 ?>