<?php 
	session_start();
	if(isset($_SESSION['root'])) {
		require_once $_SERVER['DOCUMENT_ROOT'] . '/root_nav_menu.php';
	} 
	else {
		//redirect to the home page
	$home_url = 'http://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/';
	header('Location: ' . $home_url);
	}

 ?>