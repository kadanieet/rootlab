<tr><form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
	<!--label for="reg_no">Registration Number</label-->
		<td><input type="text" name="reg_no" value="<?php echo $reg_no; ?>" placeholder="Registration Number"><!--br--></td>
	<!--label for="surname">Surname</label-->
		<td><input type="text" name="surname" value ="<?php echo "$surname"; ?>" placeholder="Surname"><!--br--></td>
	<!--label for="other_name">Other Name</label-->
		<td><input type="text" name="other_name" value ="<?php echo "$other_name"; ?>" placeholder="Other Name"><!--br--></td>
	<!--label for="position">Position</label-->	
		<td><input type="text" name="position" value="<?php echo $position; ?>" placeholder="position"><!--br--></td>
	<!--label for="gender">Gender</label-->
		<td><select name="gender">
			  <option value="null" <?php if ($gender == "null") echo "selected='selected'"; ?> >--Select--</option>
			  <option value="female" <?php if ($gender == "female") echo "selected='selected'"; ?> >Female</option>
			  <option value="male" <?php if ($gender == "male") echo "selected='selected'"; ?> >Male</option>
		</select><!--br--></td>
	<!--label for="study_year">Study Year</label-->
		<td><select name="study_year">
		    <option value="null" <?php if ($study_year == "null") echo "selected='selected'"; ?> >--Select--</option>
			    <?php 
						$xmlDoc = new DOMDocument();
						
						if(file_exists($_SERVER['DOCUMENT_ROOT'] . 'schools_initials_years.xml')){
								$xmlDoc->load($_SERVER['DOCUMENT_ROOT'] .'schools_initials_years.xml');
								$node = $xmlDoc->getElementsByTagName("school");
								
								foreach ($node as $s_node) {
									$initial = $s_node->getAttribute('initial'); 
									$reg_arr = str_split(strtoupper($reg_no_1));
									if ($reg_no_1 == "")
											$reg_arr[0] = 'P';

									if($initial == $reg_arr[0]) { 
										$years = $s_node->getAttribute('years');

										foreach (range(1, $years) as $year) {
											echo "<option ";
											if ($study_year == $year) echo "selected='selected'";
											echo " value='" .$year. "'>".$year;
											echo "</option>";
										}
										break;		
									}
								}
						} else { exit('error loading file');}
					
			 	?>
		 </select><!--br--></td>
	<!--label for="resident">School Resident</label-->
		<td><select name="resident">
		    <option value="null" <?php if ($resident == "null") echo "selected='selected'"; ?> >--Select--</option>
			<option value="no" <?php if ($resident == "no") echo "selected='selected'"; ?> >No</option>
			<option value="yes" <?php if ($resident == "yes") echo "selected='selected'"; ?> >Yes</option>
		</select><!--br--></td>
	<!--label for="hostel">Hostel Name</label-->
		<td><select name="hostel">
		    <option value="null" selected>---------Select--------</option>
		    		<?php 
						$xmlDoc = new DOMDocument();
						if(file_exists($_SERVER['DOCUMENT_ROOT'] . 'hostels_codes.xml')){
							$xmlDoc->load($_SERVER['DOCUMENT_ROOT'] . 'hostels_codes.xml');
							$node = $xmlDoc->getElementsByTagName("hostel");
							foreach ($node as $s_node) {
								$code = $s_node->getAttribute('code');
								echo "<option ";
								if ($hostel == $code) echo "selected='selected'";
								echo " value='" .$code. "'>".$s_node->nodeValue;
								echo "</option>";
							}
						} else {
							echo 'Error loading file';
							die();
						}
				
		 	?>
	    </select><!--br--></td>
	<!--label for="user_image">Aspirant Image</label-->
		<td><input type="file" name="user_image"><!--br--></td>
	<td><input type="submit" name="add" value="Register"></td>
</form><tr>