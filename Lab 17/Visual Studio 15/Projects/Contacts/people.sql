﻿CREATE TABLE People
(
	[ContactId] INT NOT NULL PRIMARY KEY UNIQUE IDENTITY, 
    [Name] NCHAR(100) NULL, 
    [Company] NCHAR(100) NULL, 
    [Telephone] NCHAR(100) NULL, 
    [Email] NCHAR(100) NULL, 
    [Client] BIT NULL, 
    [LastCall] DATETIME NULL
)
