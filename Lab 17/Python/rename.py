def rename_files(directory, start_character, character_number):
    import os
    os.chdir(directory)
    for filename in os.listdir("."):
        if filename.startswith(start_character):
            os.rename(filename, filename[character_number:])
