from lxml.html import parse
from urllib2 import urlopen

parsed = parse(urlopen('https://www.wunderground.com/history/airport/KATW/2016/1/1/DailyHistory.html?req_city=&req_state=&req_statename=&reqdb.zip=&reqdb.magic=&reqdb.wmo'))
doc = parsed.getroot()
tables = doc.findall('.//table')
table = tables[4]
#rows = table.findall('.//tr')

def _unpack(row, kind='td'):
    elts = row.findall('.//%s' % kind)
    return [val.text_content() for val in elts]

#_unpack(rows[0], kind='th')
#_unpack(rows[1], kind='td')SS


from pandas.io.parsers import TextParser

def parse_options_data(table):
    rows = table.findall('.//tr')
    header = _unpack(rows[0], kind='th')
    data = [_unpack(r) for r in rows[1:]]
    return TextParser(data, names=header).get_chunk()

data = parse_options_data(table)
