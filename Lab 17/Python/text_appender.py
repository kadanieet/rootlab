import print_lol

"""This module takes an text file and appends a string to the end of each line
that contains a constant prefix and a sequential integer as per the line number"""

def text_appender(input_file, output_file, string='', delimiter='-'):
    data = []
    nos = []
    clean_data = []
    with open(input_file) as file:
        for e_l in file:
            e_l = e_l.strip('\n')
            data.append(e_l)
            
    for e_l in data:
        e_l = e_l.strip('\n')
        clean_data.append(e_l)
		
    for i in range(1,len(data) + 1):
        nos.append(' ' + delimiter + ' ' + string +  str(i))
        
    app_txt = [x + str(y) for x, y in zip(data, nos)]

    with open(output_file, 'w') as output_f:
        print_lol.print_lol(app_txt, fh=output_f)
