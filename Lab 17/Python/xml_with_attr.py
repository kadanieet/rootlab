import print_lol

"""xml with attr is a module that creates an xml file(s) from a text file(s) adding an attribute to each xml element.
A series of root and child element of tags plus an xml declaration for xml must be defined. The attribute value is
defined at the end of each line in the txt file separated from the element value using a delimiter."""

def xml_with_attr(text_file, root_element_title='root', child_element_name='child', attribute_name='initial', delimiter='-', xml_dec='xml_dec.xml'):
    dirty_xml = []
    (file_name, ext) = text_file.split('.', 1)
    try:
        with open(xml_dec) as declaration:
            for dec in declaration:
                clean_dec = dec.strip()
                dirty_xml.append(clean_dec)
    except IOError as err:
        print('File error: ' + str(err))

    dirty_xml.append('<' + str(root_element_title.strip()) + '>')

    try:
        with open(text_file) as file:
            for each_line in file:
                clean_line = each_line.strip()
                (value, initial) = clean_line.split(delimiter, 1)
                dirty_xml.append('<' + str(child_element_name.strip()) + ' ' + str(attribute_name.strip()) + '="' + str(initial.strip()) + '">' + str(value.strip()) + '</' + str(child_element_name.strip()) + '>')

    except IOError as err:
        print ('File error: ' + str(err))

    dirty_xml.append('</' + str(root_element_title) + '>')

    with open(file_name + '.xml', 'w') as clean_xml:
        print_lol.print_lol(dirty_xml, fh=clean_xml)
