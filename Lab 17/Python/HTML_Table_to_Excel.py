from lxml.html import parse
from urllib2 import urlopen
from pandas.io.parsers import TextParser
import pandas as pd
import os

os.chdir('C:\\Common\\OneDrive\\Lab 17')

"""This module fetches weather information for a year
from a website(Link as url) and converts it into excel spreadsheet"""

month = 6
date = 1

data = pd.DataFrame()
data_2 = pd.DataFrame()

month_range = range(6,13)

day_range = range(1)

Day_31 = [7,8,10,12]
Day_30 = [6,9,11]

def _unpack(row, kind='td'):
    elts = row.findall('.//%s' % kind)
    return [val.text_content() for val in elts]

def parse_options_data(table):
    rows = table.findall('.//tr')
    header = _unpack(rows[0], kind='th')
    header.append('Date')
    header = ['Time (CDT)' if x=='Time (CST)' else x for x in header]
    data = [_unpack(r) for r in rows[1:]]
    for each_row in data:
        each_row.append(str(date) + '/' + str(month) + '/2016')
    return TextParser(data, names=header).get_chunk()

def HTML_Table_to_Excel(table_index):
    url = 'https://www.wunderground.com/history/airport/KATW/2016/' + str(month) + '/' + str(date) + '/DailyHistory.html?req_city=&req_state=&req_statename=&reqdb.zip=&reqdb.magic=&reqdb.wmo='
    print(url)
    parsed = parse(urlopen(url))
    doc = parsed.getroot()
    tables = doc.findall('.//table')
    table = tables[table_index]

    temp_data = parse_options_data(table)
    #print(temp_data)
    global data_2
    data_2 = data_2.append(temp_data)
    #print(data_2)

for each_month in month_range:
    if each_month in Day_31:
        global day_range
        day_range = range(1,32)
    elif each_month in Day_30:
        global day_range
        day_range = range(1,31)

    for each_day in day_range:
        global month
        global date
        month = each_month
        date = each_day
        HTML_Table_to_Excel(4)
        
#print(data_2)
writer = pd.ExcelWriter('Final.xlsx')
data_2.to_excel(writer, 'Data', index = False, columns=['Date', 'Time (CDT)', 'Temp.', 'Windchill', 'Dew Point', 'Humidity', 'Pressure', 'Visibility', 'Wind Dir', 'Wind Speed', 'Gust Speed', 'Precip', 'Events', 'Conditions'])
writer.save()
