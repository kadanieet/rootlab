import print_lol


"""Text processor is a module that takes a text files and outputs a cleaned
and sanitized text file from trailing spaces and empty lines"""

def text_processor(input_url, output_url):
    file_data = []

    with open(input_url) as input_file:
        for each_line in input_file:
            clean_line = each_line.strip()
            file_data.append(clean_line)

    file_data.sort()
    
    with open(output_url, 'w') as output_file:
        print_lol.print_lol(file_data, fh=output_file)
